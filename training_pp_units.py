import argparse
import time
from tqdm import tqdm
import numpy as np
import threading
import os
import copy
import numpy as np


import utils as mg_utils
import main_globals as mg
import pitch_estimation.tasks.post_processing.types.all as pp_units
import pitch_estimation.utils as pitch_estimation_utils
from pitch_estimation.tasks.post_processing.controller import PostProcessingController
from pitch_estimation.auxiliary.data.results.note_results import NoteResults
from pitch_estimation.auxiliary.data.metadata.global_metadata import GlobalMetadata
from training_pps_scripts.pp_units_reader import PPUnitsCSVDataImporter
from pitch_estimation.tasks.learner.filters.types.sigmoid import Sigmoid
import debug_tools
from threads.data.shared_data import SharedData
import pitch_estimation.configs as configs
from pitch_estimation.auxiliary.training.early_stopping import EarlyStoppingException
import pitch_estimation.utils as utils
import web.commands as commands
import web.running_mode as running_mode
from pitch_estimation.auxiliary.metrics.types.metrics import BEST_MODEL_METRICS


def main():
    np.random.seed(seed=configs.NUMPY_RANDOM_SEED)
    args = __parse_arguments()
    args.start_time = time.time()

    mg.SET_DATASET_FOLDER(dataset_folder=args.dataset_folder)
    if args.training:
        start_training(args=args)
    elif args.predicting:
        if args.models_folder == 'last':
            args.models_folder = mg_utils.get_last_models_folder()

        start_predicting(args=args)
    else:
        mg.ERROR('Only training and prediction mode is supported. Given arguments: %s' % (args))

    mg.INFO("The whole process took %s" % (debug_tools.get_operation_time_from_start_time(start_time=args.start_time)))


def start_training(args):
    mg.INFO('Starting training method...')
    # Shitty code but it works!
    args.variations_size = 0
    if len(args.names_post_processing) > 0:
        first_name_post_processing = args.names_post_processing[0]
        if 'nn_per_note' not in first_name_post_processing and \
           'single_nn' not in first_name_post_processing and \
           'fix_onsets' not in first_name_post_processing and \
           'fix_offsets' not in first_name_post_processing and \
           'discard_notes' not in first_name_post_processing:
            raise ValueError('Invalid post processing unit')

        counting_variations = ['_also_']
        for v in counting_variations:
            args.variations_size += first_name_post_processing.count(v)

    with PPUnitsCSVDataImporter(is_training=True,
                                variations_size=args.variations_size) as data_importer:  # Only using for getting global metadata
        with PostProcessingController(summary_output_folder=pitch_estimation_utils.get_summaries_output_folder(args.start_time),
                                      output_folderpath=pitch_estimation_utils.get_models_output_folder(args.start_time),
                                      post_processes=args.post_processing,
                                      names_post_processes=args.names_post_processing,
                                      notes_number=args.notes_number,
                                      is_training=True) as post_processing:

            training_global_meta = data_importer.get_training_global_metadata()
            testing_global_meta = data_importer.get_testing_global_metadata()
            # Print dataset informations
            mg.INFO(GlobalMetadata.get_joined_DS_informations(training_global_meta=training_global_meta,
                                                              testing_global_meta=testing_global_meta))

            all_threads = __start_train_producer_and_consumer_threads(args=args,
                                                                      training_global_meta=training_global_meta,
                                                                      testing_global_meta=testing_global_meta,
                                                                      data_importer=data_importer,
                                                                      post_processing=post_processing)
            __wait_for_threads(threads=all_threads)


def start_predicting(args):
    mg.INFO('Starting prediction method...')
    # Shitty code but it works!
    args.variations_size = 0
    if len(args.names_post_processing) > 0:
        first_name_post_processing = args.names_post_processing[0]
        if 'nn_per_note' not in first_name_post_processing and \
           'single_nn' not in first_name_post_processing and \
           'fix_onsets' not in first_name_post_processing and \
           'fix_offsets' not in first_name_post_processing and \
           'discard_notes' not in first_name_post_processing:
            raise ValueError('Invalid post processing unit')

        counting_variations = ['_also_']
        for v in counting_variations:
            args.variations_size += first_name_post_processing.count(v)

    args.post_processing, args.names_post_processing = utils.add_post_processing_unit_if_does_not_exist(post_processes=args.post_processing,
                                                                                                        names_post_processes=args.names_post_processing,
                                                                                                        pp_unit_class=pp_units.WriteOutput)

    args.post_processing, args.names_post_processing = utils.add_post_processing_unit_if_does_not_exist(post_processes=args.post_processing,
                                                                                                        names_post_processes=args.names_post_processing,
                                                                                                        pp_unit_class=pp_units.ToMidi)
    with PPUnitsCSVDataImporter(is_training=False,
                                variations_size=args.variations_size) as data_importer:  # Only using for getting global metadata
        with PostProcessingController(summary_output_folder=pitch_estimation_utils.get_summaries_output_folder(args.start_time),
                                      output_folderpath=None,
                                      post_processes=args.post_processing,
                                      names_post_processes=args.names_post_processing,
                                      recover_models_folder=args.models_folder,
                                      notes_number=args.notes_number,
                                      is_training=False) as post_processing:

            testing_global_meta = data_importer.get_testing_global_metadata()
            # Print dataset informations
            mg.INFO(testing_global_meta)

            all_threads = __start_predict_producer_and_consumer_threads(args=args,
                                                                        testing_global_meta=testing_global_meta,
                                                                        data_importer=data_importer,
                                                                        post_processing=post_processing)
            __wait_for_threads(threads=all_threads)


def __start_train_producer_and_consumer_threads(args, training_global_meta, testing_global_meta, data_importer, post_processing):
    training_shared_data, evaluation_shared_data = __get_shared_data(training_global_meta=training_global_meta,
                                                                     testing_global_meta=testing_global_meta)
    all_threads = []
    producer = threading.Thread(name='producer', target=__train_producer, args=(args,
                                                                                training_shared_data,
                                                                                evaluation_shared_data,
                                                                                data_importer))
    all_threads.append(producer)

    consumer = threading.Thread(name='consumer', target=__train_consumer, args=(args,
                                                                                training_shared_data,
                                                                                evaluation_shared_data,
                                                                                training_global_meta,
                                                                                testing_global_meta,
                                                                                post_processing))
    all_threads.append(consumer)

    for t in all_threads:
        t.start()

    return all_threads


def __start_predict_producer_and_consumer_threads(args, testing_global_meta, data_importer, post_processing):
    _, evaluation_shared_data = __get_shared_data(testing_global_meta=testing_global_meta)
    all_threads = []
    producer = threading.Thread(name='producer', target=__predict_producer, args=(args,
                                                                                  evaluation_shared_data,
                                                                                  data_importer))
    all_threads.append(producer)

    consumer = threading.Thread(name='consumer', target=__predict_consumer, args=(args,
                                                                                  evaluation_shared_data,
                                                                                  testing_global_meta,
                                                                                  post_processing))
    all_threads.append(consumer)

    for t in all_threads:
        t.start()

    return all_threads


def __predict_producer(args, testing_shared_data, data_importer):
    data_importer.produce_prediction_data(testing_shared_data=testing_shared_data)


def __predict_consumer(args, testing_shared_data, testing_global_meta, post_processing):
    __predict(args=args,
              testing_shared_data=testing_shared_data,
              post_processing=post_processing,
              global_metadata=testing_global_meta)


def __train_producer(args, training_shared_data, evaluation_shared_data, data_importer):
    for i in range(args.num_iterations):
        data_importer.produce_training_data(training_shared_data=training_shared_data)

        current_iteration = i + 1
        if current_iteration % args.num_test_folds == 0:
            data_importer.produce_evaluation_data(evaluation_shared_data=evaluation_shared_data)
            is_to_continue = evaluation_shared_data.wait_for_consumers()
        else:
            is_to_continue = training_shared_data.wait_for_consumers()

        if not is_to_continue:
            break

        data_importer.reset_reading_indexes()


def __train_consumer(args, training_shared_data, evaluation_shared_data, training_global_meta, testing_global_meta, post_processing):
    for i in range(args.num_iterations):
        iteration_time = time.time()
        __train(args=args,
                training_shared_data=training_shared_data,
                post_processing=post_processing,
                global_metadata=training_global_meta)

        current_iteration = i + 1
        if current_iteration % args.num_test_folds == 0:
            __evaluate(args=args,
                       evaluation_shared_data=evaluation_shared_data,
                       post_processing=post_processing,
                       global_metadata=testing_global_meta)

        try:
            post_processing.increase_global_step()
        except EarlyStoppingException as e:
            mg.INFO(e)
            evaluation_shared_data.notify_early_stopping()
            training_shared_data.notify_early_stopping()
            break
        else:
            evaluation_shared_data.notify_producer()
            training_shared_data.notify_producer()
        finally:
            mg.INFO("Iteration %d took %s" % (current_iteration,
                                              debug_tools.get_operation_time_from_start_time(start_time=iteration_time)))

    __upload_models(to=args.upload,
                    start_time=args.start_time,
                    note_number=args.notes_number[0])


def __train(args, training_shared_data, post_processing, global_metadata):
    __loop_over_data(run_mode=mg.RunMode.TRAINING,
                     args=args,
                     shared_data=training_shared_data,
                     post_processing=post_processing,
                     global_metadata=global_metadata)


def __evaluate(args, evaluation_shared_data, post_processing, global_metadata):
    __loop_over_data(run_mode=mg.RunMode.EVALUATING,
                     args=args,
                     shared_data=evaluation_shared_data,
                     post_processing=post_processing,
                     global_metadata=global_metadata)


def __predict(args, testing_shared_data, post_processing, global_metadata):
    __loop_over_data(run_mode=mg.RunMode.PREDICTING,
                     args=args,
                     shared_data=testing_shared_data,
                     post_processing=post_processing,
                     global_metadata=global_metadata)


def __loop_over_data(run_mode, args, shared_data, post_processing, global_metadata):
    tqdm_shared_data = tqdm(shared_data)
    for i, d in enumerate(tqdm_shared_data):
        data_in = d.data_in
        data_out = d.data_out
        local_meta = d.local_meta

        if len(data_in) <= 0:
            mg.WARN('The sound with the following filename {0} doesn\'t have any training data.'.format(local_meta.filename))

        else:  # Do training
            is_last_music = shared_data.has_finish()
            results = get_all_note_results_from_data(data_in,
                                                     data_out,
                                                     local_meta=local_meta,
                                                     notes_number=args.notes_number,
                                                     variations_size=args.variations_size)
            onsets, offsets = __get_notes_onsets_and_offsets(frames=data_in)
            post_processing.apply(results=results,
                                  run_mode=run_mode,
                                  is_last_music=is_last_music,
                                  global_metadata=global_metadata,
                                  best_model=args.best_model,
                                  additional_data={'predictions_onsets': onsets,
                                                   'predictions_offsets': offsets})


def __get_notes_onsets_and_offsets(frames):
    if type(frames) != np.ndarray:
        frames = np.asarray(frames)

    others_onsets = np.zeros(shape=frames.shape)
    others_offsets = np.zeros(shape=frames.shape)
    frames = np.round(frames)  # Returns a copy of the array
    for n in range(mg.NUMBER_OF_NOTES):
        previous_frame = 0
        for i in range(len(frames)):
            current_frame = frames[i][n]
            if previous_frame == 0 and current_frame == 1:
                others_onsets[i][n] = 1
            elif previous_frame == 1 and current_frame == 0:
                others_offsets[i][n] = 1

            previous_frame = current_frame

    return others_onsets, others_offsets


def get_all_note_results_from_data(input_data, labels, local_meta, notes_number, variations_size):
    original_all_notes_results = get_note_results_from_data(input_data,
                                                            labels,
                                                            local_meta,
                                                            notes_number,
                                                            variation_level=0,
                                                            variations_size=variations_size)
    prev = None
    for i in range(1, variations_size + 1):
        additional_all_notes_results = get_note_results_from_data(input_data,
                                                                  labels,
                                                                  local_meta,
                                                                  notes_number,
                                                                  variation_level=i,
                                                                  variations_size=variations_size)
        append_to_previous_note_results(additional_all_notes_results, prev)
        prev = additional_all_notes_results

    append_to_previous_note_results(original_all_notes_results, prev)
    return original_all_notes_results


def append_to_previous_note_results(recent_all_notes_results, previous_all_notes_results):
    if previous_all_notes_results is None:
        for recent_note_results in recent_all_notes_results['sigmoid']:
            recent_note_results.previous_note_results = None
    else:
        assert len(recent_all_notes_results['sigmoid']) == len(previous_all_notes_results['sigmoid'])
        for recent_note_results in recent_all_notes_results['sigmoid']:
            found = False
            for prev_note_results in previous_all_notes_results['sigmoid']:
                if recent_note_results.note_number == prev_note_results.note_number:
                    recent_note_results.previous_note_results = prev_note_results
                    found = True
                    break
            assert found


def get_note_results_from_data(input_data, labels, local_meta, notes_number, variation_level, variations_size):
    filter_type = 'sigmoid'
    all_notes_results = []
    creator_identifier = 'nn_per_note_' + str(variation_level - 1)
    start_note_index = 0
    if variation_level == 0:
        creator_identifier = 'nn_per_note_' + str(variations_size)
    else:
        if variation_level == 1:
            creator_identifier = 'classifier'

        start_note_index = mg.NUMBER_OF_NOTES * variation_level

    for n in notes_number:
        note_index = n - mg.FIRST_NOTE_NUMBER
        predictions = __transform_output_data(input_data, note_index + start_note_index)
        note_labels = __transform_output_data(labels, note_index)
        note_labels = __round_labels(note_labels)
        note_results = NoteResults(creator_identifier=creator_identifier,
                                   filename=local_meta.filename,
                                   note_number=n,
                                   filter_type=filter_type,
                                   labels=note_labels,
                                   predictions=predictions,
                                   losses=[])
        all_notes_results.append(note_results)

    return {filter_type: all_notes_results}


def __transform_output_data(output_data, note_index):
    real = []
    for data in output_data:
        value = data[note_index]
        real.append(value)
    return np.array(real)


def __round_labels(a, has_note=Sigmoid.HAS_NOTE, no_note=Sigmoid.NO_NOTE):
    real_a = []
    middle = (has_note + no_note) / 2
    for val in a:
        if val <= middle:
            real_a.append(no_note)
        else:
            real_a.append(has_note)
    return np.array(real_a)


def __parse_arguments():
    parser = argparse.ArgumentParser(description='Available arguments:')

    algo_group = parser.add_mutually_exclusive_group(required=True)
    algo_group.add_argument('-t', '-T', '--training',
                            help='Run the training algorithm',
                            action='store_const',
                            const=True)
    algo_group.add_argument('-p', '-P', '--predicting',
                            help='Run the prediction algorithm',
                            action='store_const',
                            const=True)

    # Required options
    parser.add_argument('-no', '-NO', '--notes_number',
                        help='Notes to be detected by the NN',
                        type=int,
                        required=True,
                        nargs='+')
    parser.add_argument('-ds', '-DS', '--dataset_folder',
                        help='Dataset folder',
                        type=str,
                        required=True)
    parser.add_argument('-upload', '-UPLOAD', '--upload',
                        help='Folder to upload',
                        type=str,
                        required=True)

    # Training options
    parser.add_argument('-n', '-N', '--num_iterations',
                        help='Number of iterations that the algorithm should run',
                        default=10,
                        type=int)
    parser.add_argument('-n_test_folds', '-N_TEST_FOLDS', '--num_test_folds',
                        help='Number of iterations that the algorithm should run the training dataset before running the testing dataset',
                        default=1,
                        type=int)
    parser.add_argument('-pp', '-PP', '--post_processing',
                        help='Post processing tools',
                        type=str,
                        required=True)

    # Prediction options
    parser.add_argument('-m', '-M', '--models_folder',
                        help='Folder that contains all the trained models')

    parser.add_argument('-b_m', '-B_M', '--best_model',
                        help='Best model by metric name',
                        default=None,
                        type=str)

    args = parser.parse_args()
    __validate_args(args)
    return args


def __validate_args(args):
    error_message = None
    usage = 'usage: ' + __file__ + ' -n [number of iterations] -n_test_folds [number of folds] --notes_number [note number to train] --dataset [FOLDER CONTAINING DATASET] --post_processing [type of post-processing]\n\n'

    # Required arguments
    if args.post_processing == 'step_1':
        args.post_processing = 'nn_per_note'
    elif args.post_processing == 'step_2':
        args.post_processing = 'nn_per_note_also_classifier_onsets_classifier'
    elif args.post_processing == 'step_3':
        args.post_processing = 'fix_onsets_per_note'
    
    args.post_processing, args.names_post_processing = pp_units.get_pps_from_names(args.post_processing)
    if args.post_processing is None:
        all_pps_names = mg_utils.array_to_string(pp_units.all_pps_names)
        error_message = usage + __file__ + ': [post_processing] must be only the following values: ' + all_pps_names

    if not os.path.isdir(args.upload):
        error_message = usage + __file__ + ': [upload] must be an existent folder.'

    if not mg_utils.is_number_between(numbers=args.notes_number,
                                      min_inclusive=mg.FIRST_NOTE_NUMBER,
                                      max_inclusive=mg.FIRST_NOTE_NUMBER + mg.NUMBER_OF_NOTES - 1):
        error_message = usage + __file__ + ': error: [notes_number] must be at least one note number between or equal to 21-108'
    else:
        if len(args.notes_number) > 1 and args.training:
            for name in args.names_post_processing:
                if 'per_note' in name:
                    error_message = usage + __file__ + ': error: [notes_number] in training mode only one note can be trained at a time'
                    break

    # Training arguments
    if args.training:
        if args.num_iterations:
            if not mg_utils.is_positive_int(args.num_iterations):
                error_message = usage + __file__ + ': error: [number of iterations] must be a number bigger than 0'
        if args.num_test_folds:
            if not mg_utils.is_positive_int(args.num_test_folds):
                error_message = usage + __file__ + ': error: [number of folds] must be a number bigger than 0'

    # Prediction arguments
    elif args.predicting:
        usage = 'usage: ' + __file__ + ' -p -m [MODEL] --best_model [METRIC NAME] --notes_number [NOTE NUMBER] --dataset [FOLDER] --upload [OUTPUT FOLDER]\n\n'
        if args.models_folder is None:
            error_message = usage + '-m/-M/--models_folder is required when running prediction algorithm'

        elif args.models_folder != 'last' and not os.path.isdir(args.models_folder):
            error_message = usage + '-m/-M/--models_folder must be an existent directory with tensorflow models'

        elif args.best_model is not None:
            args.best_model = args.best_model.lower()
            if args.best_model not in BEST_MODEL_METRICS:
                error_message = usage + '-b_m/-B_M/--best_model must be one of the following values ' + str(BEST_MODEL_METRICS) + '.'

    if error_message:
        mg.ERROR(error_message)


def __get_shared_data(testing_global_meta, training_global_meta=None):
    training_shared_data = None
    if training_global_meta:
        training_shared_data = SharedData(length=training_global_meta.nr_of_sounds,
                                          sequencial_musics=True,
                                          log_filepath=None)

    evaluation_shared_data = SharedData(length=testing_global_meta.nr_of_sounds,
                                        sequencial_musics=True,  # In the evaluation it doesn't really matters to not be sequencial because we are not training the model, just evaluating it.
                                        log_filepath=None)

    return training_shared_data, evaluation_shared_data


def __wait_for_threads(threads):
    for t in threads:
        t.join()


def __upload_models(to, start_time, note_number, debug=False):
    if to:
        models_output_folder = utils.get_models_output_folder(start_time)
        summary_output_folder = utils.get_summaries_output_folder(start_time)
        filepath = __get_zip_filepath_to_upload(to=to, note_number=note_number)
        __zip_files(filepath=filepath, files=[models_output_folder, summary_output_folder])


def __get_zip_filepath_to_upload(to, note_number):
    return os.path.join(to, str(note_number) + '.zip')


def __zip_files(filepath, files, debug=False):
    if isinstance(files, str):
        files = [files]

    if not isinstance(files, list):
        mg.ERROR('Invalid argument type denominated by "files" in "__zip_files" function. Expected type: list or str. Given type: %s' % (type(files)))

    if len(files) == 0:
        mg.WARN('No files to upload.')
        return False

    # Build OS command
    cmd = commands.ZIP + ' -r ' + str(filepath) + ' '
    for f in files:
        cmd += ' ' + str(f)
    # Run command
    running_mode.RunningMode.exec_command(command=cmd, debug=debug)
    return True


if __name__ == '__main__':
    main()
