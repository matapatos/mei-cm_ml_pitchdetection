## Folder structure ##

This folder is separated per each fold of the Configuration 1 dataset used.

Also, inside each fold there is always five sub-folders: 

- **originals** - Folder containing the original musical pieces used for transcription.
- **labels** - " " the expected transcription and also the conversion of that expected transcription into musical pieces. This is different from the **originals** sub-folder because during the conversion some variables like the volume of each musical note is lost, and thus, to be more fair, a comparison from these musical pieces with the ones originated by our transcription system is much more fair.
- **classifiers** - " " the resultant transcription and the musical pieces created from that transcription from the classification stage.
- **step_1** - " " " " " " from the post-processing stage, step 1.
- **step_2** -  " " " " " " from the post-processing stage, step 2.
- **step_3** -  " " " " " " from the post-processing stage, step 3.

Additionally, inside each sub-folder there are three types of files:

- *.html* - This file contains the resultant or expected transcription.
- *.mid* - Same as the *.html* file but in another file format.
- *.mp3* - Audio file created from the transcription represented in the *.html* file.
