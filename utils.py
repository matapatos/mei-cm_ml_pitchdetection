import re
import os
import time
import sys


import time_utils as time_utils
import main_globals as mg


def get_output_folder(start_time, default_folder):
    str_localtime = time.strftime(time_utils.TIME_FORMAT, time.localtime(start_time))
    str_localtime = str_localtime.replace(':', ' ')
    return os.path.join(default_folder, str_localtime)


def get_int(num):
    try:
        n = int(num)
        return n
    except Exception:
        return None


def get_float(num):
    try:
        n = float(num)
        return n
    except Exception:
        return None


def is_zero_or_positive_int(num):
    n = get_int(num=num)
    if n < 0:
        return False
    else:
        return True


def is_positive_int(num):
    n = get_int(num=num)
    if n is None:
        return False
    elif n <= 0:
        return False
    else:
        return True


def is_number_between(numbers, min_inclusive, max_inclusive=sys.maxsize):
    if not isinstance(numbers, list):
        numbers = [numbers]

    for num in numbers:
        num = get_int(num=num)
        if num is None:
            return False

        if num < min_inclusive or num > max_inclusive:
            return False

    return True


def convert_to_underscore_and_lowercase(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def get_last_models_folder():
    return get_last_folder_from_folder(main_folder=mg.PARENT_MODELS_FOLDER)


def get_last_graph_folder():
    return get_last_folder_from_folder(main_folder=mg.PARENT_SUMMARIES_FOLDER)


def get_last_folder_from_folder(main_folder):
    if not os.path.isdir(main_folder):
        os.makedirs(main_folder)

    last_datetime = None
    for f in os.listdir(main_folder):
        cur_datetime = time_utils.convert_string_as_datetime(f)
        if last_datetime is None or time_utils.compare_localtimes(last_datetime, cur_datetime) == time_utils.L1_SMALLER_THAN_L2:
            last_datetime = cur_datetime

    if last_datetime is None:
        mg.ERROR('It doesn\'t exists any models folder on {0}'.format(main_folder))

    return os.path.join(main_folder, time.strftime(time_utils.TIME_FORMAT, last_datetime))


def array_to_string(a):
    new_a = str(a).replace('[', '')
    return new_a.replace(']', '')


def dict_to_string(d):
    return array_to_string(list(d.values()))


def is_class_initialized(class_var):
    return not type(class_var) == type


def has_any_initialized_class_in_array(a, class_var=None):
    '''
    If none the parameter "class_var" is not given, then it will return the number of initialized classes contained in the array.
    Otherwise a count of the class given will be returned.
    '''
    counter = 0
    for val in a:
        if class_var and not isinstance(val, class_var):
            continue

        if is_class_initialized(val):
            counter += 1
    return counter


def get_note_index_from_number(note_number):
    return note_number - mg.FIRST_NOTE_NUMBER


def stop_system(reason):
    mg.INFO(reason)
    sys.exit(0)
