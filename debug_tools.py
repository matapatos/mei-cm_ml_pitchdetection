'''
This file contaisn all the necessary functions and variables for debuging purposes.
'''

import time
import os


import main_globals as mg


def write_to_file(filename, lines, mode='a'):
    debug_dir = "debug/"
    if not os.path.exists(debug_dir):
        os.mkdir(debug_dir)

    filename = os.path.join(debug_dir, filename)
    with open(file=filename, mode=mode) as f:
        for l in lines:
            f.write(str(l) + "\n")


def calculate_and_print_operation_time(start_time):
    str_operation_time = get_operation_time_from_start_time(start_time=start_time)
    mg.DEBUG("Operation time " + str_operation_time)


def get_operation_time_from_start_time(start_time):
    seconds = __calculate_time(start_time=start_time)
    return __get_operation_time_as_string_from_seconds(seconds=seconds)


def __calculate_time(start_time):
    return round(time.time() - start_time)


def __get_operation_time_as_string_from_seconds(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)

    return "%d:%02d:%02d" % (h, m, s)
