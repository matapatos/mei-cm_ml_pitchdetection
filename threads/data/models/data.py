

class Data():

    def __init__(self, data_in, data_out, local_meta=None):
        self.data_in = data_in
        self.data_out = data_out
        self.local_meta = local_meta

    def __repr__(self):
        return str(self.local_meta)
