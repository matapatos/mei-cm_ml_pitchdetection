import threading
import random
import collections


random.seed(a=33)


class SharedData():

    MAX_NUMBER_OF_MUSICS = 30
    MIN_NUMBER_TO_NOTIFY = 10

    def __init__(self,
                 length,
                 sequencial_musics=False,
                 log_filepath=None):
        '''
        sequencial_musics -> If True in each training iteration it uses the same music sequence,
                             otherwise if it\'s False at each iteration the music sequence is different

                             If sequencial_musics is False the algorithm reaches better performance metrics
                             but for the moment it\'s impossible to repeat the experiment and get the same 
                             results, because there is no controlling system for that. But in anycase, 
                             the sequence of the musics used is logged in a file, making possible of in
                             the future being able to repeat the experiment.
        '''
        self.length = length
        self.queue_data = []
        self.log_music_sequence_data = collections.defaultdict(list)
        self.log_filepath = log_filepath
        self.condition = threading.Condition()
        self.is_done = False
        self.reachedLimit = False
        self.force_stop = False
        self.iteration_num = 0
        self.sequencial_musics = sequencial_musics

    ############################################
    # Producer functions
    ############################################

    def has_finish(self):
        self.condition.acquire()

        state = True
        if len(self.queue_data) != 0 or self.is_done is False:
            state = False

        self.condition.release()
        return state

    def append(self, data):
        self.condition.acquire()

        self.queue_data.append(data)
        self.condition.notify_all()
        self.__wait_if_queue_has_reach_limit()

        is_to_continue = not self.force_stop

        self.condition.release()
        return is_to_continue

    def set_done(self, done):
        self.condition.acquire()

        self.is_done = done

        self.condition.release()

    def wait_for_consumers(self):
        self.condition.acquire()

        while self.is_done and not self.force_stop:
            self.condition.wait()

        is_to_continue = not self.force_stop

        self.condition.release()
        return is_to_continue

    def __wait_if_queue_has_reach_limit(self):
        while len(self.queue_data) >= self.MAX_NUMBER_OF_MUSICS and not self.force_stop:
            self.reachedLimit = True
            self.condition.wait()

        self.reachedLimit = False

    ############################################
    # Consumer functions
    ############################################

    def write_log_music_sequence(self):
        with open(self.log_filepath, 'a') as f:
            info = "'''Music training sequence\n\nNote that each line in this file corresponds to an iteration'''\n"
            f.writelines(info)
            for iteration_music_sequence in self.log_music_sequence_data.values():
                f.writelines(str(iteration_music_sequence) + "\n")

    def notify_producer(self):
        self.condition.acquire()

        self.is_done = False
        self.condition.notify_all()

        self.condition.release()

    def notify_early_stopping(self):
        self.condition.acquire()

        self.force_stop = True
        self.condition.notify_all()

        self.condition.release()

    def __pop_random_from_queue(self):
        # Select random data from queue
        max_index = len(self.queue_data) - 1
        index = random.randint(0, max_index)
        data = self.queue_data[index]
        # Remove selected index from queue
        queue = self.queue_data[:index]
        if index < max_index:
            queue += self.queue_data[index + 1:]
        self.queue_data = queue
        # Log 'poped' music
        self.__log_music_sequence(data=data)

        return data

    def __pop_first_from_queue(self):
        data = self.queue_data[0]
        self.queue_data = self.queue_data[1:]
        # Log 'poped' music
        self.__log_music_sequence(data=data)
        return data

    def __log_music_sequence(self, data):
        self.log_music_sequence_data[self.iteration_num].append(data.local_meta.filename)

    def __iter__(self):
        return self

    def __next__(self):
        self.condition.acquire()  # Acquire lock

        while len(self.queue_data) == 0:  # Wait until having data to read
            if self.is_done:
                self.iteration_num += 1
                self.condition.release()
                raise StopIteration
            else:
                self.condition.wait()

        if self.sequencial_musics:
            data = self.__pop_first_from_queue()
        else:
            data = self.__pop_random_from_queue()

        if self.reachedLimit and len(self.queue_data) <= self.MIN_NUMBER_TO_NOTIFY:  # Notify producer if queue is getting empty
            self.condition.notify_all()

        self.condition.release()  # Release lock
        return data

    def __len__(self):
        return self.length
