import sys

# Windows configurations
PYTHON = 'C:\\Users\\2151630\\AppData\\Local\\Programs\\Python\\Python36\\python.exe'
UNZIP = 'C:\\Users\\2151630\\Desktop\\Thesis\\mei-cm_ml_pitchdetection\\unzip.exe'
DEL = 'del'
ZIP = 'C:\\Users\\2151630\\Desktop\\Thesis\\mei-cm_ml_pitchdetection\\zip.exe'
COPY = 'copy'

if sys.platform == 'linux':  # Linux configurations
    PYTHON = 'python3'
    UNZIP = 'unzip'
    DEL = 'rm'
    ZIP = 'zip'
    COPY = 'cp'
