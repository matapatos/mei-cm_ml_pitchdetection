
import os


import main_globals as mg
import web.commands as commands


class RunningMode():

    def start(self):
        raise NotImplementedError

    def notify_error(self, error):
        raise NotImplementedError

    @staticmethod
    def exec_command(command, debug=False):
        if type(command) is not str:
            raise TypeError('Invalid command type \'' + type(command) + '\'. A command must be of \'str\' type.')

        if not debug:
            command = RunningMode.disable_command_verbose(cmd=command)

        try:
            # subprocess.check_call(command)
            os.system(command)
        except Exception as e:
            mg.WARN("An error occoured when trying to execute the following command: \' " + str(e) + "\'")
            # self.notifyError(error=e)

    @staticmethod
    def disable_command_verbose(cmd):
        cmd = cmd.strip()
        if cmd.startswith(commands.ZIP):
            if ' -q ' not in cmd:
                cmd = cmd.replace(commands.ZIP + ' ', commands.ZIP + ' -q ')
        return cmd

    def shutdown(self):
        mg.INFO('Shutting down...')
        if self.socket:
            self.socket.close()
