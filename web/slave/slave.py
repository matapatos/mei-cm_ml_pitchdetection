import socket
import os
import shutil


from web.running_mode import RunningMode
import web.msgs.types as msgs_types
import main_globals as mg
import web.slave.configs as configs
import web.commands as commands
import web.master.configs as master_configs


class Slave(RunningMode):

    def __init__(self):
        self.socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)

        if not os.path.isdir(configs.NOTES_DATASETS_FOLDER):
            os.makedirs(configs.NOTES_DATASETS_FOLDER)

        if not os.path.isdir(configs.UPLOAD_FOLDER):
            os.makedirs(configs.UPLOAD_FOLDER)

    def start(self):
        master_address = master_configs.ADDRESS
        self.socket.connect(master_address)
        self.__wait_for_next_work()
        self.shutdown()

    def __wait_for_next_work(self):
        self.socket.sendall(msgs_types.NeedWorkMsg().toBytes())
        while True:
            try:
                data = self.socket.recv(1024).decode()
            except ConnectionError:
                mg.WARN("Master disconnected...")
                break

            if not data:
                break

            msg_type = msgs_types.get_master_msg_type(data)
            if msg_type is None:
                mg.WARN('Unknown message type received from master.')
            else:
                if msg_type == msgs_types.NoMoreWorkMsg:  # Models generated
                    break
                elif msg_type == msgs_types.WorkMsg:
                    work = msgs_types.to_msg_type(data)
                    self.__handle_work(work)
                    # Update master to let him know that the model was generated and uploaded
                    note_number = work.data['note_number']
                    self.socket.sendall(msgs_types.FinishWorkMsg(note_number=note_number).toBytes())

    def __handle_work(self, work):
        dataset_folder = self.__download_DS_if_needed(work=work)
        graph_filepath, model_filepath = self.__execute_work(work=work,
                                                             dataset_folder=dataset_folder)
        self.__upload_model_and_graph(work=work,
                                      model_filepath=model_filepath,
                                      graph_filepath=graph_filepath)

    def __download_DS_if_needed(self, work):
        note_number = work.data['note_number']
        host_dataset = work.data['dataset']
        new_ds = work.data['new_ds']

        local_dataset_folder = self.__get_DS_folder(note_number=note_number)
        if os.path.isdir(local_dataset_folder):  # Already contains folder
            if new_ds:  # If it's to update ds
                shutil.rmtree(local_dataset_folder)
            else:
                return local_dataset_folder

        # Download file
        self.__download_from_master(host_filepath=host_dataset,
                                    local_filepath=configs.NOTES_DATASETS_FOLDER)
        # Unzip file
        cmd = commands.UNZIP + ' ' + local_dataset_folder + '.zip -d ' + local_dataset_folder
        self.exec_command(cmd)
        # Remove unnecessary .zip file
        cmd = commands.DEL + ' ' + local_dataset_folder + '.zip'
        self.exec_command(cmd)
        return local_dataset_folder

    def __execute_work(self, work, dataset_folder):
        note_number = work.data['note_number']
        num_iterations = work.data['num_iterations']
        test_folds = work.data['test_folds']
        filter_type = work.data['filter_type']
        debug = work.data['debug']

        if debug:
            debug = ' -d'
        else:
            debug = ''

        cmd = commands.PYTHON + ' main.py -t -n ' + str(num_iterations) + ' -n_test_folds ' + str(test_folds) + ' --notes_number ' + str(note_number) + ' --filter_type ' + str(filter_type) + ' --dataset_folder ' + str(dataset_folder) + debug
        self.exec_command(command=cmd)
        # Get last generated Graph and Model folder
        graph_folder = mg.get_last_graph_folder()
        model_folder = mg.get_last_models_folder()
        return graph_folder, model_folder

    def __upload_model_and_graph(self, work, model_filepath, graph_filepath):
        note_number = work.data['note_number']
        print(note_number)
        # Zip generated Graph and Model folder
        upload_zip_file = self.__get_upload_zip_file_of_note(note_number=note_number)
        cmd = commands.ZIP + ' -r ' + upload_zip_file + ' ' + graph_filepath + ' ' + model_filepath
        self.exec_command(command=cmd)
        # Upload zip file
        self.__upload_to_master(local_filepath=upload_zip_file,
                                host_location=master_configs.TRAINING_RESULTS_FOLDER)
        # Remove unnecessary files
        cmd = commands.DEL + ' ' + upload_zip_file
        self.exec_command(command=cmd)

    def __get_upload_zip_file_of_note(self, note_number):
        return os.path.join(configs.UPLOAD_FOLDER, str(note_number) + '.zip')

    def __get_DS_folder(self, note_number):
        return os.path.join(configs.NOTES_DATASETS_FOLDER, str(note_number))

    def __upload_to_master(self, local_filepath, host_location):
        if master_configs.SSH_HOST:
            cmd = 'scp ' + local_filepath + ' ' + master_configs.SSH_HOST + ':' + host_location
        else:
            cmd = 'copy ' + local_filepath + ' ' + host_location
        self.exec_command(cmd)

    def __download_from_master(self, host_filepath, local_filepath):
        if master_configs.SSH_HOST:
            cmd = 'scp ' + master_configs.SSH_HOST + ':' + host_filepath + ' ' + local_filepath
        else:
            cmd = 'copy ' + host_filepath + ' ' + local_filepath
        self.exec_command(cmd)
