import sys


# Windows configurations
NOTES_DATASETS_FOLDER = 'Z:\\master_datasets\\folds_configuration_1'
TRAINING_RESULTS_FOLDER = 'Z:\\results'

# Linux configurations
if sys.platform == 'linux':
    NOTES_DATASETS_FOLDER = '--'
    TRAINING_RESULTS_FOLDER = '--'

# Common configurations
NOTES_MODELS_TO_GENERATE = [78,76,74,72,70,69,68,67,66,65,64,63,62,60,59,58,57,56,55,54,49,43,42,41,40]
NUM_ITERATIONS = 80
TEST_FOLDS = 1
FILTER_TYPE = "mean_squared"
DEBUG = False

SSH_HOST = False
HOSTNAME = '10.20.141.18'
HOST_PORT = 8003
MESSAGE_LIMIT = 512  # Limit in bytes

ADDRESS = (HOSTNAME, HOST_PORT)