import socket
import threading
import os


from web.running_mode import RunningMode
import main_globals as mg
import web.msgs.types as msg_types
import web.master.configs as configs


class Master(RunningMode):

    def __init__(self):
        self.socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.notes_missing = configs.NOTES_MODELS_TO_GENERATE
        self.notes_on_progress = []
        self.notes_done = []
        self.condition = threading.Condition()

        if not os.path.isdir(configs.NOTES_DATASETS_FOLDER):
            os.makedirs(configs.NOTES_DATASETS_FOLDER)

        if not os.path.isdir(configs.TRAINING_RESULTS_FOLDER):
            os.makedirs(configs.TRAINING_RESULTS_FOLDER)

    def start(self):
        # Set up server settings
        self.socket.bind(configs.ADDRESS)
        self.socket.listen(5)
        # Waiting for connections
        self.__wait_for_connections()
        self.shutdown()

    def __wait_for_connections(self):
        mg.INFO('Waiting connections on ' + configs.HOSTNAME + ':' + str(configs.HOST_PORT))
        num_connections = 0
        while True:
            connection, client_address = self.socket.accept()
            t = threading.Thread(name='client_' + str(num_connections),
                                 target=self.__handle_slave,
                                 args=(connection, client_address))
            t.start()

    def __handle_slave(self, connection, client_address):
        while True:
            data = connection.recv(1024).decode()
            if not data:
                break

            msg_type = msg_types.get_slave_msg_type(data)
            if msg_type is None:
                mg.WARN('Unknown message type.')
            else:
                if msg_type == msg_types.NeedWorkMsg:
                    self.__slave_needs_work(connection=connection, client_address=client_address)
                elif msg_type == msg_types.FinishWorkMsg:
                    msg = msg_types.to_msg_type(data)
                    self.__slave_finish_work(msg=msg)
                    self.__slave_needs_work(connection=connection, client_address=client_address)

        connection.close()

    def __slave_needs_work(self, connection, client_address):
        note_number = self.__get_next_note_number()
        if note_number is None:  # Finish all notes number
            connection.sendall(msg_types.NoMoreWorkMsg().toBytes())
        else:
            note_ds = self.__get_DS(note_number=note_number)
            work_msg = msg_types.WorkMsg(dataset=note_ds,
                                         note_number=note_number,
                                         num_iterations=configs.NUM_ITERATIONS,
                                         test_folds=configs.TEST_FOLDS,
                                         filter_type=configs.FILTER_TYPE,
                                         new_ds=True,
                                         debug=configs.DEBUG)
            connection.sendall(work_msg.toBytes())

    def __slave_finish_work(self, msg):
        note_number = msg.data['note_number']
        self.condition.acquire()

        self.notes_on_progress.remove(note_number)
        self.notes_done.append(note_number)

        self.condition.release()

    def __get_next_note_number(self):
        note_number = None

        self.condition.acquire()
        if len(self.notes_missing) > 0:
            note_number = self.notes_missing.pop()
            self.notes_on_progress.append(note_number)

        self.condition.release()
        return note_number

    def __get_DS(self, note_number):
        ds_filepath = self.__get_DS_filepath_by_note_number(note_number=note_number)
        if not os.path.isfile(ds_filepath):
            self.__generate_DS(filepath=ds_filepath,
                               note_number=note_number)
        return ds_filepath

    def __get_DS_filepath_by_note_number(self, note_number):
        return os.path.join(configs.NOTES_DATASETS_FOLDER, str(note_number) + ".zip")

    def __generate_DS(self, filepath, note_number):
        return True
        # main(1, true, true, true, false, false, [55], true, false, false, 20, '/media/andre/AB4A-A1D0/Thesis/Database/70_musics/Test')
        # raise NotImplementedError
        # Generate Training DS

        # Generate Testing DS

        # Zip contents to filepath
        # Remove previous generated folder