

from web.msgs.msg import Msg


class NoMoreWorkMsg(Msg):

    def __init__(self, no_more_work=True):
        Msg.__init__(self=self, no_more_work=no_more_work)
