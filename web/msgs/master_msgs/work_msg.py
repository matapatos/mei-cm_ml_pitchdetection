
from web.msgs.msg import Msg


class WorkMsg(Msg):
    def __init__(self,
                 dataset,
                 note_number,
                 num_iterations,
                 test_folds,
                 filter_type,
                 new_ds=True,
                 debug=False):
        Msg.__init__(self=self,
                     dataset=dataset,
                     note_number=note_number,
                     num_iterations=num_iterations,
                     test_folds=test_folds,
                     filter_type=filter_type,
                     new_ds=new_ds,
                     debug=debug)
