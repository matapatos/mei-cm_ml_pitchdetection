

from web.msgs.msg import Msg


class FinishWorkMsg(Msg):

    def __init__(self, note_number, finish=True):
        Msg.__init__(self, note_number=note_number, finish=finish)
