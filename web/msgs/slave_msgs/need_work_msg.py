

from web.msgs.msg import Msg


class NeedWorkMsg(Msg):

    def __init__(self, need_work=True):
        Msg.__init__(self, need_work=need_work)