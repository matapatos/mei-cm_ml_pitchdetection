
import json


from web.msgs.msg import Msg
# Master messages
from web.msgs.master_msgs.no_more_work_msg import NoMoreWorkMsg
from web.msgs.master_msgs.work_msg import WorkMsg
# Slave messages
from web.msgs.slave_msgs.finish_work_msg import FinishWorkMsg
from web.msgs.slave_msgs.need_work_msg import NeedWorkMsg


all_master_msgs = [NoMoreWorkMsg, WorkMsg]
all_slave_msgs = [FinishWorkMsg, NeedWorkMsg]
all_msgs = all_master_msgs + all_slave_msgs


def to_msg_type(data_string):
    msg_type, json = __get_msg_type_and_json(data_string=data_string,
                                             msgs_collection=all_msgs)
    if msg_type is None:
        return None

    return msg_type(**json)


def get_slave_msg_type(data_string):
    return __get_msg_type(data_string=data_string,
                          msgs_collection=all_slave_msgs)


def get_master_msg_type(data_string):
    return __get_msg_type(data_string=data_string,
                          msgs_collection=all_master_msgs)


def __get_msg_type(data_string, msgs_collection):
    msg_type, _ = __get_msg_type_and_json(data_string=data_string,
                                          msgs_collection=msgs_collection)
    return msg_type


def __get_msg_type_and_json(data_string, msgs_collection):
    try:
        parsed_json = json.loads(data_string)
    except json.JSONDecodeError:
        return None, None

    json_keys = parsed_json.keys()
    for m_t in msgs_collection:
        var_names = m_t.__init__.__code__.co_varnames
        if __has_same_keys(json_keys=json_keys, var_names=var_names):
            return m_t, parsed_json

    return None, parsed_json


def __has_same_keys(json_keys, var_names):
    if len(json_keys) != (len(var_names) - 1):  # It's -1 because we want to discard the 'self' argument
        return False

    for k in json_keys:
        if k not in var_names:
            return False

    return True
