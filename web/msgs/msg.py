import json


class Msg():
    def __init__(self, **data):
        self.data = data

    def toString(self):
        return json.dumps(self.data)

    def toBytes(self):
        string_data = self.toString()
        return str.encode(string_data)
