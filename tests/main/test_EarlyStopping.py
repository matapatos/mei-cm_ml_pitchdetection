import logging
import nose


from pitch_estimation.auxiliary.training.early_stopping import EarlyStopping,\
                                                               EarlyStoppingException,\
                                                               BEST_MODEL_METRICS


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup(self, metric_name=None):
        if not metric_name:
            self.metric_name = BEST_MODEL_METRICS[0]

        self.early_stopping = EarlyStopping(num_iterations_after_best=3,
                                            min_percentage=0.5,
                                            metrics=self.metric_name)

    def teardown(self):
        # After every test function
        pass

    def test_if_stop_condition_does_not_stop_when_updating_best_model(self):
        for i in range(self.early_stopping.num_iterations_after_best + 2):
            self.early_stopping.update_best_model(metric_name=self.metric_name, value=self.early_stopping.min_percentage)
            self.early_stopping.increment_global_step()

    @nose.tools.raises(EarlyStoppingException)
    def test_stop_condition_when_global_step_incremented(self):
        self.early_stopping.update_best_model(metric_name=self.metric_name, value=self.early_stopping.min_percentage)  # Save best value
        try:
            for i in range(self.early_stopping.num_iterations_after_best + 1):
                self.early_stopping.increment_global_step()
        except EarlyStoppingException:
            self.setup()  # Reset variables.
            self.early_stopping.update_best_model(metric_name=self.metric_name, value=self.early_stopping.min_percentage + 1)  # Save best value
            for i in range(self.early_stopping.num_iterations_after_best + 1):
                self.early_stopping.increment_global_step()

    def test_does_not_stop_when_min_percentage_not_reached(self):
        self.early_stopping.update_best_model(metric_name=self.metric_name, value=self.early_stopping.min_percentage - 0.1)  # Save best value
        for i in range(self.early_stopping.num_iterations_after_best + 1):
            self.early_stopping.increment_global_step()

    def test_if_does_not_stop_when_updating_different_metric(self):
        wrong_metric_name = BEST_MODEL_METRICS[1]
        assert wrong_metric_name != self.metric_name

        self.early_stopping.update_best_model(metric_name=wrong_metric_name, value=self.early_stopping.min_percentage)  # Save best value
        for i in range(self.early_stopping.num_iterations_after_best + 1):
            self.early_stopping.increment_global_step()
