import logging
import inspect


from pitch_estimation.tasks.learner.filters.filter import Filter
from pitch_estimation.tasks.post_processing.types.single_nn import SingleNN
from pitch_estimation.tasks.post_processing.types.fix_onsets import FixOnsets
from pitch_estimation.tasks.post_processing.types.fix_offsets import FixOffsets
from pitch_estimation.tasks.post_processing.types.discard_notes import DiscardNotes
import main_globals as mg


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup(self, metric_name=None):
        pass

    def teardown(self):
        # After every test function
        pass

    def test_filter_default_optimizer(self):
        self.__check_if_parameter_of_a_given_function(class_=Filter,
                                                      func_name='define_optimizer',
                                                      parameter_name='self.optimizer',
                                                      supposed_value='adamoptimizer')

    def test_filter_default_lr(self):
        self.__check_if_parameter_of_a_given_function(class_=Filter,
                                                      func_name='define_NN_parameters_and_build_model',
                                                      parameter_name='self.learning_rate',
                                                      supposed_value='1e-6')

    def test_filter_default_model(self):
        self.__check_if_parameter_of_a_given_function(class_=Filter,
                                                      func_name='define_NN_parameters_and_build_model',
                                                      parameter_name='self.model',
                                                      supposed_value='FFTEficientMF_v2')

    def test_single_nn_default_optimizer(self):
        self.__check_if_parameter_of_a_given_function(class_=SingleNN,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.optimizer',
                                                      supposed_value='adamoptimizer')

    def test_single_nn_default_lr(self):
        self.__check_if_parameter_of_a_given_function(class_=SingleNN,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.learning_rate',
                                                      supposed_value='1e-5')

    def test_single_nn_default_model(self):
        self.__check_if_parameter_of_a_given_function(class_=SingleNN,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.model',
                                                      supposed_value='PPSingleNN')

    def test_fixed_onsets_default_optimizer(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOnsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.optimizer',
                                                      supposed_value='adamoptimizer')

    def test_fixed_onsets_default_lr(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOnsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.learning_rate',
                                                      supposed_value='1e-3')

    def test_fixed_onsets_default_model(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOnsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.model',
                                                      supposed_value='PPSingleNN')

    def test_fixed_offsets_default_optimizer(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOffsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.optimizer',
                                                      supposed_value='adamoptimizer')

    def test_fixed_offsets_default_lr(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOffsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.learning_rate',
                                                      supposed_value='1e-3')

    def test_fixed_offsets_default_model(self):
        self.__check_if_parameter_of_a_given_function(class_=FixOffsets,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.model',
                                                      supposed_value='PPSingleNN')

    def test_discard_notes_default_optimizer(self):
        self.__check_if_parameter_of_a_given_function(class_=DiscardNotes,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.optimizer',
                                                      supposed_value='adamoptimizer')

    def test_discard_notes_default_lr(self):
        self.__check_if_parameter_of_a_given_function(class_=DiscardNotes,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.learning_rate',
                                                      supposed_value='1e-5')

    def test_discard_notes_default_model(self):
        self.__check_if_parameter_of_a_given_function(class_=DiscardNotes,
                                                      func_name='__define_hyper_parameters',
                                                      parameter_name='self.model',
                                                      supposed_value='PPSingleNN')

    def test_main_globals_batch_size(self):
        assert mg.BATCH_SIZE == 1

    def test_main_globals_sequential(self):
        assert mg.SEQUENCIAL_MUSICS

    def __check_if_parameter_of_a_given_function(self, class_, func_name, parameter_name, supposed_value):
        parameter_name = str(parameter_name)
        stripped_lines = self.__get_stripped_source_lines_of_function(class_=class_,
                                                                      func_name=func_name,
                                                                      additional_strip=['#' + parameter_name + '='])
        lines = []
        for line in stripped_lines:
            if parameter_name + '=' in line:
                lines.append(line)

        assert len(lines) > 0
        if isinstance(supposed_value, list):
            for val in supposed_value:
                val = str(val)
                val = val.lower()
                assert val in lines[len(lines) - 1]
        else:
            supposed_value = str(supposed_value)
            supposed_value = supposed_value.lower()
            assert supposed_value in lines[len(lines) - 1]

    def __get_stripped_source_lines_of_function(self, class_, func_name, additional_strip=None):
        stripped_lines = []
        source_lines = self.__get_source_lines_of_function(class_, func_name)
        for line in source_lines:
            line = line.lower()
            line = line.rstrip("\t")
            line = line.replace(' ', '')
            if additional_strip:
                for a in additional_strip:
                    line.replace(a, '')
            stripped_lines.append(line)
        return stripped_lines

    def __get_source_lines_of_function(self, class_, func_name):
        if func_name not in class_.__dict__:
            func_name = '_' + str(class_.__name__) + func_name

        assert func_name in class_.__dict__
        func = class_.__dict__[func_name]
        return inspect.getsourcelines(func)[0]
