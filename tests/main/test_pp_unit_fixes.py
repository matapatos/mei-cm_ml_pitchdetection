import logging
import numpy as np


from pitch_estimation.tasks.post_processing.types.fix_onsets import FixOnsets
from pitch_estimation.tasks.post_processing.types.fix_offsets import FixOffsets
import tests.utils as utils
import pitch_estimation.auxiliary.metrics.utils as metrics_utils


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    @classmethod
    def setup_class(cls):
        np.random.seed(33)
        cls.notes_number = [55]

    @classmethod
    def teardown_class(cls):
        pass

    def setup(self):
        # Before every test function
        pass

    def teardown(self):
        # After every test function
        pass

    def test_fix_onset_pp_unit(self):
        '''Only tests function FixOnsets.__get_fix_onset_of_group
        The function that is responsible of telling the ANNs how to adapt the onset of a note.'''
        fix_onsets = FixOnsets(None, None, notes_number=self.notes_number)
        labels = [0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1]
        predictions = [0, 0, 0, 0.75, 0.75, 0.75, 0, 0, 0, 0.75, 0.75, 0.75, 0, 0.75, 0, 0.75, 0.75, 0, 0, 0, 0, 0, 0.75, 0.75, 0.75, 0.75, 0.75, 0, 0, 0, 0, 0, 0, 0, 0, 0.75, 0.75]
        labels_group = metrics_utils.get_notes_group(group=labels)
        predictions_group = metrics_utils.get_notes_group(group=predictions)
        correct_fixes = [-1, 0, 1, 1, 0, 0]
        for i, p in enumerate(predictions_group):
            fix = fix_onsets._FixOnsets__get_fix_onset_of_group(p, labels_group)
            assert correct_fixes[i] == fix

        assert i == len(correct_fixes) - 1

    def test_fix_offset_pp_unit(self):
        '''Only tests function FixOffsets.__get_fix_offset_of_group
        The function that is responsible of telling the ANNs how to adapt the offset of a note.'''
        fix_offsets = FixOffsets(None, None, notes_number=self.notes_number)
        labels = [0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1]
        predictions = [0, 0, 0, 0.8, 0.8, 0.8, 0, 0, 0, 0.8, 0.8, 0, 0, 0.8, 0, 0.8, 0.8, 0, 0, 0, 0, 0, 0.8, 0.8, 0.8, 0.8, 0.8, 0, 0, 0, 0, 0, 0, 0, 0, 0.8, 0.8, 0.8, 0, 0, 0.8, 0.8, 0.8, 0.8, 0, 0.8, 0.8, 0, 0, 0]
        labels_group = metrics_utils.get_notes_group(group=labels)
        predictions_group = metrics_utils.get_notes_group(group=predictions)
        correct_fixes = [0, 1, -1, 1, -1, -1, -1, 1]
        for i, p in enumerate(predictions_group):
            fix = fix_offsets._FixOffsets__get_fix_offset_of_group(p, labels_group)
            assert correct_fixes[i] == fix

        assert i == len(correct_fixes) - 1
