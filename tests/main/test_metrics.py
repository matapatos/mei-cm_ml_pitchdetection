import os
import tempfile
import logging
import numpy as np
import shutil
import copy
from nose.tools import assert_almost_equal


from pitch_estimation.tasks.post_processing.types.nn_per_note import NNPerNote
from pitch_estimation.tasks.learner.learner import Learner
import main_globals as mg
import tests.utils as utils


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    ASSERT_EPSILON = 1e-7

    @classmethod
    def setup_class(cls):
        np.random.seed(33)

        cls.main_folder = tempfile.gettempdir() + os.sep + 'fake_tests'
        cls.output_folderpath = os.path.join(cls.main_folder, 'output')
        cls.summaries_folderpath = os.path.join(cls.main_folder, 'graphs')
        if not os.path.isdir(cls.output_folderpath):
            os.makedirs(cls.output_folderpath)
        if not os.path.isdir(cls.summaries_folderpath):
            os.makedirs(cls.summaries_folderpath)

    @classmethod
    def teardown_class(cls):
        shutil.rmtree(cls.main_folder, ignore_errors=True)  # Recursively remove sub-folders too.

    def setup(self):
        # Before every test function
        pass

    def teardown(self):
        # After every test function
        pass

    def test_nn_per_note_single_note_frame_based_metrics(self):
        notes_number = [55]
        with NNPerNote(summary_output_folder=self.summaries_folderpath,
                       output_folderpath=self.output_folderpath,
                       notes_number=notes_number) as nn_per_note:
            self.__test_frame_based_with_single_note(metrics_container=nn_per_note, notes_number=notes_number)

    def test_nn_per_note_multiple_notes_frame_based(self):
        notes_number = [40, 55, 57, 60]
        with NNPerNote(summary_output_folder=self.summaries_folderpath,
                       output_folderpath=self.output_folderpath,
                       notes_number=notes_number) as nn_per_note:
            self.__test_frame_based_with_multiple_notes(metrics_container=nn_per_note, notes_number=notes_number)

    def test_learner_single_note_frame_based_metrics(self):
        notes_number = [55]
        with Learner(is_training=False,
                     summary_output_folder=self.summaries_folderpath,
                     filter_type='sigmoid',
                     notes_number=notes_number) as learner:
            self.__test_frame_based_with_single_note(metrics_container=learner, notes_number=notes_number)

    def test_learner_multiple_notes_frame_based(self):
        notes_number = [40, 55, 57, 60]
        with Learner(is_training=False,
                     summary_output_folder=self.summaries_folderpath,
                     filter_type='sigmoid',
                     notes_number=notes_number) as learner:
            self.__test_frame_based_with_multiple_notes(metrics_container=learner, notes_number=notes_number)

    def __test_frame_based_with_multiple_notes(self, metrics_container, notes_number):
        global_labels = []
        global_predictions = []
        fake_all_notes_results = utils.fake_all_notes_results(notes_number=notes_number, labels_threshold='random')
        for i in range(len(notes_number)):
            labels = fake_all_notes_results[i].labels
            predictions = fake_all_notes_results[i].predictions

            metrics_container.metrics.update_metrics(run_mode=mg.RunMode.PREDICTING,
                                                     note_result=fake_all_notes_results[i],
                                                     is_last_music=False)
            global_tf_metrics = utils.get_tf_metrics(metrics_controller=metrics_container.metrics,
                                                     type_='frame-based')

            global_labels.extend(labels)
            global_predictions.extend(predictions)
            precision, recall, fmeasure = utils.calculate_frame_based_metrics(labels=global_labels, predictions=global_predictions)
            assert_almost_equal(global_tf_metrics['precision'], precision, delta=self.ASSERT_EPSILON)
            assert_almost_equal(global_tf_metrics['recall'], recall, delta=self.ASSERT_EPSILON)
            assert_almost_equal(global_tf_metrics['fmeasure'], fmeasure, delta=self.ASSERT_EPSILON)

    def __test_frame_based_with_single_note(self, metrics_container, notes_number):
        # First fake music
        fake_all_notes_results_1 = utils.fake_all_notes_results(notes_number=notes_number)
        labels_1 = fake_all_notes_results_1[0].labels
        predictions_1 = fake_all_notes_results_1[0].predictions
        precision_1, recall_1, fmeasure_1 = utils.calculate_frame_based_metrics(labels=labels_1, predictions=predictions_1)
        metrics_container.metrics.update_metrics(run_mode=mg.RunMode.PREDICTING,
                                                 note_result=fake_all_notes_results_1[0],
                                                 is_last_music=False)
        metrics_tf_1 = utils.get_tf_metrics(metrics_controller=metrics_container.metrics,
                                            type_='frame-based')
        assert_almost_equal(metrics_tf_1['precision'], precision_1, delta=self.ASSERT_EPSILON)
        assert_almost_equal(metrics_tf_1['recall'], recall_1, delta=self.ASSERT_EPSILON)
        assert_almost_equal(metrics_tf_1['fmeasure'], fmeasure_1, delta=self.ASSERT_EPSILON)

        fake_all_notes_results_2 = utils.fake_all_notes_results(notes_number=notes_number, size=2500, labels_threshold=0.8)
        labels_2 = fake_all_notes_results_2[0].labels
        predictions_2 = fake_all_notes_results_2[0].predictions
        metrics_container.metrics.update_metrics(run_mode=mg.RunMode.PREDICTING,
                                                 note_result=fake_all_notes_results_2[0],
                                                 is_last_music=False)
        global_tf_metrics = utils.get_tf_metrics(metrics_controller=metrics_container.metrics,
                                                 type_='frame-based')

        global_labels = copy.deepcopy(labels_1)
        global_predictions = copy.deepcopy(predictions_1)
        global_labels = global_labels.tolist()
        global_predictions = global_predictions.tolist()
        global_labels.extend(labels_2)
        global_predictions.extend(predictions_2)
        precision, recall, fmeasure = utils.calculate_frame_based_metrics(labels=global_labels, predictions=global_predictions)
        assert_almost_equal(global_tf_metrics['precision'], precision, delta=self.ASSERT_EPSILON)
        assert_almost_equal(global_tf_metrics['recall'], recall, delta=self.ASSERT_EPSILON)
        assert_almost_equal(global_tf_metrics['fmeasure'], fmeasure, delta=self.ASSERT_EPSILON)
