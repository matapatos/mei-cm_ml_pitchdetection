import logging
import tempfile
import os
import shutil
import numpy as np
import pandas


from pitch_estimation.tasks.post_processing.types.write_output import WriteOutput
import main_globals as mg
import tests.utils as utils
from pitch_estimation.tasks.post_processing.utils import Variation


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def setup(self):
        self.output_folder = tempfile.gettempdir() + os.sep + 'fake_tests'
        if not os.path.isdir(self.output_folder):
            os.makedirs(self.output_folder)

        self.write_output = WriteOutput(summary_output_folder=self.output_folder)
        self.write_output.__enter__()

    def teardown(self):
        self.write_output.__exit__(None, None, None)

        if os.path.isdir(self.output_folder):
            shutil.rmtree(self.output_folder, ignore_errors=True)  # Recursively remove sub-folders too.

    def test_training_writing_single_output(self):
        self.__test_writing_single_output(run_mode=mg.RunMode.TRAINING,
                                          notes_number=[21, 55])

    def test_training_writing_multiple_outputs(self):
        self.__test_writing_multiple_output(run_mode=mg.RunMode.TRAINING,
                                            notes_number=[21, 55])

    def test_predicting_writing_single_output(self):
        self.__test_writing_single_output(run_mode=mg.RunMode.PREDICTING,
                                          notes_number=[21, 55])

    def test_predicting_writing_multiple_outputs(self):
        self.__test_writing_multiple_output(run_mode=mg.RunMode.PREDICTING,
                                            notes_number=[21, 55])

    def test_evaluating_writing_single_output(self):
        self.__test_writing_single_output(run_mode=mg.RunMode.EVALUATING,
                                          notes_number=[21, 55])

    def test_evaluating_writing_multiple_outputs(self):
        self.__test_writing_multiple_output(run_mode=mg.RunMode.EVALUATING,
                                            notes_number=[21, 55])

    def __test_writing_single_output(self, run_mode, notes_number):
        all_notes_results_1 = utils.fake_all_notes_results(notes_number=notes_number, size=10)
        self.write_output.apply(all_notes_results=all_notes_results_1,
                                run_mode=run_mode,
                                is_last_music=False,
                                global_metadata=None)
        self.__compare_all_notes_with_write_output_file(all_notes_results=all_notes_results_1,
                                                        run_mode=run_mode)

        all_notes_results_2 = utils.fake_all_notes_results(notes_number=notes_number, size=10, labels_threshold=0.4)
        self.write_output.apply(all_notes_results=all_notes_results_2,
                                run_mode=run_mode,
                                is_last_music=True,
                                global_metadata=None)
        self.__compare_all_notes_with_write_output_file(all_notes_results=[all_notes_results_1, all_notes_results_2],
                                                        run_mode=run_mode)

    def __test_writing_multiple_output(self, run_mode, notes_number):
        self.write_output.variations = [Variation(name='_also_', ref='fake_child_1'),
                                        Variation(name='_also_', ref='fake_child_2')]
        all_notes_results_1 = utils.fake_all_notes_results_with_children(notes_number=notes_number,
                                                                         num_children=2,
                                                                         size=10)
        self.write_output.apply(all_notes_results=all_notes_results_1,
                                run_mode=run_mode,
                                is_last_music=False,
                                global_metadata=None)
        self.__compare_all_notes_with_write_output_file(all_notes_results=all_notes_results_1,
                                                        run_mode=run_mode)

        all_notes_results_2 = utils.fake_all_notes_results_with_children(notes_number=notes_number,
                                                                         num_children=2,
                                                                         size=10)
        self.write_output.apply(all_notes_results=all_notes_results_2,
                                run_mode=run_mode,
                                is_last_music=True,
                                global_metadata=None)
        self.__compare_all_notes_with_write_output_file(all_notes_results=[all_notes_results_1, all_notes_results_2],
                                                        run_mode=run_mode)

    def __compare_all_notes_with_write_output_file(self, all_notes_results, run_mode):
        '''Function used to compare a single or multiple arrays of all_notes_results with the self.write_output.files'''

        if not isinstance(all_notes_results, list) and not isinstance(all_notes_results, np.ndarray):
            raise TypeError("Invalid type of argument all_notes_results")

        if len(all_notes_results) == 0:
            raise ValueError("Argument all_notes_results is empty")

        if not isinstance(all_notes_results[0], list) and not isinstance(all_notes_results[0], np.ndarray):
            all_notes_results = [all_notes_results]

        predictions_data, labels_data = self.__get_contents_from_file_of_write_output(run_mode=run_mode)
        size = (len(self.write_output.variations) + 1) * mg.NUMBER_OF_NOTES
        if predictions_data.shape[1] != size:
            raise ValueError('Invalid number of columns of predictions')

        if labels_data.shape[1] != mg.NUMBER_OF_NOTES:
            raise ValueError('Invalid number of columns of labels')

        for music_index, all_results in enumerate(all_notes_results):
            for note_results in all_results:
                size = utils.len_previous_note_results(note_results)
                prediction_start_note_index = 0
                while note_results:
                    self.__assert_note_results_with_readed_data(note_results=note_results,
                                                                predictions_data=predictions_data,
                                                                labels_data=labels_data,
                                                                music_index=music_index,
                                                                prediction_start_note_index=prediction_start_note_index)
                    prediction_start_note_index = size * mg.NUMBER_OF_NOTES
                    note_results = note_results.previous_note_results
                    size -= 1

    def __assert_note_results_with_readed_data(self, note_results, predictions_data, labels_data, music_index, prediction_start_note_index):
        predictions, labels = self.__get_predictions_and_labels_from_pandas_data(predictions_data=predictions_data,
                                                                                 labels_data=labels_data,
                                                                                 music_index=music_index,
                                                                                 note_number=note_results.note_number,
                                                                                 prediction_start_note_index=prediction_start_note_index)
        utils.assert_lists_almost_equal(note_results.labels.tolist(), labels)
        utils.assert_lists_almost_equal(note_results.predictions.tolist(), predictions)

    def __get_predictions_and_labels_from_pandas_data(self, predictions_data, labels_data, music_index, note_number, prediction_start_note_index):
        predictions = predictions_data[str(note_number + prediction_start_note_index)]
        labels = labels_data[str(note_number)]

        predictions = self.__get_parsed_array(predictions, music_index=music_index)
        labels = self.__get_parsed_array(labels, music_index=music_index)

        return predictions, labels

    def __get_parsed_array(self, a, music_index):
        data = []
        current_music_index = 0
        for p in a:
            if p == 'fake_filename' or pandas.isnull(p):
                continue
            elif p == -9 or p == '-9':
                current_music_index += 1
                continue
            elif music_index == current_music_index:
                data.append(float(p))
        return data

    def __get_contents_from_file_of_write_output(self, run_mode):
        predictions_file, labels_file = self.write_output._WriteOutput__get_files(run_mode=run_mode,
                                                                                  filter_type='fake_sigmoid')
        # Finish pending operations
        predictions_file.flush()
        labels_file.flush()
        predictions_data = pandas.read_csv(predictions_file.name)
        labels_data = pandas.read_csv(labels_file.name)
        return predictions_data, labels_data
