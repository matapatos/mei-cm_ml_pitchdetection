import numpy as np
from nose.tools import assert_almost_equal
import copy


from pitch_estimation.auxiliary.metrics.utils import ConfusionMatrix
from pitch_estimation.auxiliary.data.results.note_results import NoteResults
from pitch_estimation.auxiliary.data.metadata.local_metadata import LocalMetadata


np.random.seed(33)


def abstract_assert_music(hasNext,
                          input_data,
                          output_data,
                          local_meta):
    assert hasNext
    assert input_data
    assert output_data
    assert isinstance(local_meta, LocalMetadata)


def assert_no_music(hasNext,
                    input_data,
                    output_data,
                    local_meta):
    assert not hasNext
    assert not input_data
    assert not output_data
    assert not local_meta


def abstract_assert_notes_results(note_results):
    assert note_results
    assert note_results.creator_identifier
    assert note_results.filename
    assert note_results.note_number
    assert note_results.filter_type
    assert len(note_results.labels) > 0
    assert len(note_results.predictions) > 0
    assert np.array(note_results.labels).shape == (len(note_results.labels),)
    assert np.array(note_results.predictions).shape == (len(note_results.predictions),)


def assert_lists_almost_equal(list_1, list_2):
    assert len(list_1) == len(list_2)

    for l_1, l_2 in zip(list_1, list_2):
        assert_almost_equal(l_1, l_2)


def fully_assert_notes_results(note_results, expected_labels, expected_predictions):
    '''Only fully checks the labels and predictions'''
    abstract_assert_notes_results(note_results)
    assert note_results.labels.tolist() == expected_labels
    assert note_results.predictions.tolist() == expected_predictions


def calculate_frame_based_metrics(labels, predictions):
    if len(labels) != len(predictions):
        raise ValueError('Different size of predictions and labels.')

    predictions = np.copy(predictions)
    predictions = np.round(predictions)
    cf = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    for l, p in zip(labels, predictions):
        cf.update(label=l, pred=p)

    return (__precision(confusion_matrix=cf),
            __recall(confusion_matrix=cf),
            __f_measure(confusion_matrix=cf))


def get_tf_metrics(metrics_controller, type_=None):
    if type_:
        valid_types = ['frame-based', 'onset', 'onsets', 'onset-offset', None]
        if type_ not in valid_types:
            raise ValueError('\'type_\' argument must be one of the following types %s' % (str(valid_types)))

    all_metrics = metrics_controller.tensorboard_testing_iteration_metrics_queue.metrics
    metrics = {}
    for m in all_metrics:
        name = m.name.lower()
        if type_:
            if type_ == 'frame-based':
                if '_' in name:
                    continue
            elif type_ == 'onset' or type_ == 'onsets':
                if '_onset' not in name or 'offset' in name:
                    continue
            elif type_ == 'onset-offset':
                if '_onset_offset' not in name:
                    continue

        metrics[m.name] = m.metric_val

    operations = list(metrics.values())
    values = metrics_controller.container.session.run(operations)
    assert len(values) == len(metrics.keys())
    new_metrics = {}
    for metric_name, val in zip(metrics.keys(), values):
        metric_name = metric_name.lower()
        new_metrics[metric_name] = val

    return new_metrics


def fake_all_notes_results_with_children(notes_number, num_children, size=1000, labels_threshold=0.2):
    if not isinstance(num_children, int):
        raise TypeError('num_children argument must be type int type but %s given' % (type(num_children)))

    if num_children <= 0:
        raise ValueError('num_children must be a number higher than 0 but %s given' % (num_children))

    parent_all_notes_results = fake_all_notes_results(notes_number=notes_number,
                                                      size=size,
                                                      labels_threshold=labels_threshold,
                                                      creator_identifier='fake_parent')

    prev = parent_all_notes_results
    for i in range(num_children):
        child_all_notes_results = fake_all_notes_results(notes_number=notes_number,
                                                         size=size,
                                                         labels_threshold=labels_threshold,
                                                         creator_identifier='fake_child_' + str(num_children - i),
                                                         parent=parent_all_notes_results)

        __append_previous_all_notes_results(parent=prev, child=child_all_notes_results)
        prev = child_all_notes_results

    return parent_all_notes_results


def fake_all_notes_results(notes_number, size=1000, labels_threshold=0.2, creator_identifier='fake_classifiers', parent=None):
    if not isinstance(notes_number, list) and not isinstance(notes_number, np.ndarray):
        notes_number = [notes_number]

    if labels_threshold == 'random':
        labels_threshold = np.random.random_sample(size=len(notes_number))

    if not isinstance(labels_threshold, list) and not isinstance(labels_threshold, np.ndarray):
        labels_threshold = [labels_threshold] * len(notes_number)

    if len(notes_number) != len(labels_threshold):
        raise ValueError('Length mismatch between notes_number and labels_threshold')

    all_notes_results = []
    for n, threshold in zip(notes_number, labels_threshold):
        if parent is None:
            labels = np.random.random_sample(size=size)
            new_labels = []
            for x in labels:
                if x >= threshold:
                    x = 1.0
                else:
                    x = 0.0
                new_labels.append(x)
            labels = new_labels
        else:
            real_parent = None
            for p in parent:
                if p.note_number == n:
                    real_parent = p
            if real_parent is None:
                raise ValueError('No parent exists with the note number %d' % (n))
            labels = copy.deepcopy(real_parent.labels)

        predictions = np.random.random_sample(size=size)
        note_results = fake_note_results_from_frames(creator_identifier=creator_identifier,
                                                     note_number=n,
                                                     predictions=predictions,
                                                     labels=labels)
        all_notes_results.append(note_results)
    return all_notes_results


def fake_note_results_from_frames(creator_identifier, note_number, predictions, labels):
    return NoteResults(creator_identifier=creator_identifier,
                       filename='fake_filename',
                       note_number=note_number,
                       filter_type='fake_sigmoid',
                       labels=labels,
                       predictions=predictions,
                       losses=[])


def len_previous_note_results(note_results):
    if not isinstance(note_results, NoteResults):
        raise TypeError('Argument note_results must be of type NoteResults but %s given' % (type(note_results)))

    num = 0
    while note_results.previous_note_results:
        num += 1
        note_results = note_results.previous_note_results
    return num


def __append_previous_all_notes_results(parent, child):
    assert len(parent) == len(child)

    for n_p in parent:
        found = False
        for n_c in child:
            if n_p.note_number == n_c.note_number:
                n_p.previous_note_results = n_c
                found = True
                break

        if not found:
            raise Exception('Note results with note number %d not found in child' % (n_p.note_number))


def __f_measure(confusion_matrix):
        p = __precision(confusion_matrix)
        r = __recall(confusion_matrix)
        return 2 * ((p * r) / (p + r))


def __precision(confusion_matrix):
    return confusion_matrix.tp / (confusion_matrix.tp + confusion_matrix.fp)


def __recall(confusion_matrix):
    return confusion_matrix.tp / (confusion_matrix.tp + confusion_matrix.fn)
