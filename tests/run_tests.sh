# Run functional tests of training_pp_units
set -e # Essential to exit when some of the commands gets an error
nosetests tests/main/test_default_configurations.py:FunctionalTests -q
nosetests tests/training_pp_units/test_PPUnitsCSVDataImporter.py:FunctionalTests -q
nosetests tests/main/test_EarlyStopping.py:FunctionalTests -q
nosetests tests/main/test_metrics.py:FunctionalTests -q
nosetests tests/main/test_WriteOutput.py:FunctionalTests -q
nosetests tests/main/test_pp_unit_fixes.py:FunctionalTests -q
exit 0