import os
import logging


from training_pps_scripts.pp_units_reader import PPUnitsCSVDataImporter
from training_pp_units import get_note_results_from_data
import main_globals as mg
import tests.utils as utils


logging.getLogger('tensorflow').disabled = True  # Disable tensorflow logs


class FunctionalTests:

    @classmethod
    def setup_class(cls):
        # Beginning of the class tests
        # Set dataset folder
        cls.data_importer = cls.__get_initialized_data_importer('original', variations_size=0)
        cls.variations_size = 2
        cls.data_importer_also = cls.__get_initialized_data_importer('with_also', variations_size=cls.variations_size)

    @classmethod
    def teardown_class(cls):
        # In the end of the class tests
        cls.data_importer.__exit__(None, None, None)
        cls.data_importer_also.__exit__(None, None, None)

    def setup(self):
        # Before every test function
        pass

    def teardown(self):
        # After every test function
        self.data_importer.reset_reading_indexes()
        self.data_importer_also.reset_reading_indexes()

    def test_reading_CSV_for_training(self):
        hasNext, input_data, output_data, local_meta = self.data_importer.next_training_music()
        utils.abstract_assert_music(hasNext=hasNext, input_data=input_data, output_data=output_data, local_meta=local_meta)

        hasNext, input_data, output_data, local_meta = self.data_importer.next_training_music()
        utils.assert_no_music(hasNext=hasNext, input_data=input_data, output_data=output_data, local_meta=local_meta)

        hasNext, input_data, output_data, local_meta = self.data_importer.next_evaluation_music()
        utils.abstract_assert_music(hasNext=hasNext, input_data=input_data, output_data=output_data, local_meta=local_meta)

        hasNext, input_data, output_data, local_meta = self.data_importer.next_evaluation_music()
        utils.assert_no_music(hasNext=hasNext, input_data=input_data, output_data=output_data, local_meta=local_meta)

    def test_get_note_results_from_training_pp_units(self):
        # Training music
        expected_labels = [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]
        expected_predictions = [0.2, 0.5, 1, 1, 0.6, 0.9, 0.8, 0, 0, 0, 0, 0, 0, 0]
        hasNext, input_data, output_data, local_meta = self.data_importer.next_training_music()
        note_results = get_note_results_from_data(input_data=input_data,
                                                  labels=output_data,
                                                  local_meta=local_meta,
                                                  notes_number=[55],
                                                  variation_level=0,
                                                  variations_size=0)
        utils.fully_assert_notes_results(note_results=note_results['sigmoid'][0],
                                         expected_labels=expected_labels,
                                         expected_predictions=expected_predictions)

        # Testing music
        expected_predictions = [0.1, 0.5, 0.9, 1, 0.6, 0.9, 0.8, 0, 0, 0, 0, 0, 0, 0]
        hasNext, input_data, output_data, local_meta = self.data_importer.next_evaluation_music()
        note_results = get_note_results_from_data(input_data=input_data,
                                                  labels=output_data,
                                                  local_meta=local_meta,
                                                  notes_number=[55],
                                                  variation_level=0,
                                                  variations_size=0)
        utils.fully_assert_notes_results(note_results=note_results['sigmoid'][0],
                                         expected_labels=expected_labels,
                                         expected_predictions=expected_predictions)

    def test_get_note_results_from_training_pp_units_with_also(self):
        # Training music
        expected_labels = [1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]
        expected_predictions = [0.2, 0.5, 1, 1, 0.6, 0.9, 0.8, 0, 0, 0, 0, 0, 0, 0]
        hasNext, input_data, output_data, local_meta = self.data_importer_also.next_training_music()
        for i in range(self.variations_size + 1):
            note_results = get_note_results_from_data(input_data=input_data,
                                                      labels=output_data,
                                                      local_meta=local_meta,
                                                      notes_number=[55],
                                                      variation_level=i,
                                                      variations_size=self.variations_size)
            utils.fully_assert_notes_results(note_results=note_results['sigmoid'][0],
                                             expected_labels=expected_labels,
                                             expected_predictions=expected_predictions)
            first = expected_predictions[0]
            expected_predictions = expected_predictions[1:]
            expected_predictions.append(first)

        # Testing music
        expected_predictions = [0.1, 0.5, 0.9, 1, 0.6, 0.9, 0.8, 0, 0, 0, 0, 0, 0, 0]
        hasNext, input_data, output_data, local_meta = self.data_importer_also.next_evaluation_music()
        for i in range(self.variations_size + 1):
            note_results = get_note_results_from_data(input_data=input_data,
                                                      labels=output_data,
                                                      local_meta=local_meta,
                                                      notes_number=[55],
                                                      variation_level=i,
                                                      variations_size=self.variations_size)
            utils.fully_assert_notes_results(note_results=note_results['sigmoid'][0],
                                             expected_labels=expected_labels,
                                             expected_predictions=expected_predictions)
            first = expected_predictions[0]
            expected_predictions = expected_predictions[1:]
            expected_predictions.append(first)

    def __get_initialized_data_importer(relative_ds_folder, variations_size, is_training=True):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        dataset_folder = os.path.join(dir_path, '..' + os.sep + 'testing_dataset' + os.sep + str(relative_ds_folder))
        mg.SET_DATASET_FOLDER(dataset_folder)

        data_importer = PPUnitsCSVDataImporter(is_training=is_training, variations_size=variations_size)
        data_importer.__enter__()
        return data_importer
