Machine Learning, differences between classification and regression:
	https://www.youtube.com/watch?v=i04Pfrb71vk


Understand activation functions in Machine Learning, and why it's necessary:
	https://www.youtube.com/watch?v=-7scQpJT7uo -> The basic ideia is that without them the output would be always a linear function, and in Machine Learning we what to solve all kind of problems such as non linear problems. 

What is logits in ML:
	https://stackoverflow.com/questions/34240703/difference-between-tensorflow-tf-nn-softmax-and-tf-nn-softmax-cross-entropy-with

Types of error:
	Relative and absolute error:
		https://www.youtube.com/watch?v=9jDsHQExlkc
		
	Root mean absolute error:
		https://www.youtube.com/watch?v=zMFdb__sUpw

	Mean absolute error:
		https://www.youtube.com/watch?v=yXoxdXMvD7c&t=213s

	Correlation of coefficient:
		https://www.youtube.com/watch?v=ugd4k3dC_8Y&t=341s (Explaining how it works)
		https://www.youtube.com/watch?v=u4ugaNo6v1Q (Calculate)

	Root relative squared error:
		https://stats.stackexchange.com/questions/131267/how-to-interpret-error-measures-in-weka-output
