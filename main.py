'''
File that contains the main functions of the ML program
'''
import argparse
import os
import sys
import time


import main_globals as mg
import utils as mg_utils
import debug_tools
import pitch_estimation.tasks.learner.filters.types.all as filter_types
from web.slave.slave import Slave
from web.master.master import Master
from pitch_estimation.auxiliary.metrics.types.metrics import BEST_MODEL_METRICS
import pitch_estimation.tasks.post_processing.types.all as post_processing
from pitch_estimation.controller import PitchEstimationController


class Main():

    def start(self):
        args = self.__parse_arguments()

        mg.SET_DATASET_FOLDER(dataset_folder=args.dataset_folder)
        if args.debug:  # Warning that debug is on
            mg.WARN("Mode DEBUG enabled!")
            mg.ENABLE_DEBUG()

        if args.floydhub:  # Warning that floydhub mode is enabled
            mg.ENABLE_FLOYD_HUB_MODE()

        if args.models_folder == 'last':
            args.models_folder = mg_utils.get_last_models_folder()

        start_time = time.time()
        if args.training:
            num_iterations = int(args.num_iterations)
            testing_iteration_folds = int(args.num_test_folds)
            if testing_iteration_folds == 0:
                testing_iteration_folds = sys.maxsize  # To be able to ignore testing folds

            pitch_estimation_controller = PitchEstimationController(save_output=False)
            pitch_estimation_controller.train(notes_number=args.notes_number,
                                              num_iterations=num_iterations,
                                              testing_iteration_folds=testing_iteration_folds,
                                              models_folder=args.models_folder,
                                              model_iteration=args.model_iteration,
                                              filter_type=args.filter_type,
                                              upload=args.upload,
                                              post_processes=args.post_processing,
                                              names_post_processes=args.names_post_processing,
                                              best_model=args.best_model,
                                              debug=args.debug)
        elif args.predicting:
            pitch_estimation_controller = PitchEstimationController(save_output=True)
            pitch_estimation_controller.predict(notes_number=args.notes_number,
                                                models_folder=args.models_folder,
                                                model_iteration=args.model_iteration,
                                                filter_type=args.filter_type,
                                                best_model=args.best_model,
                                                post_processes=args.post_processing,
                                                names_post_processes=args.names_post_processing,
                                                debug=args.debug)
        elif args.slave:
            self.slave = Slave()
            self.slave.start()

        elif args.master:
            self.master = Master()
            self.master.start()

        mg.INFO("The whole process took %s" % (debug_tools.get_operation_time_from_start_time(start_time=start_time)))

    def __parse_arguments(self):
        parser = argparse.ArgumentParser(description='Available arguments:')

        algo_group = parser.add_mutually_exclusive_group(required=True)
        algo_group.add_argument('-t', '-T', '--training',
                                help='Run the training algorithm',
                                action='store_const',
                                const=True)
        algo_group.add_argument('-p', '-P', '--predicting',
                                help='Run the prediction algorithm',
                                action='store_const',
                                const=True)
        algo_group.add_argument('-slave', '-SLAVE', '--slave',
                                help='Run software as a slave',
                                action='store_const',
                                const=True)
        algo_group.add_argument('-master', '-MASTER', '--master',
                                help='Run software as a master',
                                action='store_const',
                                const=True)
        # Training options
        parser.add_argument('-n', '-N', '--num_iterations',
                            help='Number of iterations that the algorithm should run',
                            default=20)
        parser.add_argument('-n_test_folds', '-N_TEST_FOLDS', '--num_test_folds',
                            help='Number of iterations that the algorithm should run the training dataset before running the testing dataset',
                            default=1)
        parser.add_argument('-upload', '-UPLOAD', '--upload',
                            help='Upload zipped results to specified directory',
                            type=str)
        # Prediction options
        parser.add_argument('-m', '-M', '--models_folder',
                            help='Folder that contains all the trained models')

        parser.add_argument('-m_i', '-M_I', '--model_iteration',
                            help='Model iteration number to be used',
                            default=None)

        parser.add_argument('-b_m', '-B_M', '--best_model',
                            help='Best model by metric name',
                            default=None,
                            type=str)

        parser.add_argument('-pp', '-PP', '--post_processing',
                            help='Post processing tools',
                            default=[],
                            type=str,
                            nargs='+')

        # Mutual options
        parser.add_argument('-d', '-D', '--debug',
                            help='For debug purposes',
                            action='store_const',
                            const=True,
                            default=False)

        parser.add_argument('-f', '-F', '--filter_type',
                            help='Type of filter to be used',
                            default='sigmoid',
                            type=str)

        parser.add_argument('-fh', '-FH', '--floydhub',
                            help='Option to be used in FloydHub website',
                            action='store_const',
                            const=True,
                            default=False)

        parser.add_argument('-no', '-NO', '--notes_number',
                            help='Notes to be detected by the NN',
                            default=[],
                            type=str,
                            nargs='+')

        parser.add_argument('-ds', '-DS', '--dataset_folder',
                            help='Dataset folder',
                            default=mg.DATASET_FOLDER,
                            type=str)

        args = parser.parse_args()

        self.__parse_training_and_prediction_args(args=args)

        return args

    def __parse_training_and_prediction_args(self, args):
        if args.predicting is None and args.training is None:
            return False

        error_message = None
        if args.predicting:
            usage = 'usage: ' + __file__ + ' -p -m [MODEL] --model_iteration [ITERATION_NUMBER] --notes_number [NOTE NUMBER]  --dataset [OPTIONAL]\n\n'
            if args.models_folder is None:
                error_message = usage + '-m/-M/--models_folder is required when running prediction algorithm'

            elif args.models_folder != 'last' and not os.path.isdir(args.models_folder):
                error_message = usage + '-m/-M/--models_folder must be an existent directory or \'last\' in case you want to run the last generated model'

            if args.model_iteration is not None and args.model_iteration != 'last' and not self.__is_positive_int(args.model_iteration):
                error_message = usage + '-m_i/-M_I/--model_iteration must be a positive number bigger than 0 or \'last\' in case that you want to use the last iteration number of the models generated'

            if args.upload:
                mg.WARN('\'upload\' argument discarded because it\'s only used in training mode.')

        else:
            usage = 'usage: ' + __file__ + ' -n [number of iterations] -n_test_folds [number of folds] --notes_number [NOTE NUMBER] --models_folder [OPTIONAL] --model_iteration [OPTIONAL] --dataset [OPTIONAL]\n\n'
            if args.num_iterations:
                if not mg_utils.is_positive_int(args.num_iterations):
                    error_message = usage + __file__ + ': error: [number of iterations] must be a number bigger than 0'

            if args.num_test_folds:
                if not mg_utils.is_zero_or_positive_int(args.num_test_folds):
                    error_message = usage + __file__ + ': error: [number of folds] must be a number equal or bigger than 0'
                elif int(args.num_test_folds) > int(args.num_iterations):
                    mg.WARN("Number of testing folds is bigger than the number of iterations, so the data will not run any testing data.")

            if not args.models_folder:
                if post_processing.has_any_trainable_unit(args.post_processing):
                    mg.ERROR('Training classifiers and post-processing units at the same time is not supported yet!\n\nPlease train a classifier first and then add its folder using the option -m/-M/--models_folder and the best model metric to use using option -b_m/-B_M/--best_model')
            else:
                if args.models_folder != 'last' and not os.path.isdir(args.models_folder):
                    error_message = usage + '-m/-M/--models_folder must be an existent directory or \'last\' in case you want to run the last generated model'

                elif args.model_iteration is not None and args.model_iteration != 'last' and not mg_utils.is_positive_int(args.model_iteration):
                    error_message = usage + '-m_i/-M_I/--model_iteration must be a positive number bigger than 0 or \'last\' in case that you want to use the last iteration number of the models generated'

                elif args.best_model is not None:
                    args.best_model = args.best_model.lower()
                    if args.best_model not in BEST_MODEL_METRICS:
                        error_message = usage + '-b_m/-B_M/--best_model must be one of the following values ' + str(BEST_MODEL_METRICS) + '.'

            if args.upload and not os.path.isdir(args.upload):
                error_message = usage + '-upload/-UPLOAD/--upload must be an existent directory'

        if not args.notes_number:
            error_message = usage + __file__ + ': error: [notes_number] must be at least one note number between or equal to 21-109, silence or all'

        else:
            if 'silence' not in args.notes_number and 'all' not in args.notes_number:
                if not mg_utils.is_number_between(numbers=args.notes_number,
                                                  min_inclusive=mg.FIRST_NOTE_NUMBER,
                                                  max_inclusive=mg.FIRST_NOTE_NUMBER + mg.NUMBER_OF_NOTES):
                    error_message = usage + __file__ + ': error: [notes_number] must be at least one note number between or equal to 21-109, silence or all'

        if args.filter_type not in filter_types.all_filters_names:
            filters_names = mg_utils.array_to_string(filter_types.all_filters_names)
            error_message = usage + __file__ + ': error: [filter_type] must be one of the following values: ' + filters_names

        if not os.path.isdir(args.dataset_folder):
            error_message = usage + __file__ + ': [dataset_folder] must be an existent folder.'

        if args.post_processing:
            args.post_processing, args.names_post_processing = post_processing.get_pps_from_names(args.post_processing)
            if args.post_processing is None:
                all_pps_names = mg_utils.array_to_string(post_processing.all_pps_names)
                error_message = usage + __file__ + ': [post_processing] must be only the following values: ' + all_pps_names
        else:
            args.names_post_processing = None

        if error_message is not None:
            print(error_message)
            sys.exit(-1)

        return True


if __name__ == '__main__':
    main = Main()
    main.start()
