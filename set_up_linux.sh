install_command_if_not_exists() {
    if hash "$1" 2>/dev/null; then
       return 
    else
        if "$1" == "conda"; then
          wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
          chmod +x Miniconda3-latest-Linux-x86_64.sh
          ./Miniconda3-latest-Linux-x86_64.sh
          rm Miniconda3-latest-Linux-x86_64.sh
        fi
    fi
}

install_command_if_not_exists "conda"
conda create -n pitchestimation python=3.6 -y
# Following code based from https://raw.githubusercontent.com/tensorflow/magenta/master/magenta/tools/magenta-install.sh
set +e
source activate pitchestimation
set -e
if [[ $(conda info --envs | grep "*" | awk '{print $1}') != "pitchestimation" ]]; then
  err 'Did not successfully activate the pitchestimation conda environment'
fi

# Installing dependencies
pip install -r requirements.txt
gksudo apt-get install build-essential libasound2-dev libjack-dev

echo $'\n\nSet up completed!\nNow activate the conda environment by executing the following command:\n - conda activate pitchestimation'