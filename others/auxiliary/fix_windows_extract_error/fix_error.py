import os
import shutil
import time


FOLDER_PATH = '/home/andre-gtx/Desktop/Thesis/Final_DS/fold_4/models/step_3'
notes_number = 'ALL'


def __move_dir(old_filepath, new_filepath):
    print("INFO: Move %s to %s" % (old_filepath, new_filepath))
    if os.path.isdir(new_filepath):
        print("\tRemoving %s" % (new_filepath))
        shutil.rmtree(new_filepath)
        while True:
            try:
                os.makedirs(new_filepath)
            except:
                time.sleep(1)
            else:
                os.removedirs(new_filepath)
                break

    shutil.copytree(src=old_filepath, dst=new_filepath)
    shutil.rmtree(old_filepath)


if type(notes_number) == str:
    notes_number = range(21, 106)

if type(notes_number) != range:
    raise TypeError('Invalid type of notes_number')

for note in notes_number:
    folder_path = os.path.join(FOLDER_PATH, str(note))
    folder_path += os.sep + "others" + os.sep + "output"
    if not os.path.isdir(folder_path):
        raise ValueError('Missing directory %s' % (folder_path))
    files = os.listdir(folder_path)
    files.sort()
    if len(files) > 1:
        if len(files) != 2:
            raise ValueError('Too many files in the directory %s. Files: %s' % (folder_path, files))

        filepath_with_correct_data = os.path.join(folder_path, files[1])
        destination_filepath = os.path.join(folder_path, files[0])
        __move_dir(filepath_with_correct_data, destination_filepath)

    
