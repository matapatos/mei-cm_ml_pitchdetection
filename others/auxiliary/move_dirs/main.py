import os
import sys
import shutil
import ntpath
import errno


dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + os.sep + '..' + os.sep + '..' + os.sep + '..')
import main_globals as mg
import utils


CLASSIFIER_DIRECTORY = '/home/andre/Desktop/Thesis/ahah/classifiers'
STEP_1_DIRECTORY = '/home/andre/Desktop/Thesis/ahah/step_1'


def main():
    global CLASSIFIER_DIRECTORY

    files = os.listdir(CLASSIFIER_DIRECTORY)
    for f in files:
        if utils.is_number_between(numbers=f, min_inclusive=mg.FIRST_NOTE_NUMBER, max_inclusive=mg.FIRST_NOTE_NUMBER + mg.NUMBER_OF_NOTES - 1):
            directory = __get_directory_from_classifier_to_be_moved(note_number=int(f))
            to = __get_output_directory_of_step_1(note_number=int(f))
            __move_dir(directory=directory, to=to)


def __get_directory_from_classifier_to_be_moved(note_number):
    global CLASSIFIER_DIRECTORY
    note_number_dir = CLASSIFIER_DIRECTORY + os.sep + str(note_number) + os.sep + 'output'
    if not os.path.isdir(note_number_dir):
        note_number_dir = CLASSIFIER_DIRECTORY + os.sep + str(note_number) + os.sep + 'others' + os.sep + 'output'
        if not os.path.isdir(note_number_dir):
            raise FileNotFoundError('Directory %s does not exists' % (note_number_dir))

    last_folder = utils.get_last_folder_from_folder(main_folder=note_number_dir)
    last_folder = os.path.join(last_folder, 'sigmoid')
    if not os.path.isdir(last_folder):
        raise FileNotFoundError('Directory %s does not exists' % (last_folder))
    return last_folder



def __get_output_directory_of_step_1(note_number):
    global STEP_1_DIRECTORY
    note_number_dir = STEP_1_DIRECTORY + os.sep + str(note_number) + os.sep + 'others' + os.sep + 'output'
    if not os.path.isdir(note_number_dir):
        raise FileNotFoundError('Directory %s does not exists' % (note_number_dir))

    last_folder = utils.get_last_folder_from_folder(main_folder=note_number_dir)
    if not os.path.isdir(last_folder):
        raise FileNotFoundError('Directory %s does not exists' % (last_folder))
    return last_folder


def __move_dir(directory, to):
    dirname = ntpath.basename(directory)
    to = os.path.join(to, dirname)
    if not os.path.isdir(to):
        print("INFO: Moving %s to %s" % (directory, to))
        shutil.copytree(src=directory, dst=to)


if __name__ == "__main__":
    main()
