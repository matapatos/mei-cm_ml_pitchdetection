import os
import sys
import shutil
import ntpath


dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + os.sep + '..' + os.sep + '..' + os.sep + '..')
import main_globals as mg
import utils


CLASSIFIER_DIRECTORY = '/home/andre-gtx/Desktop/Thesis/Final_DS/fold_1/models/classifiers'
STEP_1_DIRECTORY = '/home/andre-gtx/Desktop/Thesis/Final_DS/fold_1/models/step_1'


def main():
    global CLASSIFIER_DIRECTORY

    files = os.listdir(CLASSIFIER_DIRECTORY)
    for f in files:
        if utils.is_number_between(numbers=f, min_inclusive=mg.FIRST_NOTE_NUMBER, max_inclusive=mg.FIRST_NOTE_NUMBER + mg.NUMBER_OF_NOTES - 1):
            classifier_dir = __get_directory_from_classifier_to_be_copied(note_number=int(f))
            dst = __get_output_directory_of_step_1(note_number=int(f))
            __copy_dir(directory=classifier_dir, dst=dst)


def __get_directory_from_classifier_to_be_copied(note_number):
    global CLASSIFIER_DIRECTORY
    last_folder = __get_directory_from_note_number(main_folder=CLASSIFIER_DIRECTORY,
                                                   note_number=note_number)
    last_folder = os.path.join(last_folder, 'sigmoid')
    if not os.path.isdir(last_folder):
        raise FileNotFoundError('Directory %s does not exists' % (last_folder))
    return last_folder


def __get_output_directory_of_step_1(note_number):
    global STEP_1_DIRECTORY
    return __get_directory_from_note_number(main_folder=STEP_1_DIRECTORY,
                                            note_number=note_number)


def __get_directory_from_note_number(main_folder, note_number):
    note_number_dir = main_folder + os.sep + str(note_number) + os.sep + 'output'
    if os.path.isdir(note_number_dir):
        if os.path.isdir(os.path.join(note_number_dir, 'sigmoid')):
            return note_number_dir

    if not os.path.isdir(note_number_dir):
        note_number_dir = main_folder + os.sep + str(note_number) + os.sep + 'others' + os.sep + 'output'
        if not os.path.isdir(note_number_dir):
            raise FileNotFoundError('Directory %s does not exists' % (note_number_dir))

    last_folder = utils.get_last_folder_from_folder(main_folder=note_number_dir)
    if not os.path.isdir(last_folder):
        raise FileNotFoundError('Directory %s does not exists' % (last_folder))
    return last_folder


def __copy_dir(directory, dst):
    dirname = ntpath.basename(directory)
    dst = os.path.join(dst, dirname)
    if not os.path.isdir(dst):
        print("INFO: Copy %s to %s" % (directory, dst))
        shutil.copytree(src=directory, dst=dst)


if __name__ == "__main__":
    main()
