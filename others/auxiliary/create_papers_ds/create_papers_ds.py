'''
Pseudo code:

# For each fold (i)
    # Read train and test fold (i)
        # For each line (filename) move filename to output folder
'''
import os
import collections
import logging
import sys
import shutil


Fold = collections.namedtuple('Fold', 'id_ train test')
logger = logging.getLogger(name=__name__)
logger.setLevel(logging.WARNING)


CURRENT_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

FOLDS_FOLDER = os.path.join(CURRENT_SCRIPT_DIR, 'configuration_1')
OUTPUT_FOLDER = '/media/andre/AB4A-A1D0/Thesis/Database/configuration_1'
FROM_MUSICS_DS_FOLDER = '/media/andre/AB4A-A1D0/Thesis/Database/All_Musics'  # None if it's the same folder as this script run otherwise select the folder that contains the musics


def __match_filenames(filenames, match):
    if not match:
        return filenames

    matching_filenames = []
    for f in filenames:
        if match in f:
            matching_filenames.append(f)
    return matching_filenames


def __get_folds(folder):
    if not folder or not os.path.isdir(folder):
        logging.error('The following folder doesn\'t exists \'%r\'' % (folder))
        sys.exit(-1)

    filenames = os.listdir(folder)
    train_filenames = __match_filenames(filenames=filenames,
                                        match='train_fold_')
    folds = []
    for tr_f in train_filenames:
        num = tr_f.split('train_fold_')[1]
        num = num.replace('.txt', '')
        test_filepath = os.path.join(folder, 'test_fold_' + str(num) + '.txt')
        if os.path.isfile(test_filepath):
            tr_f = os.path.join(folder, tr_f)
            folds.append(Fold(id_=num,
                              train=tr_f,
                              test=test_filepath))
        else:
            logging.warning('No testing fold denominated as %r in folder %r' % (test_filepath, folder))

    return folds


def __read_lines_from_file(filepath):
    lines = []
    with open(filepath, 'r') as f:
        lines = f.readlines()
    return lines


def __copy_files(filenames, to_, from_=None):
    if not os.path.isdir(to_):
        os.makedirs(to_)

    if from_:  # Append from folder
        for i in range(len(filenames)):
            filenames[i] = os.path.join(from_, filenames[i])

    for f in filenames:
        output_filename = os.path.basename(f)
        output_filepath = os.path.join(to_, output_filename)
        f = f.replace('\n', '')
        output_filepath = output_filepath.replace('\n', '')
        f = f.split('.')[0]
        output_filepath = output_filepath.split('.')[0]
        # Copy .txt file
        shutil.copyfile(f + '.txt', output_filepath + '.txt')
        # Copy .wav file
        shutil.copyfile(f + '.wav', output_filepath + '.wav')
        # Copy .wav file
        shutil.copyfile(f + '.mid', output_filepath + '.mid')


def __generate_ds_from_folds(folds_folder, output_folder, from_=None):
    all_folds = __get_folds(folder=folds_folder)
    for fold in all_folds:
        fold_output_folder = os.path.join(output_folder, 'fold_' + str(fold.id_))
        # Generate training set
        training_set = __read_lines_from_file(fold.train)
        training_set_output_folder = os.path.join(fold_output_folder, 'train')
        __copy_files(filenames=training_set,
                     to_=training_set_output_folder,
                     from_=from_)
        # Generate testing set
        testing_set = __read_lines_from_file(fold.test)
        testing_set_output_folder = os.path.join(fold_output_folder, 'test')
        __copy_files(filenames=testing_set,
                     to_=testing_set_output_folder,
                     from_=from_)


if __name__ == '__main__':
    if not os.path.isdir(OUTPUT_FOLDER):
        os.makedirs(OUTPUT_FOLDER)

    __generate_ds_from_folds(folds_folder=FOLDS_FOLDER,
                             output_folder=OUTPUT_FOLDER,
                             from_=FROM_MUSICS_DS_FOLDER)
