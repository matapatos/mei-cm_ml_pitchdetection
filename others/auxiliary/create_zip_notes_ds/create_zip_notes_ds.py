
import os
import collections
import logging
import sys


# TRAINING_FILES_FORLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/specific_frames_datasets'
# TESTING_FILES_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/testing'
# OUTPUT_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/fold_1'


TRAINING_FILES_FORLDER = '/media/andre/Maxtor/Configuration_1/fold_4_o'
TESTING_FILES_FOLDER = '/home/andre/Desktop/Thesis/mei-cm_ml_io_generator/output/testing'
OUTPUT_FOLDER = '/home/andre/Desktop/Thesis/mei-cm_ml_io_generator/output/yey'

TrainingSet = collections.namedtuple('TrainingSet', 'note_number input output local_metadata global_metadata configs')
TestingSet = collections.namedtuple('TestingSet', 'input output local_metadata global_metadata configs possible_notes onsets')


logger = logging.getLogger(name=__name__)
logger.setLevel(logging.WARNING)


def __move_file(filepath, to_filepath):
    cmd = 'mv "' + filepath + '" "' + to_filepath + '"'
    __exec_command(cmd)


def __get_training_set_from_filenames(folder, filenames, note_number):
    for i, f in enumerate(filenames):  # Append folder path
        current_filepath = os.path.join(folder, f)
        new_folder = os.path.join(folder, note_number)
        if not os.path.isdir(new_folder):
            os.makedirs(new_folder)

        if 'input' in f:
            f = 'train.csv'
        else:
            f = f.replace(str(note_number) + '_', 'train_')

        new_filepath = os.path.join(new_folder, f)
        __move_file(filepath=current_filepath,
                    to_filepath=new_filepath)
        filenames[i] = new_filepath

    input_filepath = None
    output_filepath = None
    local_metadata = None
    global_metadata = None
    configs = None
    for f in filenames:
        if 'train.csv' in f:
            input_filepath = f
        elif 'train_configs.txt' in f:
            configs = f
        elif 'train_global_metadata.csv' in f:
            global_metadata = f
        elif 'train_local_metadata.csv' in f:
            local_metadata = f
        elif 'train_output.csv' in f:
            output_filepath = f

    return TrainingSet(note_number=note_number,
                       input=input_filepath,
                       output=output_filepath,
                       local_metadata=local_metadata,
                       global_metadata=global_metadata,
                       configs=configs)


def __get_training_sets_from_folder(folder):
    if not os.path.isdir(folder):
        logger.error('The following folder doesn\'t exists' % (folder))
        sys.exit(-1)

    filenames = os.listdir(folder)
    training_sets = []
    training_sets_generator = __get_next_training_set(folder=folder, filenames=filenames)
    for t in training_sets_generator:
        training_sets.append(t)

    return training_sets


def __get_next_training_set(folder, filenames):
    if len(filenames) > 0:
        filenames.sort()
        note_number = filenames[0].split('_')[0]
        note_files = []
        for i, f in enumerate(filenames):
            if f.startswith(note_number + '_'):
                note_files.append(f)
            else:
                training_set = __get_training_set_from_filenames(folder, note_files, note_number)
                # Prepare data for next iteration
                note_number = filenames[i].split('_')[0]
                note_files = []
                note_files.append(f)
                yield training_set

        if len(note_files) > 0:
            training_set = __get_training_set_from_filenames(folder, note_files, note_number)
            yield training_set


def __get_testing_set_from_folder(folder):
    if not os.path.isdir(folder):
        logger.error('The following folder doesn\'t exists' % (folder))
        sys.exit(-1)

    filenames = os.listdir(folder)
    filenames.sort()

    input_filepath = None
    output_filepath = None
    local_metadata = None
    global_metadata = None
    configs = None
    onsets = None
    possible_notes = None
    for f in filenames:
        if 'test.csv' in f:
            input_filepath = os.path.join(folder, f)
        elif 'test_configs.txt' in f:
            configs = os.path.join(folder, f)
        elif 'test_global_metadata.csv' in f:
            global_metadata = os.path.join(folder, f)
        elif 'test_local_metadata.csv' in f:
            local_metadata = os.path.join(folder, f)
        elif 'test_output.csv' in f:
            output_filepath = os.path.join(folder, f)
        elif 'onset_output.csv' in f:
            onsets = os.path.join(folder, f)
        elif 'possible_notes' in f:
            possible_notes = os.path.join(folder, f)

    return TestingSet(input=input_filepath,
                      output=output_filepath,
                      local_metadata=local_metadata,
                      global_metadata=global_metadata,
                      configs=configs,
                      onsets=onsets,
                      possible_notes=possible_notes)


def __exec_command(cmd):
    os.system(cmd)


def __zip_dataset(training_set, testing_set, to_folder=None):
    pass
    # if to_folder:
    #     to_folder = '"' + to_folder + os.sep

    # files_to_zip = ''
    # # Training set
    # if training_set.input:
    #     files_to_zip += ' "' + str(training_set.input) + '"'
    # if training_set.output:
    #     files_to_zip += ' "' + str(training_set.output) + '"'
    # if training_set.configs:
    #     files_to_zip += ' "' + str(training_set.configs) + '"'
    # if training_set.global_metadata:
    #     files_to_zip += ' "' + str(training_set.global_metadata) + '"'
    # if training_set.local_metadata:
    #     files_to_zip += ' "' + str(training_set.local_metadata) + '"'

    # # Testing set
    # if testing_set.input:
    #     files_to_zip += ' "' + str(testing_set.input) + '"'
    # if testing_set.output:
    #     files_to_zip += ' "' + str(testing_set.output) + '"'
    # if testing_set.configs:
    #     files_to_zip += ' "' + str(testing_set.configs) + '"'
    # if testing_set.global_metadata:
    #     files_to_zip += ' "' + str(testing_set.global_metadata) + '"'
    # if testing_set.local_metadata:
    #     files_to_zip += ' "' + str(testing_set.local_metadata) + '"'
    # if testing_set.onsets:
    #     files_to_zip += ' "' + str(testing_set.onsets) + '"'
    # if testing_set.possible_notes:
    #     files_to_zip += ' "' + str(testing_set.possible_notes) + '"'

    # cmd = 'zip -j ' + to_folder + str(training_set.note_number) + '.zip" ' + files_to_zip
    # __exec_command(cmd)


def main(training_set_folder, testing_set_folder, to_folder=None):
    testing_set = __get_testing_set_from_folder(folder=testing_set_folder)
    training_sets = __get_training_sets_from_folder(folder=training_set_folder)
    for t in training_sets:
        __zip_dataset(training_set=t,
                      testing_set=testing_set,
                      to_folder=to_folder)


if __name__ == '__main__':
    main(training_set_folder=TRAINING_FILES_FORLDER,
         testing_set_folder=TESTING_FILES_FOLDER,
         to_folder=OUTPUT_FOLDER)
