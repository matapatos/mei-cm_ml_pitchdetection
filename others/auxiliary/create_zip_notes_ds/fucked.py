
import os
import collections
import logging
import sys


# TRAINING_FILES_FORLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/specific_frames_datasets'
# TESTING_FILES_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/testing'
# OUTPUT_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/fold_1'


TRAINING_FILES_FORLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/specific_frames_datasets'
TESTING_FILES_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/testing'
OUTPUT_FOLDER = '/home/andre-gtx/Desktop/Thesis/mei-cm_ml_io_generator/output/yey'

TrainingSet = collections.namedtuple('TrainingSet', 'note_number input output local_metadata global_metadata configs')
TestingSet = collections.namedtuple('TestingSet', 'input output local_metadata global_metadata configs')


logger = logging.getLogger(name=__name__)
logger.setLevel(logging.WARNING)


def __get_training_set_from_filenames(note_number, folder):
    filenames = os.listdir(folder)
    for i, f in enumerate(filenames):
        filenames[i] = os.path.join(folder, f)

    input_filepath = filenames[2]
    output_filepath = filenames[1]
    local_metadata = filenames[4]
    global_metadata = filenames[3]
    configs = filenames[0]
    return TrainingSet(note_number=note_number,
                       input=input_filepath,
                       output=output_filepath,
                       local_metadata=local_metadata,
                       global_metadata=global_metadata,
                       configs=configs)


def __get_training_sets_from_folder(folder):
    if not os.path.isdir(folder):
        logger.error('The following folder doesn\'t exists' % (folder))
        sys.exit(-1)

    filenames = os.listdir(folder)
    filenames.sort()

    training_sets = []
    for note_number in filenames:
        train_set = __get_training_set_from_filenames(note_number, os.path.join(folder, note_number))
        training_sets.append(train_set)

    return training_sets


def __get_testing_set_from_folder(folder):
    if not os.path.isdir(folder):
        logger.error('The following folder doesn\'t exists' % (folder))
        sys.exit(-1)

    filenames = os.listdir(folder)
    filenames.sort()

    input_filepath = os.path.join(folder, filenames[0])
    configs = os.path.join(folder, filenames[1])
    global_metadata = os.path.join(folder, filenames[2])
    local_metadata = os.path.join(folder, filenames[3])
    output = os.path.join(folder, filenames[4])
    return TestingSet(input=input_filepath,
                      output=output,
                      local_metadata=local_metadata,
                      global_metadata=global_metadata,
                      configs=configs)


def __exec_command(cmd):
    os.system(cmd)


def __zip_dataset(training_set, testing_set, to_folder=None):
    if to_folder:
        to_folder = '"' + to_folder + os.sep

    cmd = 'zip -j ' + to_folder + str(training_set.note_number) + '.zip" "' + training_set.input + '" "' + training_set.output + '" "' + training_set.configs + '" "' + training_set.global_metadata + '" "' + training_set.local_metadata + '" "' + testing_set.input + '" "' + testing_set.output + '" "' + testing_set.configs + '" "' + testing_set.global_metadata + '" "' + testing_set.local_metadata + '"'
    __exec_command(cmd)


def main(training_set_folder, testing_set_folder, to_folder=None):
    testing_set = __get_testing_set_from_folder(folder=testing_set_folder)
    training_sets = __get_training_sets_from_folder(folder=training_set_folder)
    for t in training_sets:
        __zip_dataset(training_set=t,
                      testing_set=testing_set,
                      to_folder=to_folder)


if __name__ == '__main__':
    main(training_set_folder=TRAINING_FILES_FORLDER,
         testing_set_folder=TESTING_FILES_FOLDER,
         to_folder=OUTPUT_FOLDER)
