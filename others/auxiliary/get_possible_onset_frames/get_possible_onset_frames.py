import numpy as np
import pandas as pd
import os
import collections


import pitch_estimation.auxiliary.metrics.utils as utils
import main_globals as mg


CSV_FILE_WITH_PREDICTIONS = '/media/matapatos/AB4A-A1D0/Thesis/Training_additional_post_processing/test.csv'
METADATA_FILE = '/media/matapatos/AB4A-A1D0/Thesis/Training_additional_post_processing/test_local_metadata.csv'


def main():
    pieces = __get_musical_pieces()
    all_onsets = collections.defaultdict(list)
    for piece_name, transcribed_frames in pieces.items():
        for i in range(mg.NUMBER_OF_NOTES):
            notes_group = utils.get_notes_group(transcribed_frames[i].to_numpy())
            for note in notes_group:
                all_onsets[piece_name].append(note.start_index)

        all_onsets[piece_name] = np.unique(all_onsets[piece_name])

    __write_onset_frames_per_music(all_onsets)


def __write_onset_frames_per_music(all_onsets):
    onsets_str = ''
    for piece_name, onsets in all_onsets.items():
        onsets_str += piece_name + ',' + np.array2string(onsets,
                                                         separator=',',
                                                         threshold=np.inf).replace('\n', '')\
                                                                          .replace(' ', '')
        onsets_str += '\n'

    name = os.path.splitext(os.path.basename(CSV_FILE_WITH_PREDICTIONS))[0] + '_onsets.csv'
    filepath = os.path.dirname(CSV_FILE_WITH_PREDICTIONS)
    filepath = os.path.join(filepath, name)
    if os.path.isfile(filepath):
        result = __ask_user_yes_or_no(msg='File \'%s\' already exists, do you want to overwrite it?' % filepath)
        if not result:
            raise FileExistsError('%s' % filepath)

    with open(filepath, 'w') as file:
        file.write(onsets_str)


def __ask_user_yes_or_no(msg):
    valid = {
        True: ['y', 'ye', 'yes'],
        False: ['n', 'no']
    }

    while True:
        result = __ask_user_and_wait_for_feedback(msg)
        for has_user_accepted, values in valid.items():
            if result in values:
                return has_user_accepted


def __ask_user_and_wait_for_feedback(msg):
    print(msg + ' [y/n]')
    result = input().lower()
    while not result:
        result = input().lower()
    return result


def __get_musical_pieces():
    predicted, metadata = __read_files()
    next_musical_piece_indexes = np.unique(np.where(predicted == -9)[0])
    next_musical_piece_indexes = np.sort(next_musical_piece_indexes)
    start_index = 0
    musical_pieces = {}
    for i, piece_end in enumerate(next_musical_piece_indexes):
        musical_pieces[metadata['filename'][i]] = predicted[start_index:piece_end]
        start_index = piece_end + 1

    return musical_pieces


def __read_files():
    # Read metadata file
    metadata = pd.read_csv(METADATA_FILE, sep=',')
    # Read predictions file
    predicted = pd.read_csv(CSV_FILE_WITH_PREDICTIONS, sep=',', header=None)
    predicted_data = np.round(predicted)
    return predicted_data, metadata


if __name__ == '__main__':
    main()
