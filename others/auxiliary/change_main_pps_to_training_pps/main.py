import os
import sys
import shutil


dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + os.sep + '..' + os.sep + '..' + os.sep + '..')
import main_globals as mg
import utils


DIRECTORY = '/home/andre/Desktop/Thesis/ahah'
PP_UNIT_TO_KEEP = 'nn_per_note_2'
NEW_NAME_FOR_PP_UNIT_TO_KEEP = 'nn_per_note'


def main():
    global DIRECTORY

    files = os.listdir(DIRECTORY)
    for f in files:
        if utils.is_number_between(numbers=f, min_inclusive=mg.FIRST_NOTE_NUMBER, max_inclusive=mg.FIRST_NOTE_NUMBER + mg.NUMBER_OF_NOTES - 1):
            directory = __get_pp_unit_directory(note_number=int(f))
            __remove_unnecessary_files(folder=directory)
            __rename_file_from_directory(folder=directory)


def __get_pp_unit_directory(note_number):
    note_number_dir = DIRECTORY + os.sep + str(note_number) + os.sep + 'others' + os.sep + 'output'
    if not os.path.isdir(note_number_dir):
        raise FileNotFoundError('Directory %s does not exists' % (note_number_dir))

    last_folder = utils.get_last_folder_from_folder(main_folder=note_number_dir)
    pp_unit_directory = os.path.join(last_folder, 'post_processing')
    if not os.path.isdir(pp_unit_directory):
        raise FileNotFoundError('Directory %s does not exists' % (pp_unit_directory))
    return pp_unit_directory


def __remove_unnecessary_files(folder):
    '''Remove directory that contains the name NEW_NAME_FOR_PP_UNIT_TO_KEEP if only PP_UNIT_TO_KEEP also exists!'''
    global NEW_NAME_FOR_PP_UNIT_TO_KEEP
    global PP_UNIT_TO_KEEP

    files = os.listdir(folder)
    file_to_delete = None
    is_to_delete = False
    for f in files:
        if f == NEW_NAME_FOR_PP_UNIT_TO_KEEP:
            file_to_delete = os.path.join(folder, f)
        elif f == PP_UNIT_TO_KEEP:
            is_to_delete = True

    if file_to_delete and is_to_delete:
        print("INFO: Deleting %s" % (file_to_delete))
        shutil.rmtree(file_to_delete, ignore_errors=True)  # Recursively remove sub-folders too.


def __rename_file_from_directory(folder):
    global PP_UNIT_TO_KEEP
    global NEW_NAME_FOR_PP_UNIT_TO_KEEP

    files = os.listdir(folder)
    for f in files:
        if f == PP_UNIT_TO_KEEP:
            src = os.path.join(folder, f)
            dst = os.path.join(folder, NEW_NAME_FOR_PP_UNIT_TO_KEEP)
            os.rename(src=src, dst=dst)


if __name__ == "__main__":
    main()
