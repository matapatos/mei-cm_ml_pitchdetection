import os
import argparse


def main():
	args = __parse_arguments()
	mp3s = __get_all_mp3s_from_folder(folder=args.folder, is_recursive=args.recursive)
	if mp3s is None:
		raise FileNotFoundError('No WAV files were found in \'%s\'' % args.folder)

	for w in mp3s:
		__cut_mp3_file(mp3_filepath=w)


def __cut_mp3_file(mp3_filepath, max_seconds=30):
	output_file_path = mp3_filepath[:-3] + '_fake.mp3'
	cmd = 'ffmpeg -t %d -i "%s" -acodec copy "%s"' % (max_seconds, mp3_filepath, output_file_path)
	os.system(cmd)
	# Replace mp3_filepath with the new cutted output_file_path file
	cmd = 'mv "%s" "%s"' % (output_file_path, mp3_filepath)
	os.system(cmd)


def __get_all_mp3s_from_folder(folder, is_recursive=False, mp3s=None):
	if mp3s is None:
		mp3s = []

	files = os.listdir(folder)
	for f in files:
		filepath = os.path.join(folder, f)
		if os.path.isdir(filepath):
			if is_recursive:
				mp3s = __get_all_mp3s_from_folder(folder=filepath,
												  is_recursive=is_recursive,
												  mp3s=mp3s)
		elif __is_mp3_file(filepath):
			mp3s.append(filepath)

	return mp3s


def __is_mp3_file(filepath):
	filename = os.path.basename(filepath)
	parts = filename.split('.')
	if len(parts) > 1:
		return str(parts[len(parts) - 1]).lower() == 'mp3'
	return False


def __parse_arguments():
	parser = argparse.ArgumentParser(description='Available arguments:')
	parser.add_argument('-f', '-F', '--folder',
						help='Folder containing mp3s',
						required=True,
						type=str)

	parser.add_argument('-r', '-R', '--recursive',
						help='To recursevely convert wav to mp3?',
						action='store_const',
						const=True,
						default=False)

	args = parser.parse_args()
	if not os.path.isdir(args.folder):
		raise FileNotFoundError('Directory \'%s\' does not exists.' % (args.folder))

	return args


if __name__ == "__main__":
	main()
