import os
import argparse


def main():
    args = __parse_arguments()
    wavs = __get_all_wavs_from_folder(folder=args.folder, is_recursive=args.recursive)
    if wavs is None:
        raise FileNotFoundError('No WAV files were found in \'%s\'' % args.folder)

    for w in wavs:
        mp3_filepath = w[:-3] + 'mp3'
        if os.path.isfile(mp3_filepath):
            print('INFO: Skipping \'%s\' because already exists...' % (mp3_filepath))
        else:
            __convert_wav_to_mp3(wav_filepath=w, mp3_filepath=mp3_filepath)


def __convert_wav_to_mp3(wav_filepath, mp3_filepath):
    cmd = 'ffmpeg -i %s -acodec mp3 %s' % (wav_filepath, mp3_filepath)
    os.system(cmd)


def __get_all_wavs_from_folder(folder, is_recursive=False, wavs=None):
    if wavs is None:
        wavs = []

    files = os.listdir(folder)
    for f in files:
        filepath = os.path.join(folder, f)
        if os.path.isdir(filepath):
            if is_recursive:
                wavs = __get_all_wavs_from_folder(folder=filepath,
                                                  is_recursive=is_recursive,
                                                  wavs=wavs)
        elif __is_wav_file(filepath):
            wavs.append(filepath)

    return wavs


def __is_wav_file(filepath):
    filename = os.path.basename(filepath)
    parts = filename.split('.')
    if len(parts) > 1:
        return str(parts[len(parts) - 1]).lower() == 'wav'
    return False


def __parse_arguments():
    parser = argparse.ArgumentParser(description='Available arguments:')
    parser.add_argument('-f', '-F', '--folder',
                        help='Folder containing WAVs',
                        required=True,
                        type=str)

    parser.add_argument('-r', '-R', '--recursive',
                        help='To recursevely convert wav to mp3?',
                        action='store_const',
                        const=True,
                        default=False)

    args = parser.parse_args()
    if not os.path.isdir(args.folder):
        raise FileNotFoundError('Directory \'%s\' does not exists.' % (args.folder))

    return args


if __name__ == "__main__":
    main()
