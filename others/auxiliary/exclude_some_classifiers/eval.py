import numpy as np
import pandas as pd
import os
from tqdm import tqdm


from pitch_estimation.auxiliary.metrics.sub.controller import ConfusionMatrix
import pitch_estimation.auxiliary.metrics.utils as utils
from pitch_estimation.auxiliary.metrics.configs import MIN_PERCENTAGE_TO_BE_CONSIDERED_VALID_OFFSET_NOTE,\
                                                       MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE,\
                                                       RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC


FOLDS_DATASET = './exclude_some_classifiers/folds'
MIN_NOTE_EXCLUSIVE = 36
MAX_NOTE_EXCLUSIVE = 84
MIN_NOTE = 21
NUMBER_OF_NOTES = 88


def __get_valid_offset_note_from_notes(label_note, predicted_notes):
        for p in predicted_notes:
            if __is_valid_offset(label_note=label_note, predicted_note=p):
                return p

        return None


def __is_valid_offset(label_note, predicted_note):
    '''
    This function returns True if the predicted_note has the necessary guidelines
    to be considered a valid offset comparing with the label_note otherwise it
    returns False.

    Note that this already assumes that the predicted_note has a valid Onset,
    corresponding to the label_note. For that the predicted_note passed must be
    some returned by the function __getValidOnsetNotesOnGroupByIndex(...)

    The guidelines necessary to be considered with a valid offset were based and
    adapted from:
    http://music-ir.org/mirex/wiki/2007:Multiple_Fundamental_Frequency_Estimation_%26_Tracking_Results#Overall_Summary_Results_Task_II

    The adapted guidelines are marked in the code bellow by '# ADAPTED-GUIDELINE'
    '''
    real_size = label_note.end_index - label_note.start_index + 1
    predicted_size = predicted_note.end_index - label_note.start_index + 1

    if real_size < MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE:  # ADAPTED-GUIDELINE This condition was added by me. In the MIREX rules they don't include because they consider a note being played more than 50ms
        distance = abs(real_size - predicted_size)
        if distance < RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC:
            return True
    else:
        num_frames = real_size * 0.2
        real_num_frames = max(num_frames, MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE)
        if predicted_size >= real_num_frames:
            if predicted_note.end_index <= label_note.end_index + RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC:  # ADAPTED-GUIDELINE Added this condition to limit the size of the offset, in a way that it will be not larger than the real one
                return True

    return False


def __get_valid_onset_notes_on_group_by_index(group, index):
        '''
        This function is based from:
        http://music-ir.org/mirex/wiki/2007:Multiple_Fundamental_Frequency_Estimation_%26_Tracking_Results#Overall_Summary_Results_Task_II
        '''
        min_index = index - RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC
        max_index = index + RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC

        notes = []
        for g in group:
            if min_index <= g.start_index and max_index >= g.start_index:
                notes.append(g)

        return notes


def __get_note_based_labels_and_predictions_from_1D_array(labels, predictions):
    labels_notes_group = utils.get_notes_group(group=labels)
    predictions_onset_notes_group = utils.get_notes_group(group=predictions)
    predictions_onset_offset_notes_group = list(predictions_onset_notes_group)

    confusion_matrix_onset = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    confusion_matrix_onset_offset = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    for l_g in labels_notes_group:
        notes = __get_valid_onset_notes_on_group_by_index(group=predictions_onset_notes_group, index=l_g.start_index)
        if len(notes) == 0:
            confusion_matrix_onset.fn += 1
            confusion_matrix_onset_offset.fn += 1
        else:
            confusion_matrix_onset.tp += 1
            offset_note = __get_valid_offset_note_from_notes(label_note=l_g, predicted_notes=notes)
            if offset_note is None:
                confusion_matrix_onset_offset.fn += 1
                predictions_onset_notes_group.remove(notes[0])
            else:
                confusion_matrix_onset_offset.tp += 1
                predictions_onset_offset_notes_group.remove(offset_note)
                predictions_onset_notes_group.remove(offset_note)

    confusion_matrix_onset.fp = len(predictions_onset_notes_group)
    confusion_matrix_onset_offset.fp = len(predictions_onset_offset_notes_group)
    return confusion_matrix_onset, confusion_matrix_onset_offset


def precision(cf):
    if cf.tp + cf.fp == 0:
        return 0

    return cf.tp / (cf.tp + cf.fp)


def recall(cf):
    if cf.tp + cf.fn == 0:
        return 0

    return cf.tp / (cf.tp + cf.fn)


def f_measure(prec, rec):
    if prec + rec == 0:
        return 0

    return (2 * prec * rec) / (prec + rec)


def metrics(cf):
    prec = precision(cf)
    rec = recall(cf)
    f = f_measure(prec, rec)
    return prec, rec, f


def get_frame_based_cf(ground_truth, predicted):
    cf = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    notes_index_range = range(0, NUMBER_OF_NOTES)
    for note_index in notes_index_range:
        labels = ground_truth[note_index]
        preds = predicted[note_index]
        assert(labels.shape == preds.shape)
        for l, p in zip(labels, preds):
            cf.update(label=l, pred=p)

    assert((cf.tp + cf.tn + cf.fp + cf.fn) == ground_truth.shape[0] * ground_truth.shape[1])
    return cf


def get_onset_only_and_onset_offset_cf(ground_truth, predicted):
    global_onset_cf = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    global_onset_offset_cf = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
    notes_index_range = range(0, NUMBER_OF_NOTES)
    for note_index in tqdm(notes_index_range):
        labels = ground_truth[note_index].T
        predictions = predicted[note_index].T
        onset_cf, onset_offset_cf = __get_note_based_labels_and_predictions_from_1D_array(labels=labels,
                                                                                          predictions=predictions)
        global_onset_cf = global_onset_cf + onset_cf
        global_onset_offset_cf = global_onset_offset_cf + onset_offset_cf

    return global_onset_cf, global_onset_offset_cf


def evaluate_data(ground_truth, predicted):
    assert(ground_truth.shape == predicted.shape)
    # Get CF Frame-based
    print('Calculating frame-based metrics...')
    frame_based_cf = get_frame_based_cf(ground_truth=ground_truth, predicted=predicted)
    # Calculate Frame-based metrics
    frame_based = metrics(frame_based_cf)

    # Get CFs Onset only and Onset-Offset
    print('Calculating both onset only and onset-offset metrics...')
    onset_only_cf, onset_offset_cf = get_onset_only_and_onset_offset_cf(ground_truth=ground_truth,
                                                                        predicted=predicted)
    # Calculate Onset only and Onset-Offset metrics
    onset_only = metrics(cf=onset_only_cf)
    onset_offset = metrics(cf=onset_offset_cf)
    return frame_based, onset_only, onset_offset


def eval_fold(fold_folder):
    predicted_filepath = os.path.join(fold_folder, 'test.csv')
    predicted = pd.read_csv(predicted_filepath, sep=',', header=None)
    predicted_data = np.round(predicted)
    if MIN_NOTE_EXCLUSIVE - MIN_NOTE > 0:
        for i in range(MIN_NOTE_EXCLUSIVE - MIN_NOTE):
            predicted_data[i] = 0

    if MAX_NOTE_EXCLUSIVE - MIN_NOTE + 1 < NUMBER_OF_NOTES:
        for i in range(MAX_NOTE_EXCLUSIVE - MIN_NOTE + 1, NUMBER_OF_NOTES):
            predicted_data[i] = 0

    expected_filepath = os.path.join(fold_folder, 'test_output.csv')
    expected = pd.read_csv(expected_filepath, sep=',', header=None)
    expected_data = np.round(expected)

    return evaluate_data(ground_truth=expected_data, predicted=predicted_data)


folders = os.listdir(FOLDS_DATASET)
for f in folders:
    fullpath = os.path.join(FOLDS_DATASET, f)
    if os.path.isdir(fullpath):
        files = os.listdir(fullpath)
        if 'test_output.csv' in files and 'test.csv' in files:
            (frame_based, onset_only, onset_offset) = eval_fold(os.path.join(fullpath))
            print('%s' % (f))
            print('\t\t\tPrecision\t\tRecall\t\tF-Measure')
            print('\tFrame-based:\t%s\t%s\t%s\n\tOnset only:\t%s\t%s\t%s\n\tOnsetOffset:\t%s\t%s\t%s' % (frame_based[0], frame_based[1], frame_based[2],\
                                                                                                         onset_only[0], onset_only[1], onset_only[2],\
                                                                                                         onset_offset[0], onset_offset[1], onset_offset[2]))
