import requests
import os


URL = 'http://localhost:6006/data/plugin/scalars/scalars'
METRICS = ['Accuracy', 'FMeasure', 'FMeasure_OnsetOffset', 'FMeasure_Onset']
METRIC_TYPE = 'IT'  # IT for per iteration metrics type or GL for global metrics
CURRENT_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
OUTPUT_FOLDER = os.path.join(CURRENT_SCRIPT_DIR, 'output')
JSON_OUTPUT_FOLDER = os.path.join(OUTPUT_FOLDER, 'json')

START_NOTE = 31
END_NOTE = 32


def get_content_from_url(url, params=None):
    r = requests.get(url=url, params=params)
    try:
        return r.json()
    except Exception as e:
        print("ERROR")
        print(url)
        print(params)
        print(e)
        return None


def write_json_to_file(filepath, json):
    if not json:
        print('WARN: No json to be saved.')
    else:
        with open(filepath, 'w') as f:
            f.writelines(str(json))


def download_json_file(filepath, url, params=None):
    json_file_content = get_content_from_url(url=url, params=params)
    write_json_to_file(filepath=filepath,
                       json=json_file_content)
    return json_file_content


def get_best_metric_value_from_json(json):
    if not json:
        return None

    best_val = -1.0
    for [_, step, val] in json:
        if best_val < val:
            best_val = val
    return best_val


def download_json_metrics_file(note_number, metric):
    params = {
        'run': str(note_number) + '/test/mean_squared/summary_' + str(note_number) + '/iteration',
        'tag': 'Test/Test/' + str(metric) + '_' + METRIC_TYPE
    }
    filename = str(note_number) + '_' + str(metric) + '.json'
    filepath = os.path.join(JSON_OUTPUT_FOLDER, filename)
    json = download_json_file(filepath=filepath,
                              url=URL,
                              params=params)
    return get_best_metric_value_from_json(json)


def download_json_note_file(note_number):
    best_metrics = {}
    for m in METRICS:
        best_val = download_json_metrics_file(note_number=note_number,
                                              metric=m)
        best_metrics[m] = best_val
    return best_metrics


def download_all_json_notes_files(start_note, end_note):
    global_best_metrics = {}
    for i in range(start_note, end_note + 1):
        note_best_metrics = download_json_note_file(note_number=i)
        global_best_metrics[i] = note_best_metrics

    filepath = os.path.join(OUTPUT_FOLDER, 'best_metrics.json')
    write_json_to_file(filepath=filepath,
                       json=global_best_metrics)


if __name__ == '__main__':
    if not os.path.isdir(OUTPUT_FOLDER):
        os.makedirs(OUTPUT_FOLDER)

    if not os.path.isdir(JSON_OUTPUT_FOLDER):
        os.makedirs(JSON_OUTPUT_FOLDER)

    download_all_json_notes_files(start_note=START_NOTE,
                                  end_note=END_NOTE)
