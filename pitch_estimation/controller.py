import time
import threading
from tqdm import tqdm
import numpy as np
import tensorflow as tf
import collections
import os


import main_globals as mg
import pitch_estimation.configs as configs
import debug_tools
from threads.data.shared_data import SharedData
from pitch_estimation.auxiliary.data.metadata.global_metadata import GlobalMetadata
from pitch_estimation.tasks.csv_reader.data_importer import CSVDataImporter
from pitch_estimation.tasks.learner.learner import Learner
from pitch_estimation.tasks.post_processing.controller import PostProcessingController
import pitch_estimation.utils as utils
import web.commands as commands
import web.running_mode as running_mode
import pitch_estimation.tasks.post_processing.types.all as pp_units
from pitch_estimation.auxiliary.training.early_stopping import EarlyStoppingException


class PitchEstimationController():

    def __init__(self, save_output=False):
        np.random.seed(seed=configs.NUMPY_RANDOM_SEED)
        tf.set_random_seed(seed=configs.GRAPH_RANDOM_SEED)

        self.start_time = time.time()

        # Tasks variables
        self.data_importer = None
        self.learner = None
        self.post_processing = None
        self.save_output = save_output

    def train(self,
              notes_number,
              num_iterations,
              testing_iteration_folds,
              models_folder=None,
              model_iteration=None,
              filter_type='all',
              best_model=None,
              upload=None,
              post_processes=None,
              names_post_processes=None,
              debug=False):
        mg.INFO("Starting training algorithm")
        summary_output_folder = utils.get_summaries_output_folder(self)
        is_to_train_classifiers = True
        if post_processes:
            is_to_train_classifiers = False
        if self.save_output:
            post_processes, names_post_processes = utils.add_post_processing_unit_if_does_not_exist(post_processes=post_processes,
                                                                                                    names_post_processes=names_post_processes,
                                                                                                    pp_unit_class=pp_units.WriteOutput)

            post_processes, names_post_processes = utils.add_post_processing_unit_if_does_not_exist(post_processes=post_processes,
                                                                                                    names_post_processes=names_post_processes,
                                                                                                    pp_unit_class=pp_units.ToMidi)

        with CSVDataImporter(is_training=True) as self.data_importer:
            with Learner(is_training=is_to_train_classifiers,
                         summary_output_folder=summary_output_folder,
                         filter_type=filter_type,
                         notes_number=notes_number,
                         models_folder=models_folder,
                         model_iteration=model_iteration,
                         log_configs_filepath=utils.get_log_training_configs_filepath(self),
                         best_model_folderpath=utils.get_best_models_folderpath(self),
                         best_model=best_model) as self.learner:
                with PostProcessingController(summary_output_folder=summary_output_folder,
                                              output_folderpath=utils.get_models_output_folder(self),
                                              recover_models_folder=models_folder,
                                              recover_model_iteration=model_iteration,
                                              post_processes=post_processes,
                                              names_post_processes=names_post_processes,
                                              notes_number=notes_number,
                                              is_training=True) as self.post_processing:

                    training_global_meta = self.data_importer.get_training_global_metadata()
                    testing_global_meta = self.data_importer.get_testing_global_metadata()
                    # Print dataset informations
                    mg.INFO(GlobalMetadata.get_joined_DS_informations(training_global_meta=training_global_meta,
                                                                      testing_global_meta=testing_global_meta))

                    all_threads = self.__start_producer_and_consumer_threads(training_global_meta=training_global_meta,
                                                                             testing_global_meta=testing_global_meta,
                                                                             num_iterations=num_iterations,
                                                                             testing_iteration_folds=testing_iteration_folds,
                                                                             upload=upload,
                                                                             debug=debug)
                    self.__wait_for_threads(threads=all_threads)

    def predict(self,
                models_folder,
                model_iteration,
                best_model,
                notes_number,
                filter_type='all',
                post_processes=None,
                names_post_processes=None,
                debug=False):
        mg.INFO("Starting prediction algorithm")
        summary_output_folder = utils.get_summaries_output_folder(self)
        if self.save_output:
            post_processes, names_post_processes = utils.add_post_processing_unit_if_does_not_exist(post_processes=post_processes,
                                                                                                    names_post_processes=names_post_processes,
                                                                                                    pp_unit_class=pp_units.WriteOutput)

        with CSVDataImporter(is_training=False) as self.data_importer:
            with Learner(is_training=False,
                         summary_output_folder=summary_output_folder,
                         filter_type=filter_type,
                         notes_number=notes_number,
                         models_folder=models_folder,
                         model_iteration=model_iteration,
                         best_model=best_model) as self.learner:
                with PostProcessingController(summary_output_folder=summary_output_folder,
                                              output_folderpath=None,
                                              post_processes=post_processes,
                                              names_post_processes=names_post_processes,
                                              recover_models_folder=models_folder,
                                              recover_model_iteration=model_iteration,
                                              notes_number=notes_number,
                                              is_training=False) as self.post_processing:
                    testing_global_meta = self.data_importer.get_testing_global_metadata()
                    mg.INFO(str(testing_global_meta))

                    with tqdm(total=testing_global_meta.nr_of_sounds) as progress_bar:
                        has_next, training_data, evaluation_data, local_meta = self.data_importer.next_prediction_music()
                        while has_next:
                            # Get next music if any
                            next_has_next, next_training_data, next_evaluation_data, next_local_meta = self.data_importer.next_prediction_music()

                            if len(training_data) > 0:
                                is_last_music = not next_has_next
                                results = self.learner.predict(input_data=training_data,
                                                               output_data=evaluation_data,
                                                               local_meta=local_meta,
                                                               is_last_music=is_last_music,
                                                               debug=debug)
                                results = self.post_processing.apply(results=results,
                                                                     run_mode=mg.RunMode.PREDICTING,
                                                                     is_last_music=is_last_music,
                                                                     global_metadata=testing_global_meta,
                                                                     best_model=best_model,
                                                                     debug=debug)

                            has_next, training_data, evaluation_data, local_meta = next_has_next, next_training_data, next_evaluation_data, next_local_meta
                            progress_bar.update()

    def __start_producer_and_consumer_threads(self, training_global_meta, testing_global_meta, num_iterations, testing_iteration_folds, upload=None, debug=None):
        training_shared_data, evaluation_shared_data = self.__get_shared_data(training_global_meta=training_global_meta,
                                                                              testing_global_meta=testing_global_meta)
        all_threads = []
        producer = threading.Thread(name='producer', target=self.__producer, args=(training_shared_data,
                                                                                   evaluation_shared_data,
                                                                                   num_iterations,
                                                                                   testing_iteration_folds,
                                                                                   debug))
        all_threads.append(producer)

        consumer = threading.Thread(name='consumer', target=self.__consumer, args=(training_shared_data,
                                                                                   evaluation_shared_data,
                                                                                   training_global_meta,
                                                                                   testing_global_meta,
                                                                                   num_iterations,
                                                                                   testing_iteration_folds,
                                                                                   upload,
                                                                                   debug))
        all_threads.append(consumer)

        for t in all_threads:
            t.start()

        return all_threads

    def __get_shared_data(self, training_global_meta, testing_global_meta):
        training_shared_data = SharedData(length=training_global_meta.nr_of_sounds,
                                          sequencial_musics=mg.SEQUENCIAL_MUSICS,
                                          log_filepath=utils.get_log_music_sequence_filepath(self))

        evaluation_shared_data = SharedData(length=testing_global_meta.nr_of_sounds,
                                            sequencial_musics=True,  # In the evaluation it doesn't really matters to not be sequencial because we are not training the model, just evaluating it.
                                            log_filepath=None)

        return training_shared_data, evaluation_shared_data

    def __wait_for_threads(self, threads):
        for t in threads:
            t.join()

    def __producer(self, training_shared_data, evaluation_shared_data, num_iterations, testing_iteration_folds, debug=False):
        for i in range(num_iterations):
            self.data_importer.produce_training_data(training_shared_data=training_shared_data)

            current_iteration = i + 1
            if current_iteration % testing_iteration_folds == 0:
                self.data_importer.produce_evaluation_data(evaluation_shared_data=evaluation_shared_data)
                is_to_continue = evaluation_shared_data.wait_for_consumers()
            else:
                is_to_continue = training_shared_data.wait_for_consumers()

            if not is_to_continue:
                break

            self.data_importer.reset_reading_indexes()

    def __consumer(self,
                   training_shared_data,
                   evaluation_shared_data,
                   training_global_metadata,
                   evaluation_global_metadata,
                   num_iterations,
                   testing_iteration_folds,
                   upload=None,
                   debug=False):
        for i in range(num_iterations):
            current_iteration = i + 1
            start_time = time.time()

            self.__train_with_all_training_data(training_shared_data=training_shared_data,
                                                global_metadata=training_global_metadata,
                                                debug=debug)

            if current_iteration % testing_iteration_folds == 0:
                self.__evaluate_with_all_testing_data(evaluation_shared_data=evaluation_shared_data,
                                                      global_metadata=evaluation_global_metadata,
                                                      debug=debug)

            # Increase the step. This is used in Tensorboard graphs
            self.learner.save_models(output_folderpath=utils.get_models_output_folder(self),
                                     iteration_num=current_iteration,
                                     debug=debug)
            try:
                self.learner.increase_global_step()
                self.post_processing.increase_global_step()
            except EarlyStoppingException as e:
                mg.INFO(e)
                evaluation_shared_data.notify_early_stopping()
                training_shared_data.notify_early_stopping()
                break
            else:
                evaluation_shared_data.notify_producer()
                training_shared_data.notify_producer()
            finally:
                mg.INFO("Iteration %d took %s" % (current_iteration,
                                                  debug_tools.get_operation_time_from_start_time(start_time=start_time)))

        self.__upload_models(to=upload, debug=debug)
        training_shared_data.write_log_music_sequence()

    def __upload_models(self, to, debug=False):
        if to:
            models_output_folder = utils.get_models_output_folder(self)
            summary_output_folder = utils.get_summaries_output_folder(self)
            filepath = self.__get_zip_filepath_to_upload(to=to)
            self.__zip_files(filepath=filepath, files=[models_output_folder, summary_output_folder])

    def __get_zip_filepath_to_upload(self, to):
        filters = collections.defaultdict(list)
        for f in self.learner.filters:
            filters[f.get_class_name(f)].append(f.note_number)

        name = ""
        for filter_type, notes_number in filters.items():
            if len(filters) == 1:
                filter_type = ""

            name += filter_type
            for n in notes_number:
                name += '_' + str(n)
        name += '.zip'
        name = name[1:]  # Discard first character that is an underscore
        return os.path.join(to, name)

    def __zip_files(self, filepath, files, debug=False):
        if isinstance(files, str):
            files = [files]

        if not isinstance(files, list):
            mg.ERROR('Invalid argument type denominated by "files" in "__zip_files" function. Expected type: list or str. Given type: %s' % (type(files)))

        if len(files) == 0:
            mg.WARN('No files to upload.')
            return False

        # Build OS command
        cmd = commands.ZIP + ' -r ' + str(filepath) + ' '
        for f in files:
            cmd += ' ' + str(f)
        # Run command
        running_mode.RunningMode.exec_command(command=cmd, debug=debug)
        return True

    def __train_with_all_training_data(self, training_shared_data, global_metadata, debug=False):
        self.__loop_over_data(shared_data=training_shared_data,
                              run_mode=mg.RunMode.TRAINING,
                              global_metadata=global_metadata,
                              debug=debug)

    def __evaluate_with_all_testing_data(self, evaluation_shared_data, global_metadata, debug=False):
        self.__loop_over_data(shared_data=evaluation_shared_data,
                              run_mode=mg.RunMode.EVALUATING,
                              global_metadata=global_metadata,
                              debug=debug)

    def __loop_over_data(self, shared_data, run_mode, global_metadata, debug=False):
        tqdm_shared_data = shared_data
        if not debug:
            tqdm_shared_data = tqdm(shared_data)  # Enable progress bar

        learner_function = self.learner.train if run_mode == mg.RunMode.TRAINING else self.learner.evaluate
        for d in tqdm_shared_data:
            data_in = d.data_in
            data_out = d.data_out
            local_meta = d.local_meta

            if len(data_in) <= 0:
                mg.WARN('The sound with the following filename {0} doesn\'t have any training data.'.format(local_meta.filename))

            else:  # Do training
                is_last_music = shared_data.has_finish()
                results = learner_function(input_data=data_in,
                                           output_data=data_out,
                                           local_meta=local_meta,
                                           is_last_music=is_last_music,
                                           debug=debug)
                results = self.post_processing.apply(results=results,
                                                     run_mode=run_mode,
                                                     is_last_music=is_last_music,
                                                     global_metadata=global_metadata,
                                                     debug=debug)
