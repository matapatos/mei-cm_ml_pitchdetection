import os


import main_globals as mg
import utils as mg_utils
from pitch_estimation.tasks.post_processing.utils import get_ordered_variations_from_name


class PostProcessingController():

    def __init__(self,
                 summary_output_folder,
                 output_folderpath,
                 post_processes=None,
                 names_post_processes=None,
                 notes_number=None,
                 recover_models_folder=None,
                 recover_model_iteration=None,
                 is_training=True):
        self.summary_output_folder = os.path.join(summary_output_folder, 'post_processing')
        self.output_folderpath = output_folderpath
        if self.output_folderpath:
            self.output_folderpath = os.path.join(output_folderpath, 'post_processing')
        self.post_processes = post_processes
        self.names_post_processes = names_post_processes
        if len(self.post_processes) > 0:
            os.mkdir(self.summary_output_folder)
        self.notes_number = notes_number
        self.identifiers = {}
        self.recover_models_folder = recover_models_folder
        self.recover_model_iteration = recover_model_iteration
        self.recover_best_models_folderpath = None
        if self.recover_models_folder:
            self.recover_best_models_folderpath = os.path.join(self.recover_models_folder, 'post_processing')
        self.is_training = is_training

    def apply(self, results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        if len(self.post_processes) <= 0:
            return results

        for filter_type, all_notes_results in results.items():
            for pp in self.post_processes:
                all_notes_results = pp.apply(all_notes_results=all_notes_results,
                                             run_mode=run_mode,
                                             is_last_music=is_last_music,
                                             global_metadata=global_metadata,
                                             best_model=best_model,
                                             additional_data=additional_data,
                                             debug=debug)
            results[filter_type] = all_notes_results

        return results

    def increase_global_step(self):
        for pp in self.post_processes:
            func = getattr(pp, 'increase_global_step', None)
            if callable(func):
                pp.increase_global_step()

    def __get_next_class_identifier(self, class_var):
        class_name = class_var.get_class_name(class_var)
        if class_name not in self.identifiers:
            self.identifiers[class_name] = 1

        self.identifiers[class_name] += 1
        return '_' + str(self.identifiers[class_name])

    def __enter__(self):
        mg.DEBUG('Preparing post processing tools...')
        last_pp_with_nn = None
        for i, pp in enumerate(self.post_processes):
            if not mg_utils.is_class_initialized(pp):
                identifier = pp.get_class_name(pp)
                if mg_utils.has_any_initialized_class_in_array(a=self.post_processes, class_var=pp):
                    identifier = identifier + self.__get_next_class_identifier(pp)

                pp = pp(summary_output_folder=self.summary_output_folder,
                        output_folderpath=self.output_folderpath,
                        notes_number=self.notes_number,
                        identifier=identifier)
                self.post_processes[i] = pp

            pp.variations_size = len(get_ordered_variations_from_name(pp_unit=pp, name=self.names_post_processes[i]))
            if '_hasonset_' in self.names_post_processes[i]:
                pp.has_onset_variation = True
            pp.__enter__()
            pp.set_possible_variation(name=self.names_post_processes[i])
            disable_training = getattr(pp, "disable_training", None)  # Disable training to all the post-processing units that contains an Neural Network, because only the last one can be trainable otherwise conflicts during the training phase will occur.
            if callable(disable_training):
                disable_training(recover_models_folder=self.recover_models_folder,
                                 recover_model_iteration=self.recover_model_iteration,
                                 recover_best_models_folderpath=self.recover_best_models_folderpath)
                last_pp_with_nn = pp

        if last_pp_with_nn and self.is_training:  # Enable training phase only to the last post-processing unit that contains a Neural Network
            last_pp_with_nn.enable_training()

        return self

    def __exit__(self, exc_t, exc_v, trace):
        mg.DEBUG('Closing post processing tools...')
        for pp in self.post_processes:
            pp.__exit__(exc_t, exc_v, trace)

    def __setattr__(self, name, value):
        if name == 'post_processes' or name == 'names_post_processes':
            if value is None:
                value = []
        super(PostProcessingController, self).__setattr__(name, value)
