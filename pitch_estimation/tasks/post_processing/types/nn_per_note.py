import os
import numpy as np


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
from pitch_estimation.tasks.post_processing.types.single_nn import SingleNN
from pitch_estimation.tasks.post_processing.utils import get_ordered_variations_from_name
from pitch_estimation.tasks.post_processing.types.onsets import Onsets
import pitch_estimation.tasks.post_processing.configs as configs


class NNPerNote(PostProcessing):

    def __init__(self, summary_output_folder, output_folderpath, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)

        self.identifier = identifier

        summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(NNPerNote, self).__init__(summary_output_folder=summary_output_folder)

        self.notes_number = notes_number
        if output_folderpath:
            output_folderpath = os.path.join(output_folderpath, str(identifier))
        self.output_folderpath = output_folderpath
        self.models_folder = None
        self.model_iteration = None
        self.best_models_folderpath = None

        self.single_nns = {}
        self.is_model_restored = False
        self.is_training_phase_enabled = False
        self.variations = []
        self.__onsets_pp = None
        self.has_onset_variation = False

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        new_all_notes_results = []
        for note_results in all_notes_results:
            if note_results.note_number not in self.single_nns:
                mg.WARN('Missing single nn for note number %d' % (note_results.note_number))
            else:
                note_results = self.single_nns[note_results.note_number].apply(all_notes_results=[note_results],
                                                                               run_mode=run_mode,
                                                                               is_last_music=is_last_music,
                                                                               global_metadata=global_metadata,
                                                                               best_model=best_model,
                                                                               additional_data=additional_data,
                                                                               debug=debug)[0]
                self.metrics.update_metrics(run_mode=run_mode,
                                            note_result=note_results,
                                            is_last_music=is_last_music)
                new_all_notes_results.append(note_results)

        return new_all_notes_results

    def increase_global_step(self):
        self.metrics.increase_global_step()
        for note_number, s in self.single_nns.items():
            s.increase_global_step()

    def disable_training(self, recover_models_folder, recover_model_iteration, recover_best_models_folderpath):
        self.recover_models_folder = None
        self.recover_best_models_folderpath = None

        if recover_models_folder:
            self.recover_models_folder = os.path.join(recover_models_folder, self.identifier)
        self.recover_model_iteration = recover_model_iteration
        if recover_best_models_folderpath:
            self.recover_best_models_folderpath = os.path.join(recover_best_models_folderpath, self.identifier)
        for note_number, nn in self.single_nns.items():
            recover_models_folder = None if self.recover_models_folder is None else os.path.join(self.recover_models_folder, str(note_number))
            recover_best_models_folderpath = None if self.recover_best_models_folderpath is None else os.path.join(self.recover_best_models_folderpath, str(note_number))

            nn.disable_training(recover_models_folder=recover_models_folder,
                                recover_model_iteration=self.recover_model_iteration,
                                recover_best_models_folderpath=recover_best_models_folderpath)
        self.is_training_phase_enabled = False

    def enable_training(self):
        mg.INFO('Post-processing unit with identifier \'%s\' is enabled for training with its following children Neural Networks:' % (self.identifier))
        for note_number, nn in self.single_nns.items():
            mg.INFO('\t\'%s\' -> %s' % (nn.identifier, note_number))
            nn.enable_training(is_child=True)
        self.is_training_phase_enabled = True

    @staticmethod
    def get_class_name_variations():
        return SingleNN.get_class_name_variations()

    def set_possible_variation(self, name):
        if name != self.get_class_name(self):
            self.variations = get_ordered_variations_from_name(pp_unit=self, name=name)
            for note_number, nn in self.single_nns.items():
                nn.variations = self.variations

    def get_frame_onsets(self, run_mode, note_results, global_metadata):
        if not self.__onsets_pp:
            self.__onsets_pp = Onsets(None)
            self.__onsets_pp.__enter__()
            self.__onsets_pp.data = self.__onsets_pp.get_all(global_metadata)

        real_onsets = self.__onsets_pp.get_data_of_results(note_results)[configs.H_ONSETS]
        onset_frames = np.zeros(shape=note_results.labels.shape)  # Create array with corresponding frames that contains onsets.
        for onset_frame in real_onsets:  # Set the frames with onsets to 1
            onset_frames[onset_frame] = 1
        return onset_frames

    def __get_best_models_folderpath_from_summary(self):
        try:
            main_folder, parts = self.summary_output_folder.split(mg.PARENT_SUMMARIES_FOLDER)
            date = parts.split(os.sep)[1]
            fullpath_models_folder = os.path.join(main_folder, mg.PARENT_MODELS_FOLDER)
            fullpath_models_folder = os.path.join(fullpath_models_folder, date)
            return os.path.join(fullpath_models_folder, 'nn_per_note')
        except Exception as e:
            mg.ERROR('Trying to retrieve the output folder from an invalid folder name: %s' % (str(self.summary_output_folder)))

    def __get_output_folder_path_of_note(self, note_number):
        if not self.output_folderpath:
            return None
        else:
            output_folderpath = os.path.join(self.output_folderpath, str(note_number))
            if not os.path.isdir(output_folderpath):
                os.makedirs(output_folderpath)
            return output_folderpath

    def __enter__(self):
        if self.notes_number and not self.single_nns:
            for n in self.notes_number:
                summary_output_folder = os.path.join(self.summary_output_folder, str(n))
                output_folderpath = self.__get_output_folder_path_of_note(note_number=n)
                single_nn = SingleNN(summary_output_folder=summary_output_folder,
                                     output_folderpath=output_folderpath,
                                     update_summaries=False,
                                     parent=self,
                                     identifier=self.identifier + '_single_nn_' + str(n))
                single_nn.variations_size = self.variations_size
                single_nn.note_number = n
                single_nn.has_onset_variation = self.has_onset_variation
                single_nn.__enter__()
                self.single_nns[int(n)] = single_nn
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.metrics.close()
        for note_number, s in self.single_nns.items():
            s.__exit__(exc_t, exc_v, trace)

        self.single_nns = {}
        if self.__onsets_pp:
            self.__onsets_pp.__exit__(exc_t, exc_v, trace)
