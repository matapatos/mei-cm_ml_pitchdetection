import os
import numpy as np
import magenta
import bokeh


import main_globals as mg
from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
from pitch_estimation.auxiliary.metrics.utils import get_notes_group


class ToMidi(PostProcessing):
    VELOCITY = 100

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        super(ToMidi, self).__init__(summary_output_folder=None)

        self.output_folder = os.path.join(summary_output_folder, str(identifier))

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        if all_notes_results:
            predictions_sequence = magenta.protobuf.music_pb2.NoteSequence()
            labels_sequence = magenta.protobuf.music_pb2.NoteSequence()

            for note_results in all_notes_results:
                self.__add_frames_of_note_to_sequence(sequence=predictions_sequence,
                                                      note_number=note_results.note_number,
                                                      frames=note_results.predictions,
                                                      global_metadata=global_metadata)
                self.__add_frames_of_note_to_sequence(sequence=labels_sequence,
                                                      note_number=note_results.note_number,
                                                      frames=note_results.labels,
                                                      global_metadata=global_metadata)

            self.__write_sequence_as_midi_and_plot(sequence=predictions_sequence,
                                                   all_notes_results=all_notes_results,
                                                   run_mode=run_mode,
                                                   appendix='predictions')
            self.__write_sequence_as_midi_and_plot(sequence=labels_sequence,
                                                   all_notes_results=all_notes_results,
                                                   run_mode=run_mode,
                                                   appendix='labels')

        return all_notes_results

    def __add_frames_of_note_to_sequence(self, sequence, note_number, frames, global_metadata):
        onsets, offsets = self.__get_time_onsets_and_durations_from_frames(frames=frames,
                                                                           global_metadata=global_metadata)
        assert len(onsets) == len(offsets)
        for on, off in zip(onsets, offsets):
            sequence.notes.add(pitch=note_number, start_time=on, end_time=off, velocity=ToMidi.VELOCITY)

    def __get_time_onsets_and_durations_from_frames(self, frames, global_metadata):
        frames = np.round(frames)
        notes_groups = get_notes_group(frames)
        onsets = []
        offsets = []
        for group in notes_groups:
            # Calculate onset and offset and finally the duration
            onset_time, offset_time = self.__get_note_onset_and_offset(note_group=group,
                                                                       global_metadata=global_metadata)
            onsets.append(onset_time)
            offsets.append(offset_time)
        return onsets, offsets

    def __get_note_onset_and_offset(self, note_group, global_metadata):
        initial_time, mid_time, end_time = self.__get_frame_initial_mid_and_end_time(frame_num=note_group.start_index,
                                                                                     global_metadata=global_metadata)
        if note_group.start_index == note_group.end_index:
            return initial_time, end_time
        else:
            _, offset_time, _ = self.__get_frame_initial_mid_and_end_time(frame_num=note_group.end_index,
                                                                          global_metadata=global_metadata)
            return mid_time, offset_time

    def __get_frame_initial_mid_and_end_time(self, frame_num, global_metadata):
        initial_time = (frame_num * global_metadata.frame_samples) / global_metadata.frequency_samples
        end_time = ((frame_num + 1) * global_metadata.frame_samples) / global_metadata.frequency_samples
        mid_time = initial_time + ((end_time - initial_time) / 2)
        return initial_time, mid_time, end_time

    def __get_filepath(self, all_notes_results, run_mode, appendix):
        '''Returns the filepath relative to the all_notes_results.
        It assumes that all the elements contained in the argument all_notes_results
        are corresponds to the same music.
        '''
        filter_type = all_notes_results[0].filter_type
        ds_type = 'train' if run_mode == mg.RunMode.TRAINING else 'test'
        directory = self.output_folder + os.sep + filter_type + os.sep + ds_type
        filename = self.__get_filename(all_notes_results=all_notes_results,
                                       appendix=appendix)
        return (directory, filename)

    def __get_filename(self, all_notes_results, appendix):
        music_name = all_notes_results[0].filename
        return music_name + '_' + str(appendix)

    def __write_sequence_as_midi_and_plot(self, sequence, all_notes_results, run_mode, appendix):
        directory, filename = self.__get_filepath(all_notes_results=all_notes_results,
                                                  run_mode=run_mode,
                                                  appendix=appendix)
        if not os.path.isdir(directory):
            os.makedirs(directory)

        full_filepath = os.path.join(directory, filename)
        magenta.music.sequence_proto_to_midi_file(sequence, output_file=full_filepath + '.mid')
        figure = magenta.music.plot_sequence(sequence, show_figure=False)
        bokeh.plotting.save(figure, filename=full_filepath + '.html')
