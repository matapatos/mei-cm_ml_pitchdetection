import os
import numpy as np


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
from pitch_estimation.tasks.post_processing.types.possible_notes import PossibleNotes
from pitch_estimation.tasks.post_processing.types.discard_notes import DiscardNotes
import utils as mg_utils


class DiscardNotesPerNote(PostProcessing):

    def __init__(self, summary_output_folder, output_folderpath, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)

        self.identifier = identifier

        if summary_output_folder:
            summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(DiscardNotesPerNote, self).__init__(summary_output_folder=summary_output_folder)

        self.notes_number = notes_number
        if output_folderpath:
            output_folderpath = os.path.join(output_folderpath, str(identifier))
        self.output_folderpath = output_folderpath
        self.models_folder = None
        self.model_iteration = None
        self.best_models_folderpath = None

        self.discard_notes_units = {}
        self.is_model_restored = False
        self.is_training_phase_enabled = False
        self.variations = []
        self.__possible_notes_pp = None

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        new_all_notes_results = []
        for note_results in all_notes_results:
            if note_results.note_number not in self.discard_notes_units:
                mg.WARN('Missing single nn for note number %d' % (note_results.note_number))
            else:
                note_results = self.discard_notes_units[note_results.note_number].apply(all_notes_results=[note_results],
                                                                                        run_mode=run_mode,
                                                                                        is_last_music=is_last_music,
                                                                                        global_metadata=global_metadata,
                                                                                        best_model=best_model,
                                                                                        additional_data=additional_data,
                                                                                        debug=debug)[0]
                self.metrics.update_metrics(run_mode=run_mode,
                                            note_result=note_results,
                                            is_last_music=is_last_music)
                new_all_notes_results.append(note_results)

        return new_all_notes_results

    def increase_global_step(self):
        self.metrics.increase_global_step()
        for note_number, s in self.discard_notes_units.items():
            s.increase_global_step()

    def disable_training(self, recover_models_folder, recover_model_iteration, recover_best_models_folderpath):
        self.recover_models_folder = None
        self.recover_best_models_folderpath = None

        if recover_models_folder:
            self.recover_models_folder = os.path.join(recover_models_folder, self.identifier)
        self.recover_model_iteration = recover_model_iteration
        if recover_best_models_folderpath:
            self.recover_best_models_folderpath = os.path.join(recover_best_models_folderpath, self.identifier)
        for note_number, nn in self.discard_notes_units.items():
            recover_models_folder = None if self.recover_models_folder is None else os.path.join(self.recover_models_folder, str(note_number))
            recover_best_models_folderpath = None if self.recover_best_models_folderpath is None else os.path.join(self.recover_best_models_folderpath, str(note_number))

            nn.disable_training(recover_models_folder=recover_models_folder,
                                recover_model_iteration=self.recover_model_iteration,
                                recover_best_models_folderpath=recover_best_models_folderpath)
        self.is_training_phase_enabled = False

    def enable_training(self):
        mg.INFO('Post-processing unit with identifier \'%s\' is enabled for training with its following children Neural Networks:' % (self.identifier))
        for note_number, nn in self.discard_notes_units.items():
            mg.INFO('\t\'%s\' -> %s' % (nn.identifier, note_number))
            nn.enable_training(is_child=True)
        self.is_training_phase_enabled = True

    def get_possible_notes(self, note_results, global_metadata):
        if not self.__possible_notes_pp:
            self.__possible_notes_pp = PossibleNotes(None)
            self.__possible_notes_pp.__enter__()
            self.__possible_notes_pp.data = self.__possible_notes_pp.get_all(global_metadata)

        possible_notes = self.__possible_notes_pp.get_possible_notes(note_results)
        possible_notes = np.array(possible_notes)
        note_index = mg_utils.get_note_index_from_number(note_number=note_results.note_number)
        return possible_notes[:, note_index]

    def __get_best_models_folderpath_from_summary(self):
        try:
            main_folder, parts = self.summary_output_folder.split(mg.PARENT_SUMMARIES_FOLDER)
            date = parts.split(os.sep)[1]
            fullpath_models_folder = os.path.join(main_folder, mg.PARENT_MODELS_FOLDER)
            fullpath_models_folder = os.path.join(fullpath_models_folder, date)
            return os.path.join(fullpath_models_folder, self.get_class_name(self))
        except Exception:
            mg.ERROR('Trying to retrieve the output folder from an invalid folder name: %s' % (str(self.summary_output_folder)))

    def __get_output_folder_path_of_note(self, note_number):
        if not self.output_folderpath:
            return None
        else:
            output_folderpath = os.path.join(self.output_folderpath, str(note_number))
            if not os.path.isdir(output_folderpath):
                os.makedirs(output_folderpath)
            return output_folderpath

    def __enter__(self):
        if self.notes_number and not self.discard_notes_units:
            for n in self.notes_number:
                summary_output_folder = os.path.join(self.summary_output_folder, str(n))
                output_folderpath = self.__get_output_folder_path_of_note(note_number=n)
                discard_note = DiscardNotes(summary_output_folder=summary_output_folder,
                                            output_folderpath=output_folderpath,
                                            update_summaries=False,
                                            parent=self,
                                            identifier=self.identifier + '_discard_notes_' + str(n))
                discard_note.note_number = n
                discard_note.__enter__()
                self.discard_notes_units[int(n)] = discard_note
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.metrics.close()
        for note_number, s in self.discard_notes_units.items():
            s.__exit__(exc_t, exc_v, trace)

        self.discard_notes_units = {}
        if self.__possible_notes_pp:
            self.__possible_notes_pp.__exit__(exc_t, exc_v, trace)
