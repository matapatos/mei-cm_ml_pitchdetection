import os
import numpy as np
import tensorflow as tf


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
import pitch_estimation.tasks.learner.filters.models.models as models
import pitch_estimation.tasks.learner.filters.utils as filter_utils
from pitch_estimation.auxiliary.training.early_stopping import EarlyStopping
import pitch_estimation.auxiliary.metrics.utils as metrics_utils
np.random.seed(25)


class FixOffsets(PostProcessing):

    NR_FRAMES_AROUND_PIVOT = 4

    def __init__(self, summary_output_folder, output_folderpath, notes_number=None, identifier=None, update_summaries=True, parent=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        early_stopping = EarlyStopping(num_iterations_after_best=10,
                                       min_percentage=0.1,
                                       metrics=['fmeasure', 'fmeasure_onsetoffset'],
                                       is_training=True)
        if summary_output_folder:
            summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(FixOffsets, self).__init__(summary_output_folder=summary_output_folder,
                                         output_folderpath=output_folderpath,
                                         early_stopping=early_stopping)

        self.recover_models_folder = None
        self.recover_model_iteration = None
        self.recover_best_models_folderpath = None
        self.is_model_restored = False
        self.is_training_phase_enabled = False
        self.variations = []
        self.update_summaries = update_summaries
        self.parent = parent

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        old_run_mode = run_mode
        if not self.is_training_phase_enabled:
            if run_mode == mg.RunMode.TRAINING:
                run_mode = mg.RunMode.PREDICTING

        if run_mode == mg.RunMode.TRAINING:
            all_notes_results = self.__train(run_mode=run_mode,
                                             all_notes_results=all_notes_results,
                                             is_last_music=is_last_music,
                                             global_metadata=global_metadata,
                                             debug=debug)
        else:
            if run_mode == mg.RunMode.PREDICTING and not self.is_model_restored:
                self.__restore_model(best_model=best_model)

            all_notes_results = self.__predict_or_evaluate(run_mode=run_mode,
                                                           all_notes_results=all_notes_results,
                                                           is_last_music=is_last_music,
                                                           global_metadata=global_metadata,
                                                           debug=debug)

        run_mode = old_run_mode

        return all_notes_results

    def increase_global_step(self):
        self.metrics.increase_global_step()

    def disable_training(self, recover_models_folder, recover_model_iteration, recover_best_models_folderpath):
        if recover_models_folder:
            self.recover_models_folder = os.path.join(recover_models_folder, self.identifier)
        self.recover_model_iteration = recover_model_iteration
        if recover_best_models_folderpath:
            self.recover_best_models_folderpath = os.path.join(recover_best_models_folderpath, self.identifier)
        self.is_training_phase_enabled = False
        self.early_stopping.is_training = False

    def enable_training(self, is_child=False):
        if not is_child:
            mg.INFO('Post-processing unit with identifier \'%s\' is enabled for training.' % (self.identifier))
        self.is_training_phase_enabled = True
        self.early_stopping.is_training = True

    def __train(self, run_mode, all_notes_results, is_last_music, global_metadata, debug=False):
        algorithm_type = self.__mini_batch
        if mg.BATCH_SIZE == 1:
            algorithm_type = self.__sgd

        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y, indexes = self.__get_predictions_and_labels_of_predicted_offsets(note_results=note_results,
                                                                                                   is_training=True)

            if batches_x is not None and batches_y is not None:
                note_results = algorithm_type(note_results=note_results,
                                              batches_x=batches_x,
                                              batches_y=batches_y,
                                              indexes=indexes)

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __predict_or_evaluate(self, run_mode, all_notes_results, is_last_music, global_metadata, debug=False):
        operations = [self.prediction_op]
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y, indexes = self.__get_predictions_and_labels_of_predicted_offsets(note_results=note_results,
                                                                                                   is_training=False)
            if batches_x is not None and batches_y is not None:
                fixes_preds = self.session.run(operations, feed_dict={self.model.X: batches_x, self.model.Y: batches_y, self.model.is_training: False})
                self.__update_predictions_with_fixes(note_results=note_results,
                                                     fixes_preds=fixes_preds[0],
                                                     indexes=indexes)

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        save_best_model=True if run_mode == mg.RunMode.EVALUATING else False,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __sgd(self, note_results, batches_x, batches_y, indexes):
        operations = [self.train_op, self.prediction_op, self.loss_op]
        for i in range(0, len(batches_x)):
            batch_x = batches_x[i]
            batch_y = batches_y[i]

            batch_x = np.array(batch_x)
            batch_x = batch_x.reshape((1, len(batch_x)))
            batch_y = np.array(batch_y)
            batch_y = batch_y.reshape((1, len(batch_y)))

            _, fixes_preds, _ = self.session.run(operations, feed_dict={self.model.X: batch_x, self.model.Y: batch_y, self.model.is_training: True})
            self.__update_predictions_with_fixes(note_results=note_results,
                                                 fixes_preds=fixes_preds,
                                                 indexes=[indexes[i]])

        return note_results

    def __mini_batch(self, note_results, batches_x, batches_y, indexes):
        operations = [self.train_op, self.prediction_op]
        for i in range(0, len(batches_x), mg.BATCH_SIZE):
            end_index = min(len(batches_x), i + mg.BATCH_SIZE)
            batch_x = batches_x[i:end_index]
            batch_y = batches_y[i:end_index]

            _, fixes_preds = self.session.run(operations, feed_dict={self.model.X: batch_x, self.model.Y: batch_y, self.model.is_training: True})
            self.__update_predictions_with_fixes(note_results=note_results,
                                                 fixes_preds=fixes_preds,
                                                 indexes=indexes[i:end_index])

        return note_results

    def __get_predictions_and_labels_of_predicted_offsets(self, note_results, is_training):
        labels_notes_group = metrics_utils.get_notes_group(note_results.labels)
        predictions_notes_group = metrics_utils.get_notes_group(note_results.predictions)

        predictions = []
        labels = []
        indexes = []
        for p in predictions_notes_group:
            indexes.append(p.end_index)
            fix = self.__get_fix_offset_of_group(p, group=labels_notes_group)
            if fix == 0:
                label_ = (0, 1, 0)
            elif fix < 0:
                label_ = (1, 0, 0)
            else:
                label_ = (0, 0, 1)

            pred = self.__get_batches_of_index(predictions=note_results.predictions,
                                               index=p.start_index)
            predictions.append(pred)
            labels.append(label_)

        if is_training:
            predictions, labels, indexes = self.__shuffle_batches(predictions,
                                                                  labels,
                                                                  indexes=indexes)

        predictions, labels = self.__reshape_batches(predictions, labels)

        return predictions, labels, indexes

    def __update_predictions_with_fixes(self, note_results, fixes_preds, indexes):
        fixes_preds = self.__get_fixes_from_predictions(fixes_preds)
        size = len(note_results.predictions)
        for i, fix in zip(indexes, fixes_preds):
            # If fix == 0 does not change anything
            if fix < 0:
                note_results.predictions[i] = 0
            elif fix > 0:
                if i + 1 < size:
                    note_results.predictions[i + 1] = 1

    def __get_fixes_from_predictions(self, preds):
        fixes = []
        for p in preds:
            if p == 0:  # First index means left
                fixes.append(-1)
            elif p == 1:  # Means to stay in the same index
                fixes.append(0)
            elif p == 2:
                fixes.append(1)  # Means to the right
            else:
                raise ValueError('Unsupported value %s. Expected -1, 0 or 1' % (p))
        return fixes

    def __get_fix_offset_of_group(self, note_group, group):
        for g in group:
            if g.end_index == note_group.end_index:
                return 0  # Stay the same
            elif g.end_index > note_group.start_index:
                if g.end_index < note_group.end_index:
                    return -1
                elif g.start_index > note_group.end_index:
                    return -1
                else:
                    return 1
            elif g.end_index == note_group.start_index:
                return -1  # If it reaches here, it means that g.end_index is not equal to note_group.end_index.
                           # It can not be smaller. Thus, its certainly bigger and needs to be reduced.

        return -1  # It means that the predicted offset should not exist

    def __build_model(self):
        with self.graph.as_default():
            self.__define_hyper_parameters()
            self.__define_operations()
            self.__prepare_tensorflow_variables()

    def __define_hyper_parameters(self):
        self.learning_rate = 1e-3
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        num_input_neurons = FixOffsets.NR_FRAMES_AROUND_PIVOT * 2 + 1
        self.model = models.PPSingleNN(num_input_neurons=num_input_neurons,
                                       num_output_neurons=3,
                                       dropout_prob=0.15,
                                       noise_prob=0.7,
                                       noise_std=0.05,
                                       dropconnect_prob=0,
                                       random_spike_prob=0,
                                       spike_strength=0,
                                       opposite_sign_prob=0,
                                       use_batch_normalization=False,
                                       use_alpha_dropout=False)

        logits_with_distance_error = self.__get_logits_with_distance_error()
        self.loss_op = tf.losses.softmax_cross_entropy(onehot_labels=self.model.Y,
                                                       logits=logits_with_distance_error)

    def __get_logits_with_distance_error(self):
        '''Function that returns the logits that also takes into account the distance
        between the predicted frame and the label.

        Pseudocode of the calculation of the returned logits:
            distance_error = abs(arg_max(predictions) - arg_max(label))
            array_to_add = zeros(shape=predictions)
            array_to_add[arg_max(predictions)] += distance_error

            logits = self.model.logits + array_to_add
        '''
        if mg.BATCH_SIZE > 1:
            raise NotImplementedError('A batch size higher than 1 is not supported when using FixOnsets or FixOffsets PP unit')
        if self.model.output_act_func is not tf.nn.softmax:
            raise ValueError('Unsupported activation function')

        label_arg_max = tf.arg_max(self.model.Y, 1)
        # Calculate Distance error
        pred_arg_max = tf.arg_max(self.model.predictions, 1)
        distance_error = tf.abs(tf.subtract(label_arg_max, pred_arg_max))
        distance_error = tf.cast(distance_error, dtype=tf.float32)

        logits = tf.Variable(initial_value=(0.0, 0.0, 0.0), trainable=False)
        logits = tf.assign(logits, value=(0.0, 0.0, 0.0))
        scatter_op = tf.scatter_add(logits, indices=pred_arg_max, updates=distance_error)
        logits = tf.reshape(scatter_op, shape=(1, 3))
        return tf.add(self.model.logits, logits)

    def __define_operations(self):
        # Defining training operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):  # Necessary when using Batch Normalization
            gradients, variables = zip(*self.optimizer.compute_gradients(self.loss_op))
            gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
            self.train_op = self.optimizer.apply_gradients(zip(gradients, variables))
        # Defining prediction/evaluation operation
        self.prediction_op = tf.argmax(self.model.predictions, 1)

    def __prepare_tensorflow_variables(self):
        self.best_models_saver = tf.train.Saver(max_to_keep=4)  # For saving best models metrics
        init = tf.global_variables_initializer()
        self.session.run(init)

    def get_model_filename(self, model_iteration=None, with_extension=True):
        extension = mg.TENSORFLOW_MODEL_EXTENSION
        if not with_extension:
            extension = ""

        if model_iteration is not None:
            if model_iteration == 'last':
                extension = "_n\d{1,}" + extension
            else:
                extension = "_n" + str(model_iteration) + extension

        return "filter_global" + extension

    def __get_batches_of_index(self, predictions, index):
        pivot = FixOffsets.NR_FRAMES_AROUND_PIVOT
        if index < pivot:
            mg.ERROR('Invalid starting index')

        batch_x = np.array(predictions[index - pivot:index + pivot + 1])  # Its index + pivot + 1 because in Python the last index is exclusive
        batch_x = np.reshape(batch_x, (1, len(batch_x)))
        return batch_x

    def __shuffle_batches(self, x_batches, y_batches, indexes, shuffled_indexes=None):
        size = len(x_batches)
        if size != len(y_batches):
            mg.ERROR('Batches size mismatch!')

        if shuffled_indexes is None or shuffled_indexes is False:
            shuffled_indexes = np.random.permutation(size)

        new_x_batches = []
        new_y_batches = []
        new_indexes = []
        for index in shuffled_indexes:
            new_x_batches.append(x_batches[index])
            new_y_batches.append(y_batches[index])
            new_indexes.append(indexes[index])

        return new_x_batches, new_y_batches, new_indexes

    def __reshape_batches(self, x_batches, y_batches):
        if len(x_batches) == 0 or len(y_batches) == 0:
            return None, None

        x_batches = np.asarray(x_batches)
        x_batches = x_batches.reshape((len(x_batches), x_batches.shape[2]))
        y_batches = np.asarray(y_batches)
        y_batches = y_batches.reshape((len(y_batches), y_batches.shape[1]))

        return x_batches, y_batches

    def __restore_model(self, best_model):
        if not best_model:
            best_model = 'fmeasure_onsetoffset'

        if self.recover_models_folder is None:
            mg.ERROR("Models folder of PostProcessing unit \'%s\' is NoneType" % (self.identifier))

        models_folder = os.path.dirname(self.recover_models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = filter_utils.get_models_folder(filter_instance=self,
                                                       original_folder=models_folder,
                                                       parent=self.parent)

        full_filepath = filter_utils.get_model_filepath_by_best_metric(filter_instance=self,
                                                                       models_folder=models_folder,
                                                                       best_model=best_model)

        mg.INFO('\t%s --> \'%s\'' % (self.identifier, full_filepath))
        with self.graph.as_default():
            self.best_models_saver.restore(self.session, full_filepath)
            self.is_model_restored = True

    def __enter__(self):
        self.__build_model()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.metrics.close()
