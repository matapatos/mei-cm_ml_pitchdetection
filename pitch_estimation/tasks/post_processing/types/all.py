import pitch_estimation.tasks.post_processing.utils as utils
from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
from pitch_estimation.tasks.post_processing.types.onsets import Onsets
from pitch_estimation.tasks.post_processing.types.possible_notes import PossibleNotes
from pitch_estimation.tasks.post_processing.types.mirex_confusion_matrix import MIREXConfusionMatrix
from pitch_estimation.tasks.post_processing.types.write_output import WriteOutput
from pitch_estimation.tasks.post_processing.types.fill_holes import FillHoles
from pitch_estimation.tasks.post_processing.types.smoothing import Smoothing
from pitch_estimation.tasks.post_processing.types.single_nn import SingleNN
from pitch_estimation.tasks.post_processing.types.nn_per_note import NNPerNote
from pitch_estimation.tasks.post_processing.types.tomidi import ToMidi
from pitch_estimation.tasks.post_processing.types.fix_onsets import FixOnsets
from pitch_estimation.tasks.post_processing.types.fix_onsets_per_note import FixOnsetsPerNote
from pitch_estimation.tasks.post_processing.types.fix_offsets import FixOffsets
from pitch_estimation.tasks.post_processing.types.fix_offsets_per_note import FixOffsetsPerNote
from pitch_estimation.tasks.post_processing.types.discard_notes import DiscardNotes
from pitch_estimation.tasks.post_processing.types.discard_notes_per_note import DiscardNotesPerNote

all_pps = [Onsets, PossibleNotes, FillHoles, Smoothing, MIREXConfusionMatrix,
           WriteOutput, SingleNN, NNPerNote, ToMidi, FixOnsets, FixOnsetsPerNote,
           FixOffsets, FixOffsetsPerNote, DiscardNotes, DiscardNotesPerNote]  # Insert here all the existent PosProcessing classes
all_pps_names = [PostProcessing.get_class_name(pp) for pp in all_pps]
trainable_units = [SingleNN, NNPerNote]
units_dont_change_output_result = [MIREXConfusionMatrix, WriteOutput]
units_that_change_output_result = [pp for pp in all_pps if pp not in units_dont_change_output_result]


def get_pps_from_names(names):
    if names is None or names is False:
        return None, None

    if isinstance(names, str):
        names = [names]

    pps = []
    pps_names = []
    for name in names:
        current_pp = None
        for pp in all_pps:
            class_name = PostProcessing.get_class_name(pp)
            if name == class_name or utils.check_if_name_is_a_valid_variation(name, pp):
                current_pp = pp
                break

        if current_pp is None:
            return None, None
        else:
            pps.append(current_pp)
            pps_names.append(name)

    return pps, pps_names


def has_any_trainable_unit(names):
    pp_units = get_pps_from_names(names)
    if not pp_units:
        return False

    for pp in pp_units:
        if pp in trainable_units:
            return True

    return False
