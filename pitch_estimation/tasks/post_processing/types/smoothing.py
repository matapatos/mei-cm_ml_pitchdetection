import os
import numpy as np

from pitch_estimation.tasks.post_processing.post_processing import PostProcessing


class Smoothing(PostProcessing):

    MIN_PERCENTAGE = 0.9
    SMOOTH_PERCENTAGE = 0.05

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(Smoothing, self).__init__(summary_output_folder=summary_output_folder)

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            frames = np.copy(note_results.predictions)
            length = len(frames)
            for i in range(1, length - 1):
                smoothing_value = self.__get_smoothing_value_to_apply(frames=frames, index=i)
                note_results.predictions[i] += smoothing_value

            self.metrics.update_metrics(run_mode=run_mode, note_result=note_results, is_last_music=is_last_music)

        return all_notes_results

    def __get_smoothing_value_to_apply(self, frames, index):
        ''' Returns 0.0 if frames[index] already contains a note or none of the following conditions are met:
                -> Previous frame has a value equal or higher than Smoothing.MIN_PERCENTAGE
                -> Next frame has a value equal or higher than Smoothing.MIN_PERCENTAGE

            Otherwise it will return previous or next frame value multiplied by Smoothing.SMOOTH_PERCENTAGE'''
        if frames[index] > 0.5:  # If contains note just ignore
            return 0.0

        prev_frame = frames[index - 1]
        if prev_frame >= Smoothing.MIN_PERCENTAGE:
            return prev_frame * Smoothing.SMOOTH_PERCENTAGE

        next_frame = frames[index + 1]
        if next_frame >= Smoothing.MIN_PERCENTAGE:
            return next_frame * Smoothing.SMOOTH_PERCENTAGE

        return 0.0

    def __enter__(self):
        return self

    def __exit__(self, exc_t, exc_v, trace):
        pass
