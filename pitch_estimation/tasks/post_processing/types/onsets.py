import csv
import ast
import math
import os


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
import pitch_estimation.tasks.post_processing.configs as configs
from pitch_estimation.tasks.csv_reader.configs import CSV_DELIMITER


class Onsets(PostProcessing):

    HAS_ONSET = 1
    NR_FRAMES_AROUND_PIVOT = 1

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        if summary_output_folder:
            summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(Onsets, self).__init__(summary_output_folder=summary_output_folder)

        self.onsets_file = None
        self.onsets_reader = None
        self.data = None

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        if self.data is None:
            self.data = self.get_all(global_metadata=global_metadata)

        pp_all_notes_results = []
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            music_data = self.get_data_of_results(results=note_results)

            for onset_frame_nr in music_data[configs.H_ONSETS]:
                if self.__has_any_note_nearby(results=note_results, index=onset_frame_nr):
                    note_results = self.__assign_positive_frames_around_pivot(results=note_results, index=onset_frame_nr)

            self.metrics.update_metrics(run_mode=run_mode, note_result=note_results, is_last_music=is_last_music)
            pp_all_notes_results.append(note_results)
        return pp_all_notes_results

    def get_all(self, global_metadata):
        data = {}
        try:
            next(self.onsets_reader)  # Ignore headers line
        except StopIteration:
            mg.ERROR('File denominated by \'%r\' is empty.' % (mg.GET_ONSETS_OUTPUT_FILEPATH()))

        for d in self.onsets_reader:
            data[d[configs.H_FILENAME]] = self.__get_parsed_data(d, global_metadata=global_metadata)
        return data

    def get_data_of_results(self, results):
        filename = results.filename
        return self.data[filename]

    def __has_any_note_nearby(self, results, index, pivot=NR_FRAMES_AROUND_PIVOT):
        predictions = results.predictions
        last_index = min(index + pivot, len(predictions))
        for i in range(index, last_index):
            if round(predictions[i]) >= 1:
                return True
        return False

    def __assign_positive_frames_around_pivot(self, results, index, pivot=NR_FRAMES_AROUND_PIVOT):
        predictions = results.predictions
        last_index = min(index + pivot, len(predictions))
        for i in range(index, last_index):
            predictions[i] = 1.0

        results.predictions = predictions
        return results

    def __prepare_file_and_reader(self):
        self.onsets_file = open(mg.GET_ONSETS_OUTPUT_FILEPATH(), mode='r')
        self.onsets_reader = csv.DictReader(self.onsets_file,
                                            fieldnames=configs.ONSET_FIELDNAMES,
                                            delimiter=CSV_DELIMITER)

    def __get_parsed_data(self, data, global_metadata):
        onsets_str = data[configs.H_ONSETS].replace(' ', ',')
        data[configs.H_ONSETS] = ast.literal_eval(onsets_str)
        data = self.__convert_time_onsets_to_frames(data=data, global_metadata=global_metadata)
        return data

    def __convert_time_onsets_to_frames(self, data, global_metadata):
        time_onsets = data[configs.H_ONSETS]
        frame_onsets = []
        seconds_ratio = global_metadata.frequency_samples / global_metadata.frame_samples / global_metadata.overlap_ratio
        for t in time_onsets:
            result = math.floor(t * seconds_ratio)
            if result < 0:
                result = 0
            frame_onsets.append(result)
        data[configs.H_ONSETS] = frame_onsets
        return data

    def __enter__(self):
        self.__prepare_file_and_reader()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        if self.onsets_file and not self.onsets_file.closed:
            self.onsets_file.close()
            self.onsets_reader = None
            self.data = None
