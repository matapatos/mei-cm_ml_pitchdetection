import os
import numpy as np
import tensorflow as tf
import copy


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
import pitch_estimation.tasks.learner.filters.models.models as models
import pitch_estimation.tasks.learner.filters.utils as filter_utils
from pitch_estimation.tasks.post_processing.utils import get_ordered_variations_from_name
from pitch_estimation.auxiliary.training.early_stopping import EarlyStopping
np.random.seed(25)


class SingleNN(PostProcessing):

    NR_FRAMES_AROUND_PIVOT = 4
    MIN_PERCENTAGE_OF_RELEVANT_FRAMES = 0.2

    # To distiguish between a relevant frame and an irrelevant one
    RELEVANT = 2
    OTHERS = -2

    def __init__(self, summary_output_folder, output_folderpath, notes_number=None, identifier=None, update_summaries=True, parent=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        early_stopping = EarlyStopping(num_iterations_after_best=10, min_percentage=0.1, metrics='fmeasure', is_training=True)
        summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(SingleNN, self).__init__(summary_output_folder=summary_output_folder,
                                       output_folderpath=output_folderpath,
                                       early_stopping=early_stopping)

        self.recover_models_folder = None
        self.recover_model_iteration = None
        self.recover_best_models_folderpath = None
        self.is_model_restored = False
        self.is_training_phase_enabled = False
        self.variations = []
        self.update_summaries = update_summaries
        self.parent = parent
        self.has_onset_variation = False

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        old_run_mode = run_mode
        if not self.is_training_phase_enabled:
            if run_mode == mg.RunMode.TRAINING:
                run_mode = mg.RunMode.PREDICTING

        if run_mode == mg.RunMode.TRAINING:
            all_notes_results = self.__train(run_mode=run_mode,
                                             all_notes_results=all_notes_results,
                                             is_last_music=is_last_music,
                                             global_metadata=global_metadata,
                                             debug=debug)
        else:
            if run_mode == mg.RunMode.PREDICTING and not self.is_model_restored:
                self.__restore_model(best_model=best_model)

            all_notes_results = self.__predict_or_evaluate(run_mode=run_mode,
                                                           all_notes_results=all_notes_results,
                                                           is_last_music=is_last_music,
                                                           global_metadata=global_metadata,
                                                           debug=debug)

        run_mode = old_run_mode

        return all_notes_results

    def increase_global_step(self):
        self.metrics.increase_global_step()

    def disable_training(self, recover_models_folder, recover_model_iteration, recover_best_models_folderpath):
        if recover_models_folder:
            self.recover_models_folder = os.path.join(recover_models_folder, self.identifier)
        self.recover_model_iteration = recover_model_iteration
        if recover_best_models_folderpath:
            self.recover_best_models_folderpath = os.path.join(recover_best_models_folderpath, self.identifier)
        self.is_training_phase_enabled = False
        self.early_stopping.is_training = False

    def enable_training(self, is_child=False):
        if not is_child:
            mg.INFO('Post-processing unit with identifier \'%s\' is enabled for training.' % (self.identifier))
        self.is_training_phase_enabled = True
        self.early_stopping.is_training = True

    @staticmethod
    def get_class_name_variations():
        return ['_also_', '_onsets_', '_hasonset_']

    def set_possible_variation(self, name):
        if name != self.get_class_name(self):
            self.variations = get_ordered_variations_from_name(pp_unit=self, name=name)

    def __train(self, run_mode, all_notes_results, is_last_music, global_metadata, additional_data=None, debug=False):
        algorithm_type = self.__mini_batch
        if mg.BATCH_SIZE == 1:
            algorithm_type = self.__sgd

        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y, shuffled_indexes, ignored_frames_indexes = self.__get_prepared_batches(note_results=note_results)
            if batches_x is not None:
                if self.variations:
                    for v in self.variations:
                        if v.name == '_also_':
                            previous_note_results = self.__get_first_previous_note_results(note_results, identifier=v.ref)
                            v_batches_x, _, _, _ = self.__get_prepared_batches(note_results=previous_note_results,
                                                                               shuffled_indexes=shuffled_indexes,
                                                                               ignored_frames_indexes=ignored_frames_indexes)
                            if v_batches_x is not None:
                                batches_x = np.concatenate((batches_x, v_batches_x), axis=1)
                        elif v.name == '_onsets_':
                            onsets_batches = self.__get_prepared_onset_batches(run_mode=run_mode,
                                                                               note_results=note_results,
                                                                               global_metadata=global_metadata,
                                                                               shuffled_indexes=shuffled_indexes,
                                                                               ignored_frames_indexes=ignored_frames_indexes)
                            if onsets_batches is not None:
                                batches_x = np.concatenate((batches_x, onsets_batches), axis=1)
                        elif v.name == '_hasonset_':
                            has_onsets = self.__get_onsets(run_mode=run_mode,
                                                           note_results=note_results,
                                                           global_metadata=global_metadata,
                                                           shuffled_indexes=shuffled_indexes,
                                                           ignored_frames_indexes=ignored_frames_indexes)
                            if has_onsets is not None:
                                batches_x = np.concatenate((batches_x, has_onsets), axis=1)
                        else:
                            raise ValueError('Variation given not supported %s' % (v))

                note_results = algorithm_type(note_results=note_results,
                                              batches_x=batches_x,
                                              batches_y=batches_y,
                                              shuffled_indexes=shuffled_indexes)

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __predict_or_evaluate(self, run_mode, all_notes_results, is_last_music, global_metadata, debug=False):
        operations = [self.prediction_op]
        start_index = SingleNN.NR_FRAMES_AROUND_PIVOT
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y = self.__get_all_batches(predictions=note_results.predictions,
                                                          labels=note_results.labels)
            if self.variations:
                for v in self.variations:
                    if v.name == '_also_':
                        previous_note_results = self.__get_first_previous_note_results(note_results, identifier=v.ref)
                        v_batches_x, _ = self.__get_all_batches(predictions=previous_note_results.predictions,
                                                                labels=previous_note_results.labels)
                        batches_x = np.concatenate((batches_x, v_batches_x), axis=1)
                    elif v.name == '_onsets_':
                            onsets_batches = self.__get_all_onset_batches(run_mode=run_mode,
                                                                          note_results=note_results,
                                                                          global_metadata=global_metadata)
                            if onsets_batches is not None:
                                batches_x = np.concatenate((batches_x, onsets_batches), axis=1)
                    elif v.name == '_hasonset_':
                        has_onsets = self.__get_onsets(run_mode=run_mode,
                                                       note_results=note_results,
                                                       global_metadata=global_metadata)
                        if has_onsets is not None:
                            batches_x = np.concatenate((batches_x, has_onsets), axis=1)
                    else:
                        raise ValueError('Variation given not supported %s' % (v))
            predictions = self.session.run(operations, feed_dict={self.model.X: batches_x, self.model.Y: batches_y, self.model.is_training: False})

            predictions = np.array(predictions)
            predictions = predictions.reshape((len(batches_y),))
            end_index = len(note_results.predictions) - SingleNN.NR_FRAMES_AROUND_PIVOT
            for i in range(start_index, end_index):
                note_results.predictions[i] = predictions[i - start_index]

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        save_best_model=True if run_mode == mg.RunMode.EVALUATING else False,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __sgd(self, note_results, batches_x, batches_y, shuffled_indexes):
        operations = [self.train_op, self.prediction_op]
        for i in range(0, len(batches_x)):
            batch_x = batches_x[i]
            batch_y = batches_y[i]

            batch_x = np.array(batch_x)
            batch_x = batch_x.reshape((1, len(batch_x)))
            batch_y = np.array(batch_y)
            batch_y = batch_y.reshape((1, len(batch_y)))

            _, pred = self.session.run(operations, feed_dict={self.model.X: batch_x, self.model.Y: batch_y, self.model.is_training: True})

            note_results.predictions[shuffled_indexes[i]] = pred[0][0]

        return note_results

    def __mini_batch(self, note_results, batches_x, batches_y, shuffled_indexes):
        operations = [self.train_op, self.prediction_op]
        for i in range(0, len(batches_x), mg.BATCH_SIZE):
            end_index = min(len(batches_x), i + mg.BATCH_SIZE)
            batch_x = batches_x[i:end_index]
            batch_y = batches_y[i:end_index]

            _, predictions = self.session.run(operations, feed_dict={self.model.X: batch_x, self.model.Y: batch_y, self.model.is_training: True})

            predictions = np.array(predictions)
            predictions = predictions.reshape((len(predictions),))
            indexes = shuffled_indexes[i:end_index]
            for j in range(0, len(predictions)):
                note_results.predictions[indexes[j]] = predictions[j]

        return note_results

    def __build_model(self):
        with self.graph.as_default():
            self.__define_hyper_parameters()
            self.__define_operations()
            self.__prepare_tensorflow_variables()

    def __define_hyper_parameters(self):
        self.learning_rate = 1e-5
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        num_input_neurons = (SingleNN.NR_FRAMES_AROUND_PIVOT * 2 + 1) * (self.variations_size + 1)
        if self.has_onset_variation:
            num_input_neurons -= SingleNN.NR_FRAMES_AROUND_PIVOT * 2
        self.model = models.PPSingleNN(num_input_neurons=num_input_neurons,
                                       num_output_neurons=1,
                                       dropout_prob=0.15,
                                       noise_prob=0.7,
                                       noise_std=0.05,
                                       dropconnect_prob=0,
                                       random_spike_prob=0,
                                       spike_strength=0,
                                       opposite_sign_prob=0,
                                       use_batch_normalization=False,
                                       use_alpha_dropout=False)
        self.loss_op = tf.losses.sigmoid_cross_entropy(multi_class_labels=self.model.Y,
                                                       logits=self.model.logits)

    def __define_operations(self):
        # Defining training operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):  # Necessary when using Batch Normalization
            gradients, variables = zip(*self.optimizer.compute_gradients(self.loss_op))
            gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
            self.train_op = self.optimizer.apply_gradients(zip(gradients, variables))
        # Defining prediction/evaluation operation
        self.prediction_op = self.model.predictions

    def __prepare_tensorflow_variables(self):
        self.best_models_saver = tf.train.Saver(max_to_keep=4)  # For saving best models metrics
        init = tf.global_variables_initializer()
        self.session.run(init)

    def get_model_filename(self, model_iteration=None, with_extension=True):
        extension = mg.TENSORFLOW_MODEL_EXTENSION
        if not with_extension:
            extension = ""

        if model_iteration is not None:
            if model_iteration == 'last':
                extension = "_n\d{1,}" + extension
            else:
                extension = "_n" + str(model_iteration) + extension

        return "filter_global" + extension

    def __get_all_batches(self, predictions, labels):
        start_index = SingleNN.NR_FRAMES_AROUND_PIVOT
        end_index = len(predictions) - SingleNN.NR_FRAMES_AROUND_PIVOT

        batches_x = []
        batches_y = []
        for i in range(start_index, end_index):
            batch_x, batch_y = self.__get_batches_of_index(predictions=predictions,
                                                           labels=labels,
                                                           index=i)
            batches_x.append(batch_x)
            batches_y.append(batch_y)

        return self.__reshape_batches(batches_x, batches_y)

    def __get_prepared_batches(self, note_results, ignored_frames_indexes=None, shuffled_indexes=None):
        x_batches, y_batches, categories, num_of_relevant_frames, num_of_other_frames = self.__get_batches_categorized(note_results)

        if num_of_relevant_frames == 0:  # Ignore musics that does not contain the note
            return None, None, None, None

        if ignored_frames_indexes is None or ignored_frames_indexes is False:
            ignored_frames_indexes = []
            total_frames = num_of_relevant_frames + num_of_other_frames
            if total_frames > 0:
                ratio = num_of_relevant_frames / total_frames
                if SingleNN.MIN_PERCENTAGE_OF_RELEVANT_FRAMES and ratio < SingleNN.MIN_PERCENTAGE_OF_RELEVANT_FRAMES:
                    others_percentage = 1 - SingleNN.MIN_PERCENTAGE_OF_RELEVANT_FRAMES
                    nr_other_frames_to_ignore = round(num_of_relevant_frames * others_percentage / SingleNN.MIN_PERCENTAGE_OF_RELEVANT_FRAMES)
                    x_batches, y_batches, ignored_frames_indexes = self.__ignore_other_frames(x_batches=x_batches,
                                                                                              y_batches=y_batches,
                                                                                              categories=categories,
                                                                                              num_frames_to_ignore=nr_other_frames_to_ignore)
        else:
            x_batches, y_batches, _ = self.__ignore_other_frames(x_batches=x_batches,
                                                                 y_batches=y_batches,
                                                                 categories=categories,
                                                                 ignored_frames_indexes=ignored_frames_indexes)

        x_batches, y_batches, shuffled_indexes = self.__shuffle_batches(x_batches, y_batches, shuffled_indexes=shuffled_indexes)  # Shuffle order of batches before returning
        x_batches, y_batches = self.__reshape_batches(x_batches, y_batches)
        return x_batches, y_batches, shuffled_indexes, ignored_frames_indexes

    def __get_batches_of_index(self, predictions, labels, index):
        pivot = SingleNN.NR_FRAMES_AROUND_PIVOT
        if index < pivot:
            mg.ERROR('Invalid starting index')

        batch_x = np.array(predictions[index - pivot:index + pivot + 1])  # Its index + pivot + 1 because in Python the last index is exclusive
        batch_x = np.reshape(batch_x, (1, len(batch_x)))
        batch_y = np.array(labels[index])
        batch_y = np.reshape(batch_y, (1, 1))
        return batch_x, batch_y

    def __get_batches_categorized(self, note_results):
        x_batches = []
        y_batches = []
        categories = []
        num_of_relevant_frames = 0
        num_of_other_frames = 0

        start_index = SingleNN.NR_FRAMES_AROUND_PIVOT
        end_index = len(note_results.predictions) - SingleNN.NR_FRAMES_AROUND_PIVOT
        for i in range(start_index, end_index):
            batch_x, batch_y = self.__get_batches_of_index(predictions=note_results.predictions,
                                                           labels=note_results.labels,
                                                           index=i)

            x_batches.append(batch_x)
            y_batches.append(batch_y)

            label = round(note_results.labels[i])
            pred = round(note_results.predictions[i])
            if label == 1 or label != pred:
                categories.append(SingleNN.RELEVANT)
                num_of_relevant_frames += 1
            else:
                categories.append(SingleNN.OTHERS)
                num_of_other_frames += 1

        assert len(x_batches) == len(y_batches)
        assert len(x_batches) == len(categories)

        return x_batches, y_batches, categories, num_of_relevant_frames, num_of_other_frames

    def __ignore_other_frames(self, x_batches, y_batches, categories, num_frames_to_ignore=None, ignored_frames_indexes=None):
        size = min(len(x_batches), len(y_batches))
        n = num_frames_to_ignore if num_frames_to_ignore else len(ignored_frames_indexes)
        if size < n:
            mg.WARN('Trying to remove more frames than possible. Nr of irrelevant frames: %d\tNr of frames asked to ignore: %d' % (size, n))

        if num_frames_to_ignore is None and (ignored_frames_indexes is None or ignored_frames_indexes is False):
            mg.ERROR('One of the following arguments need to be passed to the function \'__ignore_other_frames\': \'num_frames_to_ignore\' or \'ignored_frames_indexes\'')

        if ignored_frames_indexes is None or ignored_frames_indexes is False:
            others = []
            for i, category in enumerate(categories):
                if category == SingleNN.OTHERS:
                    others.append(i)
            # Shuffle indexes
            np.random.shuffle(others)
            ignored_frames_indexes = others[:num_frames_to_ignore]

        ignored_frames_indexes.sort()
        for index in reversed(ignored_frames_indexes):
            x_batches.pop(index)
            y_batches.pop(index)

        return x_batches, y_batches, ignored_frames_indexes

    def __shuffle_batches(self, x_batches, y_batches, shuffled_indexes=None):
        size = len(x_batches)
        if size != len(y_batches):
            mg.ERROR('Batches size mismatch!')

        if shuffled_indexes is None or shuffled_indexes is False:
            shuffled_indexes = np.random.permutation(size)

        new_x_batches = []
        new_y_batches = []
        for index in shuffled_indexes:
            new_x_batches.append(x_batches[index])
            new_y_batches.append(y_batches[index])

        return new_x_batches, new_y_batches, shuffled_indexes

    def __reshape_batches(self, x_batches, y_batches):
        x_batches = np.array(x_batches)
        x_batches = x_batches.reshape((len(x_batches), x_batches.shape[2]))
        y_batches = np.array(y_batches)
        y_batches = y_batches.reshape((len(y_batches), y_batches.shape[2]))

        return x_batches, y_batches

    def __restore_model(self, best_model):
        if not best_model:
            best_model = 'fmeasure'

        if self.recover_models_folder is None:
            mg.ERROR("Models folder of PostProcessing unit \'%s\' is NoneType" % (self.identifier))

        models_folder = os.path.dirname(self.recover_models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = filter_utils.get_models_folder(filter_instance=self,
                                                       original_folder=models_folder,
                                                       parent=self.parent)

        full_filepath = filter_utils.get_model_filepath_by_best_metric(filter_instance=self,
                                                                       models_folder=models_folder,
                                                                       best_model=best_model)

        mg.INFO('\t%s --> \'%s\'' % (self.identifier, full_filepath))
        with self.graph.as_default():
            self.best_models_saver.restore(self.session, full_filepath)
            self.is_model_restored = True

    def __get_first_previous_note_results(self, note_results, identifier):
        previous_note_results = note_results.get_first_note_results_from_creator_identifier(identifier)
        if not previous_note_results:
            ref = identifier + '_' + str(note_results.note_number) + '_' + str(note_results.filter_type)
            previous_note_results = note_results.get_first_note_results_from_creator_identifier(ref)
            if not previous_note_results:
                ref = identifier + '_single_nn_' + str(note_results.note_number)
                previous_note_results = note_results.get_first_note_results_from_creator_identifier(ref)

        if not previous_note_results:
            raise ValueError('Invalid identifier given %s on class %s' % (identifier, self.identifier))

        return previous_note_results

    def __get_onsets(self, run_mode, note_results, global_metadata, shuffled_indexes=None, ignored_frames_indexes=None):
        if not self.parent:
            raise TypeError('self.parent is not defined')

        frame_onsets = self.parent.get_frame_onsets(run_mode=run_mode,
                                                    note_results=note_results,
                                                    global_metadata=global_metadata)
        assert len(frame_onsets) == len(note_results.labels)
        start_index = SingleNN.NR_FRAMES_AROUND_PIVOT
        end_index = len(frame_onsets) - SingleNN.NR_FRAMES_AROUND_PIVOT
        frame_onsets = frame_onsets[start_index:end_index]
        if isinstance(ignored_frames_indexes, list) or isinstance(ignored_frames_indexes, np.ndarray):
            frame_onsets = frame_onsets.tolist()
            for index in reversed(ignored_frames_indexes):
                frame_onsets.pop(index)

        if isinstance(shuffled_indexes, list) or isinstance(shuffled_indexes, np.ndarray):
            new_onsets = []
            for index in shuffled_indexes:
                new_onsets.append(frame_onsets[index])
        frame_onsets = np.array(frame_onsets)
        return frame_onsets.reshape(len(frame_onsets), 1)

    def __get_prepared_onset_batches(self, run_mode, note_results, global_metadata, shuffled_indexes, ignored_frames_indexes):
        all_onset_batches = self.__get_all_onset_batches(run_mode=run_mode,
                                                         note_results=note_results,
                                                         global_metadata=global_metadata)
        if isinstance(ignored_frames_indexes, list) or isinstance(ignored_frames_indexes, np.ndarray):
            all_onset_batches = all_onset_batches.tolist()
            for index in reversed(ignored_frames_indexes):
                all_onset_batches.pop(index)

        if isinstance(shuffled_indexes, list) or isinstance(shuffled_indexes, np.ndarray):
            new_onset_batches = []
            for index in shuffled_indexes:
                new_onset_batches.append(all_onset_batches[index])
            all_onset_batches = new_onset_batches
        return np.array(all_onset_batches)

    def __get_all_onset_batches(self, run_mode, note_results, global_metadata):
        if not self.parent:
            raise TypeError('self.parent is not defined')

        frame_onsets = self.parent.get_frame_onsets(run_mode=run_mode,
                                                    note_results=note_results,
                                                    global_metadata=global_metadata)
        assert len(frame_onsets) == len(note_results.labels)

        start_index = SingleNN.NR_FRAMES_AROUND_PIVOT
        end_index = len(frame_onsets) - SingleNN.NR_FRAMES_AROUND_PIVOT

        batches_onsets = []
        for i in range(start_index, end_index):
            batch = np.array(frame_onsets[i - SingleNN.NR_FRAMES_AROUND_PIVOT:i + SingleNN.NR_FRAMES_AROUND_PIVOT + 1])  # Its index + pivot + 1 because in Python the last index is exclusive
            batch = np.reshape(batch, (1, len(batch)))
            batches_onsets.append(batch)

        batches_onsets = np.array(batches_onsets)
        batches_onsets = batches_onsets.reshape((len(batches_onsets), batches_onsets.shape[2]))
        return batches_onsets

    def __enter__(self):
        self.__build_model()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.metrics.close()
