import csv
import ast
import os
import sys
try:
    csv.field_size_limit(sys.maxsize)
except:
    pass


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
import utils as mg_utils
import pitch_estimation.tasks.post_processing.configs as configs
from pitch_estimation.tasks.csv_reader.configs import CSV_DELIMITER


class PossibleNotes(PostProcessing):

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        if summary_output_folder:
            summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(PossibleNotes, self).__init__(summary_output_folder=summary_output_folder)

        self.possible_notes_file = None
        self.possible_notes_reader = None
        self.data = None

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        if self.data is None:
            self.data = self.get_all(global_metadata=global_metadata)

        pp_all_notes_results = []
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            possible_notes = self.get_possible_notes(note_results=note_results)
            note_result = self.__discard_notes_not_in_possible_ones(note_result=note_results,
                                                                    possible_notes=possible_notes)

            self.metrics.update_metrics(run_mode=run_mode, note_result=note_result, is_last_music=is_last_music)
            pp_all_notes_results.append(note_results)
        return pp_all_notes_results

    def get_possible_notes(self, note_results):
        music_data = self.__get_data_of_results(results=note_results)
        return music_data[configs.H_POSSIBLE_NOTES]

    def get_all(self, global_metadata):
        data = {}
        try:
            next(self.possible_notes_reader)  # Ignore headers line
        except StopIteration:
            mg.ERROR('File denominated by \'%r\' is empty.' % (mg.GET_POSSIBLE_NOTES_OUTPUT_FILEPATH()))

        for d in self.possible_notes_reader:
            data[d[configs.H_FILENAME]] = self.__get_parsed_data(d, global_metadata=global_metadata)
        return data

    def __discard_notes_not_in_possible_ones(self, note_result, possible_notes):
        note_index = mg_utils.get_note_index_from_number(note_number=note_result.note_number)
        for i, (pred, possible_notes_per_frame) in enumerate(zip(note_result.predictions, possible_notes)):
            if possible_notes_per_frame[note_index] == 0:
                note_result.predictions[i] = 0
        return note_result

    def __prepare_file_and_reader(self):
        self.possible_notes_file = open(mg.GET_POSSIBLE_NOTES_OUTPUT_FILEPATH(), mode='r')
        self.possible_notes_reader = csv.DictReader(self.possible_notes_file,
                                                    fieldnames=configs.POSSIBLE_NOTES_FIELDNAMES,
                                                    delimiter=CSV_DELIMITER)

    def __get_parsed_data(self, data, global_metadata):
        possible_notes_str = data[configs.H_POSSIBLE_NOTES].replace(' ', ',').replace(';', '],[')
        possible_notes_str = '[' + possible_notes_str + ']'
        data[configs.H_POSSIBLE_NOTES] = ast.literal_eval(possible_notes_str)
        return data

    def __get_data_of_results(self, results):
        filename = results.filename
        return self.data[filename]

    def __enter__(self):
        self.__prepare_file_and_reader()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        if self.possible_notes_file and not self.possible_notes_file.closed:
            self.possible_notes_file.close()
            self.possible_notes_reader = None
            self.data = None
