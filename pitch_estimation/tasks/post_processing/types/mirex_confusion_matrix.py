import os
import numpy as np
import collections


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
from pitch_estimation.auxiliary.metrics.utils import ConfusionMatrix
import pitch_estimation.tasks.learner.filters.configs as filter_configs
import main_globals as mg
import utils as mg_utils


class MIREXConfusionMatrix(PostProcessing):

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        super(MIREXConfusionMatrix, self).__init__(summary_output_folder=None)

        self.output_filepath = os.path.join(summary_output_folder, str(identifier) + '.csv')
        self.notes_cf = collections.defaultdict(dict)

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        for note_results in all_notes_results:
            # Insert here PosProcessing!!
            cf = self.__generate_note_confusion_matrix(note_results=note_results, all_notes_results=all_notes_results)
            self.__append_cf(note_results=note_results, cf=cf)

        return all_notes_results

    def __generate_note_confusion_matrix(self, note_results, all_notes_results):
        rounded_predictions = np.rint(note_results.predictions)

        cf_m = ConfusionMatrix(0, 0, 0, 0)
        note_cf_m = collections.defaultdict(int)
        for i, (label, pred) in enumerate(zip(note_results.labels, rounded_predictions)):
            if not cf_m.update(label=label, pred=pred):
                note_cf_m = self.__get_fp_notes_on_index(note_cf_m=note_cf_m,
                                                         note_results=note_results,
                                                         all_notes_results=all_notes_results,
                                                         index=i)
        note_cf_m[note_results.note_number] = cf_m.tp
        return note_cf_m

    def __append_cf(self, note_results, cf):
        if note_results.filter_type not in self.notes_cf:
            self.notes_cf[note_results.filter_type] = collections.defaultdict(lambda: collections.defaultdict(int))

        current_note_cf = self.notes_cf[note_results.filter_type][note_results.note_number]
        for other_note_number, number_of_times in cf.items():
            current_note_cf[other_note_number] += number_of_times

    def __get_fp_notes_on_index(self, note_cf_m, note_results, all_notes_results, index):
        had_any_note_played_instead = False
        for other_note_results in all_notes_results:
            if note_results.note_number == other_note_results.note_number:
                continue

            pred = np.round(other_note_results.predictions[index])
            label = other_note_results.labels[index]
            if pred == 1 and label == 0:  # If is a false positive
                note_cf_m[other_note_results.note_number] += 1
                had_any_note_played_instead = True

        if not had_any_note_played_instead:
            note_cf_m[filter_configs.SILENCE_NOTE_NUMBER] += 1

        return note_cf_m

    def __enter__(self):
        return self

    def __exit__(self, exc_t, exc_v, trace):
        if self.notes_cf:
            column_sep = ','
            lines = ''
            all_notes = mg.GET_ALL_NOTES()
            all_notes.append(filter_configs.SILENCE_NOTE_NUMBER)
            str_all_notes = mg_utils.array_to_string(all_notes).replace(',', column_sep)
            for filter_type, all_cf_notes in self.notes_cf.items():
                lines += '\n\n----- ' + str(filter_type) + ' -----\n'
                lines += column_sep + str_all_notes + '\n'
                for n in all_notes:
                    lines += str(n) + column_sep
                    for note in all_notes:
                        val = all_cf_notes[n][note]
                        if val == 0:
                            val = ''
                        lines += str(val) + column_sep
                    lines += '\n'

            with open(self.output_filepath, mode='a') as f:
                f.writelines(lines)
