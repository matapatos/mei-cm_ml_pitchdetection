import os
import collections
import numpy as np


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
from pitch_estimation.tasks.csv_reader.configs import CSV_DELIMITER, MUSIC_SEP
from pitch_estimation.tasks.post_processing.utils import get_ordered_variations_from_name
import main_globals as mg


class WriteOutput(PostProcessing):

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        super(WriteOutput, self).__init__(summary_output_folder=None)

        self.files = {}
        self.filename = os.path.join(summary_output_folder, str(identifier) + '_')
        self.variations = []

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        if all_notes_results:
            predictions, labels = self.__get_predictions_with_variations_if_any(all_notes_results=all_notes_results)
            filter_type = all_notes_results[0].filter_type
            predictions_file, labels_file = self.__get_files(run_mode=run_mode, filter_type=filter_type)
            music_name = all_notes_results[0].filename
            self.__write_predictions_and_labels(labels_file=labels_file,
                                                predictions_file=predictions_file,
                                                labels=labels,
                                                predictions=predictions,
                                                music_name=music_name)
        return all_notes_results

    def __get_predictions_with_variations_if_any(self, all_notes_results):
        predictions, labels = self.__convert_all_notes_to_88_notes_binary_array(all_notes_results=all_notes_results)

        for i, v in enumerate(self.variations, 1):
            if v.name == '_also_':
                previous_all_notes_results = self.__get_previous_all_notes_results(all_notes_results=all_notes_results, ref=v.ref)
                prev_predictions, _ = self.__convert_all_notes_to_88_notes_binary_array(all_notes_results=previous_all_notes_results)

                start_index = i * mg.NUMBER_OF_NOTES
                predictions = self.__join_predictions(predictions=predictions,
                                                      predictions_to_append=prev_predictions,
                                                      start_index=start_index)
        return predictions, labels

    def __join_predictions(self, predictions, predictions_to_append, start_index=mg.NUMBER_OF_NOTES):
        assert len(predictions) == len(predictions_to_append)

        notes_number = predictions_to_append[0].keys()
        for i, p_2 in enumerate(predictions_to_append):
            frame_data = predictions[i]
            for note_number in notes_number:
                frame_data[note_number + start_index] = p_2[note_number]
        return predictions

    def __get_previous_all_notes_results(self, all_notes_results, ref):
        previous_all_notes_results = []
        for note_results in all_notes_results:
            previous_note_results = self.__get_first_previous_note_results(note_results, identifier=ref)
            if previous_note_results is None:
                raise ValueError('Couldnt find any previous note results with a name \'%s\' and a ref \'%s\'' % (ref))
            previous_all_notes_results.append(previous_note_results)

        assert len(previous_all_notes_results) == len(all_notes_results)
        return previous_all_notes_results

    @staticmethod
    def get_class_name_variations():
        return ['_also_']

    def set_possible_variation(self, name):
        if name != self.get_class_name(self):
            self.variations = get_ordered_variations_from_name(pp_unit=self, name=name)

    def __get_first_previous_note_results(self, note_results, identifier):
        previous_note_results = note_results.get_first_note_results_from_creator_identifier(identifier)
        if not previous_note_results:
            ref = identifier + '_' + str(note_results.note_number) + '_' + str(note_results.filter_type)
            previous_note_results = note_results.get_first_note_results_from_creator_identifier(ref)
            if not previous_note_results:
                ref = identifier + '_single_nn_' + str(note_results.note_number)
                previous_note_results = note_results.get_first_note_results_from_creator_identifier(ref)

        if not previous_note_results:
            raise ValueError('Invalid identifier given %s on class %s' % (identifier, self.identifier))

        return previous_note_results

    def __convert_all_notes_to_88_notes_binary_array(self, all_notes_results):
        predictions = []
        labels = []
        nr_frames = len(all_notes_results[0].labels)
        for f in range(nr_frames):
            # For fullfilling the other notes too
            predictions.append(collections.defaultdict(int))
            labels.append(collections.defaultdict(int))

            for note_results in all_notes_results:
                predictions[f][note_results.note_number] = note_results.predictions[f]
                labels[f][note_results.note_number] = note_results.labels[f]            

        return np.array(predictions), np.array(labels)

    def __get_files(self, run_mode, filter_type):
        run_mode = str(run_mode).lower().replace('runmode.', '')
        key = str(run_mode) + '_' + str(filter_type)
        if key not in self.files:
            # Open files
            predictions_file = open(self.filename + key + '_preds.csv', 'a')
            labels_file = open(self.filename + key + '_labels.csv', 'a')
            # Write headers
            predictions_file.write(self.__get_headers(is_for_prediction_file=True))
            labels_file.write(self.__get_headers())
            # Save files in local variable
            self.files[key] = (predictions_file, labels_file)

        return self.files[key]

    def __get_headers(self, is_for_prediction_file=False):
        headers = ''
        max_range = mg.NUMBER_OF_NOTES
        if is_for_prediction_file:
            max_range = max_range * (len(self.variations) + 1)
        for n in range(mg.FIRST_NOTE_NUMBER, mg.FIRST_NOTE_NUMBER + max_range):
            headers += str(n) + ','
        return headers[:-1] + '\n'

    def __write_predictions_and_labels(self, labels_file, predictions_file, labels, predictions, music_name):
        # Write music name to files
        labels_file.write(music_name + '\n')
        predictions_file.write(music_name + '\n')
        # Prepare data
        labels_str = self.__convert_array_to_csv_data(labels)
        predictions_str = self.__convert_array_to_csv_data(predictions, is_prediction=True)
        # Write data to files
        labels_file.write(labels_str)
        predictions_file.write(predictions_str)
        # Write separator to files
        labels_file.write(self.__get_music_separator(size=mg.NUMBER_OF_NOTES))
        predictions_file.write(self.__get_music_separator(size=len(predictions[0])))

    def __get_music_separator(self, size):
        data = [MUSIC_SEP] * size
        return str(data).replace('[', '').replace(']', '').replace(' ', '') + '\n'

    def __convert_array_to_csv_data(self, array_, is_prediction=False):
        csv_data = ''
        for f in range(len(array_)):
            line = ''
            max_range = mg.NUMBER_OF_NOTES
            if is_prediction:
                max_range = max_range * (len(self.variations) + 1)
            for n in range(mg.FIRST_NOTE_NUMBER, mg.FIRST_NOTE_NUMBER + max_range):
                line += str(array_[f][n]) + CSV_DELIMITER

            line = line[:-1] + '\n'
            csv_data += line
        return csv_data

    def __enter__(self):
        return self

    def __exit__(self, exc_t, exc_v, trace):
        if self.files:
            for key, (l_file, pred_file) in self.files.items():
                if l_file and not l_file.closed:
                    l_file.close()
                if pred_file and not pred_file.closed:
                    pred_file.close

        self.files = []
