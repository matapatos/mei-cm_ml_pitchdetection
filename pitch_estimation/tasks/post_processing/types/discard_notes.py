import os
import numpy as np
import tensorflow as tf
import copy


from pitch_estimation.tasks.post_processing.post_processing import PostProcessing
import main_globals as mg
import pitch_estimation.tasks.learner.filters.models.models as models
import pitch_estimation.tasks.learner.filters.utils as filter_utils
from pitch_estimation.auxiliary.training.early_stopping import EarlyStopping
import pitch_estimation.auxiliary.metrics.utils as metrics_utils
np.random.seed(25)


class DiscardNotes(PostProcessing):

    MIN_RATIO = 0.2

    def __init__(self, summary_output_folder, output_folderpath, notes_number=None, identifier=None, update_summaries=True, parent=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        early_stopping = EarlyStopping(num_iterations_after_best=200,
                                       min_percentage=0.1,
                                       metrics=['fmeasure', 'fmeasure_onset', 'fmeasure_onsetoffset'],
                                       is_training=True)
        if summary_output_folder:
            summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(DiscardNotes, self).__init__(summary_output_folder=summary_output_folder,
                                           output_folderpath=output_folderpath,
                                           early_stopping=early_stopping)

        self.recover_models_folder = None
        self.recover_model_iteration = None
        self.recover_best_models_folderpath = None
        self.is_model_restored = False
        self.is_training_phase_enabled = False
        self.variations = []
        self.update_summaries = update_summaries
        self.parent = parent
        if not self.parent:
            from pitch_estimation.tasks.post_processing.types.discard_notes_per_note import DiscardNotesPerNote
            self.parent = DiscardNotesPerNote(None, None, notes_number=notes_number)

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        old_run_mode = run_mode
        if not self.is_training_phase_enabled:
            if run_mode == mg.RunMode.TRAINING:
                run_mode = mg.RunMode.PREDICTING

        if run_mode == mg.RunMode.TRAINING:
            all_notes_results = self.__train(run_mode=run_mode,
                                             all_notes_results=all_notes_results,
                                             is_last_music=is_last_music,
                                             global_metadata=global_metadata,
                                             debug=debug)
        else:
            if run_mode == mg.RunMode.PREDICTING and not self.is_model_restored:
                self.__restore_model(best_model=best_model)

            all_notes_results = self.__predict_or_evaluate(run_mode=run_mode,
                                                           all_notes_results=all_notes_results,
                                                           is_last_music=is_last_music,
                                                           global_metadata=global_metadata,
                                                           debug=debug)

        run_mode = old_run_mode

        return all_notes_results

    def increase_global_step(self):
        self.metrics.increase_global_step()

    def disable_training(self, recover_models_folder, recover_model_iteration, recover_best_models_folderpath):
        if recover_models_folder:
            self.recover_models_folder = os.path.join(recover_models_folder, self.identifier)
        self.recover_model_iteration = recover_model_iteration
        if recover_best_models_folderpath:
            self.recover_best_models_folderpath = os.path.join(recover_best_models_folderpath, self.identifier)
        self.is_training_phase_enabled = False
        self.early_stopping.is_training = False

    def enable_training(self, is_child=False):
        if not is_child:
            mg.INFO('Post-processing unit with identifier \'%s\' is enabled for training.' % (self.identifier))
        self.is_training_phase_enabled = True
        self.early_stopping.is_training = True

    def __train(self, run_mode, all_notes_results, is_last_music, global_metadata, debug=False):
        algorithm_type = self.__mini_batch
        if mg.BATCH_SIZE == 1:
            algorithm_type = self.__sgd

        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y, num_correct_frames_discarded, num_wrong_frames_discarded, indexes = self.__get_predictions_and_labels_from_group(note_results=note_results,
                                                                                                                                                   is_training=True,
                                                                                                                                                   global_metadata=global_metadata)

            if len(batches_x) > 0 and len(batches_y):
                note_results = algorithm_type(note_results=note_results,
                                              batches_x=batches_x,
                                              batches_y=batches_y,
                                              num_correct_frames_discarded=num_correct_frames_discarded,
                                              num_wrong_frames_discarded=num_wrong_frames_discarded,
                                              indexes=indexes)

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __predict_or_evaluate(self, run_mode, all_notes_results, is_last_music, global_metadata, debug=False):
        operations = [self.prediction_op]
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            batches_x, batches_y, num_correct_frames_discarded, num_wrong_frames_discarded, indexes = self.__get_predictions_and_labels_from_group(note_results=note_results,
                                                                                                                                                   is_training=False,
                                                                                                                                                   global_metadata=global_metadata)
            if len(batches_x) > 0 and len(batches_y):
                fixes_preds = self.session.run(operations, feed_dict={self.model.X: batches_x,
                                                                      self.model.Y: batches_y,
                                                                      self.model.is_training: False,
                                                                      self.num_correct_frames_discarded: num_correct_frames_discarded,
                                                                      self.num_wrong_frames_discarded: num_wrong_frames_discarded})
                self.__update_predictions_with_fixes(note_results=note_results,
                                                     fixes_preds=fixes_preds[0],
                                                     indexes=indexes)

            self.metrics.update_metrics(run_mode=run_mode,
                                        note_result=note_results,
                                        is_last_music=is_last_music,
                                        save_best_model=True if run_mode == mg.RunMode.EVALUATING else False,
                                        update_summaries=self.update_summaries)

        return all_notes_results

    def __sgd(self, note_results, batches_x, batches_y, num_correct_frames_discarded, num_wrong_frames_discarded, indexes):
        operations = [self.train_op, self.prediction_op, self.loss_op]
        for i in range(0, len(batches_x)):
            batch_x = batches_x[i]
            batch_y = batches_y[i]
            num_correct_frames = num_correct_frames_discarded[i]
            num_wrong_frames = num_wrong_frames_discarded[i]

            batch_x = np.array(batch_x)
            batch_x = batch_x.reshape((1, len(batch_x)))
            batch_y = np.array(batch_y)
            batch_y = batch_y.reshape((1, len(batch_y)))
            num_correct_frames = np.array(num_correct_frames)
            num_correct_frames = num_correct_frames.reshape((1, 1))
            num_wrong_frames = np.array(num_wrong_frames)
            num_wrong_frames = num_wrong_frames.reshape((1, 1))

            _, fixes_preds, _ = self.session.run(operations, feed_dict={self.model.X: batch_x,
                                                                        self.model.Y: batch_y,
                                                                        self.model.is_training: True,
                                                                        self.num_correct_frames_discarded: num_correct_frames,
                                                                        self.num_wrong_frames_discarded: num_wrong_frames})
            self.__update_predictions_with_fixes(note_results=note_results,
                                                 fixes_preds=fixes_preds,
                                                 indexes=[indexes[i]])

        return note_results

    def __mini_batch(self, note_results, batches_x, batches_y, num_correct_frames_discarded, num_wrong_frames_discarded, indexes):
        operations = [self.train_op, self.prediction_op]
        for i in range(0, len(batches_x), mg.BATCH_SIZE):
            end_index = min(len(batches_x), i + mg.BATCH_SIZE)
            batch_x = batches_x[i:end_index]
            batch_y = batches_y[i:end_index]
            num_correct_frames = num_correct_frames_discarded[i:end_index]
            num_wrong_frames = num_wrong_frames_discarded[i:end_index]

            _, fixes_preds = self.session.run(operations, feed_dict={self.model.X: batch_x,
                                                                     self.model.Y: batch_y,
                                                                     self.model.is_training: True,
                                                                     self.num_correct_frames_discarded: num_correct_frames,
                                                                     self.num_wrong_frames_discarded: num_wrong_frames})
            self.__update_predictions_with_fixes(note_results=note_results,
                                                 fixes_preds=fixes_preds,
                                                 indexes=indexes[i:end_index])

        return note_results

    def __get_predictions_and_labels_from_group(self, note_results, is_training, global_metadata):
        predictions_group = metrics_utils.get_notes_group(note_results.predictions)
        if len(predictions_group) == 0:
            return (), (), (), (), ()

        possible_notes = self.parent.get_possible_notes(note_results=note_results,
                                                        global_metadata=global_metadata)
        x_batches = []
        y_batches = []
        discard_indexes = []
        others_indexes = []
        num_correct_frames_discarded = []
        num_wrong_frames_discarded = []
        indexes = []
        for i, note_group in enumerate(predictions_group):
            note_possible_group = self.__get_batch_from_group(note_group=note_group,
                                                              data_array=possible_notes)
            note_frames_batch = self.__get_batch_from_group(note_group=note_group,
                                                            data_array=note_results.predictions)
            num_of_correct_frames, num_of_wrong_frames = self.__get_num_frames_in_common_between_indices(start_index=note_group.start_index,
                                                                                                         end_index=note_group.end_index,
                                                                                                         labels=note_results.labels)
            batch_x = np.append(note_possible_group, note_frames_batch)
            if num_of_correct_frames > 0:
                batch_y = 0
                others_indexes.append(i)
            else:
                batch_y = 1
                discard_indexes.append(i)

            indexes.append((note_group.start_index, note_group.end_index))
            num_correct_frames_discarded.append(num_of_correct_frames)
            num_wrong_frames_discarded.append(num_of_wrong_frames)
            x_batches.append(batch_x)
            y_batches.append(batch_y)

        if is_training:
            x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, indexes = self.__ignore_if_needed(x_batches=x_batches,
                                                                                                                              y_batches=y_batches,
                                                                                                                              num_correct_frames_discarded=num_correct_frames_discarded,
                                                                                                                              num_wrong_frames_discarded=num_wrong_frames_discarded,
                                                                                                                              discard_indexes=discard_indexes,
                                                                                                                              others_indexes=others_indexes,
                                                                                                                              indexes=indexes)
            x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, indexes = self.__shuffle_batches(x_batches=x_batches,
                                                                                                                             y_batches=y_batches,
                                                                                                                             num_correct_frames_discarded=num_correct_frames_discarded,
                                                                                                                             num_wrong_frames_discarded=num_wrong_frames_discarded,
                                                                                                                             indexes=indexes)

        x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded = self.__reshape_batches(x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded)
        return x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, indexes

    def __get_batch_from_group(self, note_group, data_array):
        start_index = note_group.start_index - 1
        if start_index < 0:
            start_index = 0
        end_index = min(note_group.start_index + 5, note_group.end_index)
        batch = data_array[start_index:end_index]
        if len(batch) != 6:
            if start_index == 0:
                batch.insert(0, 0)  # Insert 0 in the left side of the array

            if len(batch) != 6:
                missing_elements = 6 - len(batch)
                if missing_elements <= 0:
                    raise ValueError('Too many elements in the variable batch. Limit size of 6 but %d exists' % (len(batch)))

                batch = np.append(batch, np.zeros(missing_elements))  # Append zeros in the right side of the array

        return batch

    def __get_num_frames_in_common_between_indices(self, start_index, end_index, labels):
        num_of_correct_frames = np.count_nonzero(labels[start_index:end_index + 1])
        size = end_index - start_index + 1
        num_of_wrong_frames = size - num_of_correct_frames
        return num_of_correct_frames, num_of_wrong_frames

    def __update_predictions_with_fixes(self, note_results, fixes_preds, indexes):
        if len(fixes_preds.shape) > 1:
            fixes_preds = np.reshape(fixes_preds, newshape=(len(fixes_preds), ))

        for index, pred in zip(indexes, fixes_preds):
            rounded_pred = round(pred)
            if rounded_pred >= 1:
                self.__zeroed_note_group_by_frame_index(note_results=note_results,
                                                        start_index=index[0],
                                                        end_index=index[1])

    def __zeroed_note_group_by_frame_index(self, note_results, start_index, end_index):
        if len(note_results.predictions) == 0:
            raise ValueError('Empty preditions array')

        if end_index >= len(note_results.predictions) or start_index < 0:
            raise IndexError('Index out of range.')

        if type(note_results.predictions) is not np.ndarray:
            note_results.predictions = np.asarray(note_results.predictions)

        note_results.predictions[start_index:end_index] = 0

    def __build_model(self):
        with self.graph.as_default():
            self.__define_hyper_parameters()
            self.__define_operations()
            self.__prepare_tensorflow_variables()

    def __define_hyper_parameters(self):
        self.learning_rate = 1e-5
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        self.num_correct_frames_discarded = tf.placeholder(dtype=tf.float32, shape=[None, 1])
        self.num_wrong_frames_discarded = tf.placeholder(dtype=tf.float32, shape=[None, 1])
        self.model = models.PPSingleNN(num_input_neurons=6 * 2,
                                       num_output_neurons=1,
                                       dropout_prob=0.15,
                                       noise_prob=0.7,
                                       noise_std=0.05,
                                       dropconnect_prob=0,
                                       random_spike_prob=0,
                                       spike_strength=0,
                                       opposite_sign_prob=0,
                                       use_batch_normalization=False,
                                       use_alpha_dropout=False)

        self.loss_op = self.__get_loss_func_that_takes_into_account_num_frames_missed()

    def __get_loss_func_that_takes_into_account_num_frames_missed(self):
        '''
        This function gives more enfasis of not discarding correct frames than wrong frames!


        loss = min(sigmoid_cross_entropy(labels, logits), 5)
        rounded_preds = round(predictions)
        if rounded_preds == labels:
            return loss
        else:
            additional_loss = num_correct_frames_discarded - 1.0 - num_wrong_frames_discarded / 2
            additional_loss = max(1.5, additional_loss)  # Make sure that the loss is always positive and higher than normal loss
            return loss * additional_loss
        '''
        loss = tf.minimum(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.model.logits, labels=self.model.Y), 5.0)
        equal_cond = tf.equal(tf.round(self.model.predictions), self.model.Y)
        equal_cond = tf.reshape(equal_cond, shape=())
        loss_op = tf.cond(equal_cond, lambda: loss, lambda: loss * tf.maximum(1.5, self.num_correct_frames_discarded - 1.0 - self.num_wrong_frames_discarded / 2))
        return loss_op

    def __define_operations(self):
        # Defining training operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):  # Necessary when using Batch Normalization
            gradients, variables = zip(*self.optimizer.compute_gradients(self.loss_op))
            gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
            self.train_op = self.optimizer.apply_gradients(zip(gradients, variables))
        # Defining prediction/evaluation operation
        self.prediction_op = self.model.predictions

    def __prepare_tensorflow_variables(self):
        self.best_models_saver = tf.train.Saver(max_to_keep=4)  # For saving best models metrics
        init = tf.global_variables_initializer()
        self.session.run(init)

    def get_model_filename(self, model_iteration=None, with_extension=True):
        extension = mg.TENSORFLOW_MODEL_EXTENSION
        if not with_extension:
            extension = ""

        if model_iteration is not None:
            if model_iteration == 'last':
                extension = "_n\d{1,}" + extension
            else:
                extension = "_n" + str(model_iteration) + extension

        return "filter_global" + extension

    def __ignore_if_needed(self, x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, discard_indexes, others_indexes, indexes):
        if len(discard_indexes) == 0 or len(others_indexes) == 0:  # Ignore unbalanced data
            return (), (), (), (), ()

        # Check which array is bigger
        if len(discard_indexes) > len(others_indexes):
            bigger_array = copy.copy(discard_indexes)
            smaller_array = copy.copy(others_indexes)
        else:
            bigger_array = copy.copy(others_indexes)
            smaller_array = copy.copy(discard_indexes)

        # Ignore frames from the bigger array if does not follow the DiscardNotes.MIN_RATION technique (20% postives and 80% negatives, or etc)
        total_size = len(bigger_array) + len(smaller_array)
        ratio = len(smaller_array) / total_size
        if ratio < DiscardNotes.MIN_RATIO:
            bigger_percentage = 1 - DiscardNotes.MIN_RATIO
            max_num_of_samples = round(len(smaller_array) * bigger_percentage / DiscardNotes.MIN_RATIO)
            assert max_num_of_samples > 0
            if max_num_of_samples < len(bigger_array):
                np.random.shuffle(bigger_array)
                indexes_to_ignore = bigger_array[:len(bigger_array) - max_num_of_samples]
                indexes_to_ignore.sort()
                for i in reversed(indexes_to_ignore):
                    x_batches.pop(i)
                    y_batches.pop(i)
                    num_correct_frames_discarded.pop(i)
                    num_wrong_frames_discarded.pop(i)
                    indexes.pop(i)

        return x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, indexes

    def __shuffle_batches(self, x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded, indexes, shuffled_indexes=None):
        size = len(x_batches)
        if size != len(y_batches):
            raise ValueError('Batches size mismatch!')

        if shuffled_indexes is None or shuffled_indexes is False:
            shuffled_indexes = np.random.permutation(size)

        new_x_batches = []
        new_y_batches = []
        new_num_correct_frames_discarded = []
        new_num_wrong_frames_discarded = []
        new_indexes = []
        for index in shuffled_indexes:
            new_x_batches.append(x_batches[index])
            new_y_batches.append(y_batches[index])
            new_num_correct_frames_discarded.append(num_correct_frames_discarded[index])
            new_num_wrong_frames_discarded.append(num_wrong_frames_discarded[index])
            new_indexes.append(indexes[index])

        return new_x_batches, new_y_batches, new_num_correct_frames_discarded, new_num_wrong_frames_discarded, new_indexes

    def __reshape_batches(self, x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded):
        if len(x_batches) == 0 or len(y_batches) == 0:
            return x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded

        x_batches = np.asarray(x_batches)
        x_batches = x_batches.reshape((len(x_batches), x_batches.shape[1]))
        y_batches = np.asarray(y_batches)
        y_batches = y_batches.reshape((len(y_batches), 1))
        num_correct_frames_discarded = np.asarray(num_correct_frames_discarded)
        num_correct_frames_discarded = num_correct_frames_discarded.reshape((len(num_correct_frames_discarded), 1))
        num_wrong_frames_discarded = np.asarray(num_wrong_frames_discarded)
        num_wrong_frames_discarded = num_wrong_frames_discarded.reshape((len(num_wrong_frames_discarded), 1))

        return x_batches, y_batches, num_correct_frames_discarded, num_wrong_frames_discarded

    def __restore_model(self, best_model):
        if not best_model:
            best_model = 'fmeasure_onset'

        if self.recover_models_folder is None:
            mg.ERROR("Models folder of PostProcessing unit \'%s\' is NoneType" % (self.identifier))

        models_folder = os.path.dirname(self.recover_models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = os.path.dirname(models_folder)
        models_folder = filter_utils.get_models_folder(filter_instance=self,
                                                       original_folder=models_folder,
                                                       parent=self.parent)

        full_filepath = filter_utils.get_model_filepath_by_best_metric(filter_instance=self,
                                                                       models_folder=models_folder,
                                                                       best_model=best_model)

        mg.INFO('\t%s --> \'%s\'' % (self.identifier, full_filepath))
        with self.graph.as_default():
            self.best_models_saver.restore(self.session, full_filepath)
            self.is_model_restored = True

    def __enter__(self):
        self.__build_model()
        return self

    def __exit__(self, exc_t, exc_v, trace):
        self.metrics.close()
