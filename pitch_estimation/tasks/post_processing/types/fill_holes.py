import os
import numpy as np

from pitch_estimation.tasks.post_processing.post_processing import PostProcessing


class FillHoles(PostProcessing):

    HAS_NOTE = 1
    NO_NOTE = 0

    def __init__(self, summary_output_folder, output_folderpath=None, notes_number=None, identifier=None):
        if not identifier:
            identifier = PostProcessing.get_class_name(self)
        self.identifier = identifier

        summary_output_folder = os.path.join(summary_output_folder, str(identifier))
        super(FillHoles, self).__init__(summary_output_folder=summary_output_folder)

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        for note_results in all_notes_results:
            note_results = note_results.new_instance(creator_identifier=self.identifier)
            frames = np.rint(np.copy(note_results.predictions))
            length = len(frames)
            for i in range(1, length - 1):
                if self.__has_hole(frames=frames, index=i):
                    note_results.predictions[i] = 1.0

            self.metrics.update_metrics(run_mode=run_mode, note_result=note_results, is_last_music=is_last_music)

        return all_notes_results

    def __has_hole(self, frames, index):
        '''Only considers a hole if:
            frames[index] -> Don't contain a detected note.
            &&
            frames[index - 1] && frames[index + 1] -> Contains a detected note.

            Examples:
                0010100 will be transformed to: 0011100
                0010010 will stay the same:     0010010
        '''
        if frames[index] == FillHoles.HAS_NOTE:
            return False

        prev_frame = frames[index - 1]
        if prev_frame == FillHoles.NO_NOTE:
            return False

        next_frame = frames[index + 1]
        if next_frame == FillHoles.NO_NOTE:
            return False

        return True

    def __enter__(self):
        return self

    def __exit__(self, exc_t, exc_v, trace):
        pass
