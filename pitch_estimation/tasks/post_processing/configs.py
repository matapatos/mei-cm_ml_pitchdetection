
H_FILENAME = 'filename'
H_ONSETS = 'onsets'
H_POSSIBLE_NOTES = 'possible_notes'

# Onsets configurations
ONSET_FIELDNAMES = [H_FILENAME, H_ONSETS]

# Highest picks configurations
POSSIBLE_NOTES_FIELDNAMES = [H_FILENAME, H_POSSIBLE_NOTES]
