import copy
import collections


import main_globals as mg
from pitch_estimation.tasks.post_processing.post_processing import PostProcessing


Variation = collections.namedtuple('Variation', ['name', 'ref'])


def get_ordered_variations_from_name(pp_unit, name):
    class_name = pp_unit.get_class_name(pp_unit)
    if class_name == name:
        return []

    name = name.split(class_name, 1)[1]
    refs = __get_variations_refs_from_name(pp_unit=pp_unit, name_parts=[name])
    return __get_variations_from_refs(pp_unit=pp_unit, refs=refs, name=name)


def check_if_name_is_a_valid_variation(name, pp_unit):
    variations = pp_unit.get_class_name_variations()
    class_name = PostProcessing.get_class_name(pp_unit)
    if class_name in name:
        for v in variations:
            if class_name + str(v) in name:
                return True
    return False


def __get_variations_refs_from_name(pp_unit, name_parts):
    variations = pp_unit.get_class_name_variations()
    for v in variations:
        for i, part in enumerate(name_parts):
            if v in part:
                new_parts = part.split(v)
                clone = copy.deepcopy(name_parts)
                name_parts = name_parts[:i]
                for new_p in new_parts:
                    if new_p:
                        name_parts.append(new_p)
                if len(clone) > i + 1:
                    name_parts.extend(clone[i + 1:])

                name_parts = __get_variations_refs_from_name(pp_unit=pp_unit,
                                                             name_parts=name_parts)
                return name_parts

    return name_parts


def __get_variations_from_refs(pp_unit, refs, name):
    variations = []
    all_pp_variations = pp_unit.get_class_name_variations()
    for r in refs:
        parts = name.split(r, 1)
        v = parts[0]
        if v not in all_pp_variations:
            mg.ERROR('Invalid variation \'%s\' of post-processing unit \'%s\'' % (v, pp_unit.get_class_name(pp_unit)))
        variations.append(Variation(name=v, ref=r))
        name = parts[1]

    return variations
