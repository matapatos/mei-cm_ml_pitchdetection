import abc


import utils
from pitch_estimation.auxiliary.metrics.controllers import GlobalMetrics


class PostProcessing(GlobalMetrics):
    def __init__(self, summary_output_folder, output_folderpath=None, graph=None, early_stopping=False):
        if summary_output_folder:
            super(PostProcessing, self).__init__(summary_output_folder=summary_output_folder,
                                                 best_model_folderpath=output_folderpath,
                                                 graph=graph,
                                                 early_stopping=early_stopping)
        self.variations_size = 0

    def apply(self, all_notes_results, run_mode, is_last_music, global_metadata, best_model=None, additional_data=None, debug=False):
        raise NotImplementedError()

    @abc.abstractstaticmethod
    def get_class_name(pp_class):
        try:
            name = pp_class.__name__
        except AttributeError:
            name = type(pp_class).__name__

        name = utils.convert_to_underscore_and_lowercase(name=name)
        return name.lower()

    @staticmethod
    def get_class_name_variations():
        return []

    def set_possible_variation(self, name):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_t, exc_v, trace):
        pass
