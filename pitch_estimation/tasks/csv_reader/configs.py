'''
This file contains all the configuration variables used during the ML script
'''
import pitch_estimation.tasks.csv_reader.utils as utils


CSV_DELIMITER = ','
TRAINING_CSV_FIELDNAMES = utils.get_training_CSV_fieldnames()
EVALUATION_CSV_FIELDNAMES = utils.get_evaluation_CSV_fieldnames()
GLOBAL_METADATA_CSV_FIELDNAMES = None
LOCAL_METADATA_CSV_FIELDNAMES = None

MUSIC_SEP = -9
