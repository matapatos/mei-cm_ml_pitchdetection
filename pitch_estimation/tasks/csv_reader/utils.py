import main_globals as mg


def get_training_CSV_fieldnames(num_samples=mg.NUMBER_OF_SAMPLES_PER_FRAME):
    fieldnames = []
    for i in range(num_samples):
        fieldnames.append(i)
    return fieldnames


def get_evaluation_CSV_fieldnames(first_note=mg.FIRST_NOTE_NUMBER, number_of_notes=mg.NUMBER_OF_NOTES):
    fieldnames = []
    for i in range(first_note, first_note + number_of_notes):
        fieldnames.append(i)
    return fieldnames
