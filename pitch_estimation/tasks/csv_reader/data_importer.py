'''
Class used to
'''
import csv
import numpy as np


import pitch_estimation.tasks.csv_reader.configs as configs
import main_globals as mg
from pitch_estimation.auxiliary.data.metadata.global_metadata import GlobalMetadata
from pitch_estimation.auxiliary.data.metadata.local_metadata import LocalMetadata
from threads.data.models.data import Data


class CSVDataImporter:
    def __init__(self, is_training):
        self.is_training = is_training
        self.num_musics_read = 0
        # Filepath's
        self.training_filepath = None
        self.testing_filepath = None
        self.training_evaluation_filepath = None
        self.testing_evaluation_filepath = None
        self.training_global_meta_filepath = None
        self.testing_global_meta_filepath = None
        self.training_local_meta_filepath = None
        self.testing_local_meta_filepath = None
        # File's Objects
        self.training_file = None
        self.testing_file = None
        self.training_evaluation_file = None
        self.testing_evaluation_file = None
        self.training_global_meta_file = None
        self.testing_global_meta_file = None
        self.training_local_meta_file = None
        self.testing_local_meta_file = None
        # Reader's properties
        self.training_reader = None
        self.testing_reader = None
        self.training_evaluation_reader = None
        self.testing_evaluation_reader = None
        self.trianing_global_meta_reader = None
        self.testing_global_meta_reader = None
        self.training_local_meta_reader = None
        self.testing_local_meta_reader = None

        self.testing_global_meta = None
        self.training_global_meta = None
        self.__is_prepared = False

    def produce_training_data(self, training_shared_data):
        self.__produce_data(shared_data=training_shared_data,
                            nextMusicFunction=self.__next_training_music)

    def produce_evaluation_data(self, evaluation_shared_data):
        self.__produce_data(shared_data=evaluation_shared_data,
                            nextMusicFunction=self.__next_evaluation_music)

    def next_prediction_music(self):
        return self.__next_evaluation_music()

    def get_training_global_metadata(self):
        if self.training_global_meta is None:
            self.training_global_meta = self.__get_global_metadata(global_meta_reader=self.training_global_meta_reader,
                                                                   is_training_data=True)
        return self.training_global_meta

    def get_testing_global_metadata(self):
        if self.testing_global_meta is None:
            self.testing_global_meta = self.__get_global_metadata(global_meta_reader=self.testing_global_meta_reader,
                                                                  is_training_data=False)
        return self.testing_global_meta

    def reset_reading_indexes(self):
        if self.is_training:
            self.training_file.seek(0)
            self.training_evaluation_file.seek(0)
            self.training_local_meta_file.seek(0)

        self.testing_file.seek(0)
        self.testing_evaluation_file.seek(0)
        self.testing_local_meta_file.seek(0)
        self.__reopen_local_meta_readers()

    def __set_filepaths(self):
        # Filepath's
        self.training_filepath = mg.GET_TRAINING_FILEPATH()
        self.testing_filepath = mg.GET_TESTING_FILEPATH()
        self.training_evaluation_filepath = mg.GET_TRAINING_OUTPUT_FILEPATH()
        self.testing_evaluation_filepath = mg.GET_TESTING_OUTPUT_FILEPATH()
        self.training_global_meta_filepath = mg.GET_TRAINING_GLOBAL_METADATA_FILEPATH()
        self.testing_global_meta_filepath = mg.GET_TESTING_GLOBAL_METADATA_FILEPATH()
        self.training_local_meta_filepath = mg.GET_TRAINING_LOCAL_METADATA_FILEPATH()
        self.testing_local_meta_filepath = mg.GET_TESTING_LOCAL_METADATA_FILEPATH()

    def __produce_data(self, shared_data, nextMusicFunction):
        shared_data.set_done(done=False)
        hasNext, training_data, evaluation_data, local_meta = nextMusicFunction()
        while hasNext:
            if len(training_data) <= 0:
                mg.WARN('The sound with the following filename {0} doesn\'t have any training data.'.format(
                    local_meta.filename))

            data = Data(data_in=training_data,
                        data_out=evaluation_data,
                        local_meta=local_meta)

            if not shared_data.append(data=data):
                break

            hasNext, training_data, evaluation_data, local_meta = nextMusicFunction()

        shared_data.set_done(done=True)

    def __next_training_music(self):
        return self.__next_music(input_data_reader=self.training_reader,
                                 output_data_reader=self.training_evaluation_reader,
                                 local_meta_reader=self.training_local_meta_reader,
                                 local_file=self.training_local_meta_file)

    def __next_evaluation_music(self):
        return self.__next_music(input_data_reader=self.testing_reader,
                                 output_data_reader=self.testing_evaluation_reader,
                                 local_meta_reader=self.testing_local_meta_reader,
                                 local_file=self.testing_local_meta_file)

    def __next_music(self, input_data_reader, output_data_reader, local_meta_reader, local_file):
        if not self.__is_prepared:
            mg.ERROR('Files are not opened already. Make sure that you use \'openFiles\' function before using \'nextMusic\'')
            return False, [], [], []

        hasNext, input_data = self.__next_music_data(reader=input_data_reader, row_converter=self.__convert_row_to_float_array)
        if hasNext:
            if hasNext:
                local_meta = self.__next_local_metatada(local_meta_reader=local_meta_reader, local_file=local_file)
                if hasNext:
                    hasNext, output_data = self.__next_music_data(reader=output_data_reader,
                                                                  row_converter=self.__convert_row_to_float_array,
                                                                  metadata=local_meta)

                    return hasNext, input_data, output_data, local_meta

        return False, [], [], []

    # Note that if row_converter is None, it uses by default the function __convertRowToFloatArray
    def __next_music_data(self, reader, row_converter=None, metadata=None):
        hasNext = False
        data = []

        if row_converter is None:
            row_converter = self.__convert_row_to_float_array

        for row in reader:  # Loop to gathered all the associated rows of samples of a music. It breaks when music separator token founded or end of file
            if None in row.keys():
                print(row.keys())
                mg.ERROR("Trying to read a line of the CSV file bigger than the number of columns specified. If this is the training csv file, make sure that the actual file as {0} number of columns.".format(mg.NUMBER_OF_SAMPLES_PER_FRAME))

            if self.__is_end_of_music(row):
                hasNext = True  # It means that it just arrived to the end of the music not to the end of the file specifically
                break
            row_data = row_converter(row, metadata)
            data.append(row_data)

        return hasNext, data

    def __next_local_metatada(self, local_meta_reader, local_file):
        try:
            metadata = next(local_meta_reader)
            local_meta = LocalMetadata(metadata)
            return local_meta

        except Exception:
            mg.ERROR("An error occoured when trying to get the line number {0} of file '{1}'".format(local_meta_reader.line_num, local_file.name))

    def __convert_row_to_float_array(self, row, metadata):
        return np.asfarray(list(row.values()))

    def __is_end_of_music(self, row, music_sep=configs.MUSIC_SEP):
        row_values = list(row.values())
        # We convert to float instead of int because in training dataset the values are float's and if you try to convert them directly to int it gives an exception
        return (float(row_values[0]) == music_sep)

    def __reopen_local_meta_readers(self):
        if self.is_training:
            self.training_local_meta_reader = csv.DictReader(self.training_local_meta_file,
                                                             fieldnames=configs.LOCAL_METADATA_CSV_FIELDNAMES,
                                                             delimiter=configs.CSV_DELIMITER)

        self.testing_local_meta_reader = csv.DictReader(self.testing_local_meta_file,
                                                        fieldnames=configs.LOCAL_METADATA_CSV_FIELDNAMES,
                                                        delimiter=configs.CSV_DELIMITER)

    def __init_CSV_readers(self):
        if self.is_training:
            self.training_global_meta_reader = csv.DictReader(self.training_global_meta_file,
                                                              fieldnames=configs.GLOBAL_METADATA_CSV_FIELDNAMES,
                                                              delimiter=configs.CSV_DELIMITER)
            self.training_reader = csv.DictReader(self.training_file,
                                                  fieldnames=configs.TRAINING_CSV_FIELDNAMES,
                                                  delimiter=configs.CSV_DELIMITER)
            self.training_evaluation_reader = csv.DictReader(self.training_evaluation_file,
                                                             fieldnames=configs.EVALUATION_CSV_FIELDNAMES,
                                                             delimiter=configs.CSV_DELIMITER)
            self.training_local_meta_reader = csv.DictReader(self.training_local_meta_file,
                                                             fieldnames=configs.LOCAL_METADATA_CSV_FIELDNAMES,
                                                             delimiter=configs.CSV_DELIMITER)

        self.testing_global_meta_reader = csv.DictReader(self.testing_global_meta_file,
                                                         fieldnames=configs.GLOBAL_METADATA_CSV_FIELDNAMES,
                                                         delimiter=configs.CSV_DELIMITER)
        self.testing_reader = csv.DictReader(self.testing_file,
                                             fieldnames=configs.TRAINING_CSV_FIELDNAMES,
                                             delimiter=configs.CSV_DELIMITER)
        self.testing_evaluation_reader = csv.DictReader(self.testing_evaluation_file,
                                                        fieldnames=configs.EVALUATION_CSV_FIELDNAMES,
                                                        delimiter=configs.CSV_DELIMITER)
        self.testing_local_meta_reader = csv.DictReader(self.testing_local_meta_file,
                                                        fieldnames=configs.LOCAL_METADATA_CSV_FIELDNAMES,
                                                        delimiter=configs.CSV_DELIMITER)

    def __get_global_metadata(self, global_meta_reader, is_training_data):
        if not global_meta_reader.fieldnames:
            data_type = 'Testing'
            if is_training_data:
                data_type = 'Training'

            mg.ERROR(data_type + " global metadata file is empty.")
        else:
            metadata = next(global_meta_reader)
            global_meta = GlobalMetadata(metadata=metadata, is_training_data=is_training_data)
            return global_meta

    def __update_main_global_variables(self):
        testing_global_metadata = self.get_testing_global_metadata()
        if self.is_training:
            training_global_metadata = self.get_training_global_metadata()
            default_msg = 'Training and evaluation dataset have different '
            if training_global_metadata.frequency_samples != testing_global_metadata.frequency_samples:
                mg.WARN(default_msg + 'frequency samples.')
            if training_global_metadata.frame_samples != testing_global_metadata.frame_samples:
                mg.WARN(default_msg + 'frame samples.')
            if training_global_metadata.data_type != testing_global_metadata.data_type:
                mg.WARN(default_msg + 'data types.')
            if training_global_metadata.overlap_ratio != testing_global_metadata.overlap_ratio:
                mg.WARN(default_msg + 'overlap ratios.')

        mg.NUMBER_OF_SAMPLES_PER_FRAME = testing_global_metadata.frame_samples / (2 if testing_global_metadata.data_type.lower() == 'fft' else 1)

    def __enter__(self):
        self.__is_prepared = False
        self.__set_filepaths()

        try:
            if self.is_training:
                self.training_global_meta_file = open(self.training_global_meta_filepath, mode='r')
                self.training_file = open(self.training_filepath, mode='r')
                self.training_evaluation_file = open(self.training_evaluation_filepath, mode='r')
                self.training_local_meta_file = open(self.training_local_meta_filepath, mode='r')

            self.testing_global_meta_file = open(self.testing_global_meta_filepath, mode='r')
            self.testing_file = open(self.testing_filepath, mode='r')
            self.testing_evaluation_file = open(self.testing_evaluation_filepath, mode='r')
            self.testing_local_meta_file = open(self.testing_local_meta_filepath, mode='r')

        except IOError as e:
            self.__exit__(None, None, None)  # Close files before exit
            mg.ERROR("While opening CSV files. I/O error\n{0}".format(e))
        except Exception as e:
            self.__exit__(None, None, None)  # Close files before exit
            mg.ERROR("While opening CSV files. Exception description:\n{0}".format(e))
        else:
            self.__init_CSV_readers()
            self.__update_main_global_variables()
            self.__is_prepared = True

        return self

    def __exit__(self, exc_t, exc_v, trace):
        if self.training_file is not None and self.training_file.closed is False:
            self.training_file.close()
            self.training_reader = None

        if self.testing_file is not None and self.testing_file.closed is False:
            self.testing_file.close()
            self.testing_reader = None

        if self.training_evaluation_file is not None and self.training_evaluation_file.closed is False:
            self.training_evaluation_file.close()
            self.training_evaluation_reader = None

        if self.testing_evaluation_file is not None and self.testing_evaluation_file.closed is False:
            self.testing_evaluation_file.close()
            self.testing_evaluation_reader = None

        if self.training_global_meta_file is not None and self.training_global_meta_file.closed is False:
            self.training_global_meta_file.close()
            self.training_global_meta_reader = None

        if self.testing_global_meta_file is not None and self.testing_global_meta_file.closed is False:
            self.testing_global_meta_file.close()
            self.testing_global_meta_reader = None

        if self.training_local_meta_file is not None and self.training_local_meta_file.closed is False:
            self.training_local_meta_file.close()
            self.training_local_meta_reader = None

        if self.testing_local_meta_file is not None and self.testing_local_meta_file.closed is False:
            self.testing_local_meta_file.close()
            self.testing_local_meta_reader = None

        self.__is_prepared = False
