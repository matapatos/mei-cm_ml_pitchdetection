'''
This file contains the enumeration of a type of a frame
'''

from enum import Enum


MINIMUM_SUM_TOTAL_TO_CONSIDER_NOTE = 2


class ml_FrameType(Enum):
    SILENCE = [0, 0, 0]
    OTHER_NOTES = [1, 0, 0]
    NOTE_WITH_NOISE = [1, 1, 0]
    PURE_NOTE = [1, 1, 1]
