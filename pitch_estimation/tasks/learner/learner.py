'''
This file contains all the 'entrance' for the training of all filters
'''
import os
import collections


import main_globals as mg
import utils
import pitch_estimation.tasks.learner.filters.types.all as filter_types
from pitch_estimation.tasks.learner.filters.filter import Filter
import pitch_estimation.tasks.learner.filters.configs as filter_configs
from pitch_estimation.auxiliary.metrics.controllers import GlobalMetrics


class Learner(GlobalMetrics):

    def __init__(self,
                 is_training,
                 summary_output_folder,
                 filter_type='all',
                 notes_number='all',
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):

        self.summary_output_folder = os.path.join(summary_output_folder, 'learner')
        super(Learner, self).__init__(summary_output_folder=self.summary_output_folder, best_model_folderpath=best_model_folderpath)

        self.is_training = is_training
        self.filters = []
        self.filter_types_to_use = []

        self.filter_types = filter_type
        self.notes_number = notes_number
        self.models_folder = models_folder
        self.model_iteration = model_iteration
        self.best_model = best_model
        self.log_configs_filepath = log_configs_filepath

    def train(self, input_data, output_data, local_meta, is_last_music, debug=False):
        results = collections.defaultdict(list)
        for i, f in enumerate(self.filters):
            note_result = f.train(input_data=input_data,
                                  output_data=output_data,
                                  local_meta=local_meta,
                                  is_last_music=is_last_music,
                                  debug=debug)

            is_last_music_and_filter = (is_last_music and i + 1 == len(self.filters))
            self.metrics.update_metrics(run_mode=mg.RunMode.TRAINING,
                                        note_result=note_result,
                                        is_last_music=is_last_music_and_filter)
            results[f.get_class_name(f)].append(note_result)
        return results

    def evaluate(self, input_data, output_data, local_meta, is_last_music, debug=False):
        results = collections.defaultdict(list)
        for i, f in enumerate(self.filters):
            note_result = f.evaluate(input_data=input_data,
                                     output_data=output_data,
                                     local_meta=local_meta,
                                     is_last_music=is_last_music,
                                     debug=debug)

            is_last_music_and_filter = (is_last_music and i + 1 == len(self.filters))
            self.metrics.update_metrics(run_mode=mg.RunMode.EVALUATING,
                                        note_result=note_result,
                                        is_last_music=is_last_music_and_filter)
            results[f.get_class_name(f)].append(note_result)
        return results

    def predict(self, input_data, output_data, local_meta, is_last_music, debug=False):
        results = collections.defaultdict(list)
        for i, f in enumerate(self.filters):
            note_result = f.predict(input_data=input_data,
                                    output_data=output_data,
                                    local_meta=local_meta,
                                    is_last_music=is_last_music,
                                    update_metrics=True,
                                    debug=debug)

            is_last_music_and_filter = (is_last_music and i + 1 == len(self.filters))
            self.metrics.update_metrics(run_mode=mg.RunMode.PREDICTING,
                                        note_result=note_result,
                                        is_last_music=is_last_music_and_filter)
            results[f.get_class_name(f)].append(note_result)
        return results

    def increase_global_step(self):
        for f in self.filters:
            f.increase_global_step()

        self.metrics.increase_global_step()

    def save_models(self, output_folderpath, iteration_num, debug=False):
        mg.INFO("Saving models...")
        for f in self.filters:
            f.save_model(output_folderpath=output_folderpath,
                         iteration_num=iteration_num,
                         debug=debug)

    def __set_configurations(self, filter_types, notes_number):
        mg.INFO("--------- Setting Configurations ----------")
        self.__select_filters(filter_types=filter_types)
        self.__select_and_prepare_notes_filters_by_notes_type(notes_number=notes_number)
        mg.INFO("-------------------------------------------")

    def __select_filters(self, filter_types):
        if type(filter_types) is not list:
            filter_types = [filter_types]

        if len(filter_types) == 0:
            mg.ERROR('You need to choose at least one filter type of Learner.FilterType')

        else:
            for f in filter_types:
                if f == 'all':
                    self.__select_all_filters()
                    break  # We break because it will append all types of filters
                else:
                    self.__append_filter(f)

            message = 'Filters types in use:'
            for t in self.filter_types_to_use:
                message += '\n\t' + str(t)
            mg.INFO(message)

    def __select_all_filters(self):
        self.filter_types_to_use = filter_types.all_filters

    def __append_filter(self, filter_name_type):
        for f in filter_types.all_filters:
            if Filter.get_class_name(f) == filter_name_type:
                self.filter_types_to_use.append(f)

    def __select_and_prepare_notes_filters_by_notes_type(self, notes_number):
        if type(notes_number) is not list:
            notes_number = [notes_number]

        if 'all' in notes_number:
            self.__select_and_prepare_all_notes_filters(is_training=self.is_training)
        elif 'silence' in notes_number:
            self.__select_silence_as_note_type_to_filters()
        else:
            notes = []
            for n in notes_number:
                num = utils.get_int(num=n)
                if num is None:
                    mg.ERROR('Unsupported Note Type \'%s\'' % n)
                else:
                    notes.append(num)

            self.__select_and_prepare_notes_filters(notes=notes)

    def __select_and_prepare_all_notes_filters(self, is_training):
        first_note = mg.FIRST_NOTE_NUMBER
        last_note = first_note + mg.NUMBER_OF_NOTES
        mg.INFO("Notes:\n\tAll notes between %d - %d" % (first_note, last_note - 1))
        for i in range(first_note, last_note):
            self.__set_note_to_filters(note_number=i)

    def __select_and_prepare_notes_filters(self, notes):
        first_note = mg.FIRST_NOTE_NUMBER
        last_note = first_note + mg.NUMBER_OF_NOTES
        mg.INFO("Notes:\n\t%s" % (notes))
        for i in notes:
            if i < first_note or i > last_note:
                mg.ERROR('Invalid note. Notes must be between %s and %s' % (first_note, last_note))
            else:
                self.__set_note_to_filters(note_number=i)

    def __set_note_to_filters(self, note_number):
        for filter_ in self.filter_types_to_use:
            f = filter_(note_number=note_number,
                        summary_output_folder=self.summary_output_folder,
                        is_training=self.is_training,
                        models_folder=self.models_folder,
                        model_iteration=self.model_iteration,
                        best_model=self.best_model,
                        log_configs_filepath=self.log_configs_filepath,
                        best_model_folderpath=self.best_model_folderpath)
            self.filters.append(f)

    def __select_silence_as_note_type_to_filters(self):
        self.__set_note_to_filters(note_number=filter_configs.SILENCE_NOTE_NUMBER)

    def __enter__(self):
        self.__set_configurations(filter_types=self.filter_types,
                                  notes_number=self.notes_number)
        return self

    def __exit__(self, exc_t, exc_v, trace):
        mg.INFO("Closing filters session...")
        self.metrics.close()
        for f in self.filters:
            f.close()
