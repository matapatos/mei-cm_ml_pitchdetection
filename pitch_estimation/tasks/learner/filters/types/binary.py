from pitch_estimation.tasks.learner.filters.filter import Filter


class Binary(Filter):

    NO_NOTE = 0
    HAS_NOTE = 1

    def __init__(self,
                 note_number,
                 summary_output_folder,
                 is_training,
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):
        self.num_classes = 1  # Possible output: 0 or 1, 0 -> The note doesn't exists in the specified frame, 1 -> Otherwise

        super(Binary, self).__init__(note_number=note_number,
                                     summary_output_folder=summary_output_folder,
                                     is_training=is_training,
                                     models_folder=models_folder,
                                     model_iteration=model_iteration,
                                     best_model=best_model,
                                     log_configs_filepath=log_configs_filepath,
                                     best_model_folderpath=best_model_folderpath)

    def reshape_batchs(self, batch_x, batch_y):
        # Reshape properties to meet with the self.X and self.Y defined previously
        batch_x = batch_x.reshape(1, len(batch_x))
        batch_y = batch_y.reshape(1, 1)

        return batch_x, batch_y
