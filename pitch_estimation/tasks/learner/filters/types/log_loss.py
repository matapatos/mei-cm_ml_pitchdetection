import tensorflow as tf


from pitch_estimation.tasks.learner.filters.types.binary import Binary


class LogLoss(Binary):

    def __init__(self,
                 note_number,
                 summary_output_folder,
                 is_training,
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):

        super(LogLoss, self).__init__(note_number=note_number,
                                      summary_output_folder=summary_output_folder,
                                      is_training=is_training,
                                      models_folder=models_folder,
                                      model_iteration=model_iteration,
                                      best_model=best_model,
                                      log_configs_filepath=log_configs_filepath,
                                      best_model_folderpath=best_model_folderpath)

    def define_cost_function(self):
        '''
        Function is used to define the cost function used on the NN

        Usage requirements: Make sure to call the buildNNModel function previously.
        '''
        with tf.name_scope('Loss_operation'):
            self.loss_op = tf.losses.log_loss(labels=self.model.Y,
                                              predictions=self.model.predictions)
            self.summary_loss = tf.summary.scalar('loss_val', self.loss_op)
