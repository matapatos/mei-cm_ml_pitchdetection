import tensorflow as tf


from pitch_estimation.tasks.learner.filters.filter import Filter
import main_globals as mg
import pitch_estimation.tasks.learner.filters.configs as configs


class MultiClass(Filter):

    NO_NOTE = 0
    HAS_NOTE = 1

    def __init__(self,
                 summary_output_folder,
                 is_training,
                 note_number=None,  # This parameter is discarded
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):
        self.num_classes = mg.NUMBER_OF_NOTES  # Possible output: 0 or 1, 0 -> The note doesn't exists in the specified frame, 1 -> Otherwise

        super(MultiClass, self).__init__(note_number=configs.MULTI_CLASS_NOTE_NUMBER,
                                         summary_output_folder=summary_output_folder,
                                         is_training=is_training,
                                         models_folder=models_folder,
                                         model_iteration=model_iteration,
                                         best_model=best_model,
                                         log_configs_filepath=log_configs_filepath,
                                         best_model_folderpath=best_model_folderpath)

    def reshape_batchs(self, batch_x, batch_y):
        # Reshape properties to meet with the self.X and self.Y defined previously
        batch_x = batch_x.reshape(1, len(batch_x))
        batch_y = batch_y.reshape(1, len(batch_y))
        return batch_x, batch_y

    def define_cost_function(self):
        with tf.name_scope('Loss_operation'):
            cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(logits=self.model.logits, labels=self.model.Y)  # Cross entropy is an example of a cost function
            self.loss_op = tf.reduce_mean(cross_entropy)
            self.summary_loss = tf.summary.scalar('loss_val', self.loss_op)
