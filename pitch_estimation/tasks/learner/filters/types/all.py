from pitch_estimation.tasks.learner.filters.types.sigmoid import Sigmoid
from pitch_estimation.tasks.learner.filters.types.softmax import Softmax
from pitch_estimation.tasks.learner.filters.types.mean_squared import MeanSquared
from pitch_estimation.tasks.learner.filters.types.log_loss import LogLoss
from pitch_estimation.tasks.learner.filters.types.weighted_sigmoid import WeightedSigmoid
from pitch_estimation.tasks.learner.filters.types.multi_class import MultiClass

from pitch_estimation.tasks.learner.filters.filter import Filter

all_filters = [Sigmoid, Softmax, MeanSquared, LogLoss, WeightedSigmoid, MultiClass]
all_filters_names = [Filter.get_class_name(f) for f in all_filters]
all_filters_names.append('all')
