
import tensorflow as tf


from pitch_estimation.tasks.learner.filters.filter import Filter


class Softmax(Filter):

    NO_NOTE = [1.0, 0.0]
    HAS_NOTE = [0.0, 1.0]

    def __init__(self,
                 note_number,
                 summary_output_folder,
                 is_training,
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):
        self.num_classes = 2  # Possible output: [1.0 0.0] [0.0 1.0]. The first one means without note and the second with note

        super(Softmax, self).__init__(note_number=note_number,
                                      summary_output_folder=summary_output_folder,
                                      is_training=is_training,
                                      models_folder=models_folder,
                                      model_iteration=model_iteration,
                                      best_model=best_model,
                                      log_configs_filepath=log_configs_filepath,
                                      best_model_folderpath=best_model_folderpath)

    def define_cost_function(self):
        with tf.name_scope('Loss_operation'):
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.model.logits, labels=self.model.Y)  # Cross entropy is an example of a cost function
            self.loss_op = tf.reduce_mean(cross_entropy)
            self.summary_loss = tf.summary.scalar('loss_val', self.loss_op)

    def define_others_TF_operations(self):
        self.prediction_op = tf.argmax(self.model.logits, axis=1)
        self.prediction_op = tf.reshape(tensor=self.prediction_op, shape=[])

    def reshape_batchs(self, batch_x, batch_y):
        # Reshape properties to meet with the self.X and self.Y defined previously
        batch_x = batch_x.reshape(1, len(batch_x))
        batch_y = batch_y.reshape(1, len(batch_y))

        return batch_x, batch_y
