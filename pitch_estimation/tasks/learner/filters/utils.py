import os
import re


import main_globals as mg
import utils as mg_utils


def get_model_filepath_by_iteration(filter_instance, models_folder, model_iteration='last'):
    available_models = __get_existent_models_from_folder(filter_instance=filter_instance,
                                                         folder=models_folder,
                                                         model_iteration=model_iteration)
    model_filepath = __get_model_filepath_from_models_by_iteration(models=available_models,
                                                                   model_iteration=model_iteration)
    if model_filepath is None:
        mg.ERROR('Model not found.\n       Available models: %s' % (available_models))

    return model_filepath


def get_model_filepath_by_best_metric(filter_instance, models_folder, best_model):
    best_metric_folder = __get_best_metric_folder(models_folder=models_folder,
                                                  best_model=best_model)
    available_models = __get_existent_models_from_folder(filter_instance=filter_instance,
                                                         folder=best_metric_folder,
                                                         is_best_model=True)
    return available_models[0]  # We only have one model (the best one of this accuracy).


def get_models_folder(filter_instance, original_folder, parent=None):
    class_name = filter_instance.get_class_name(filter_instance)
    output_folder = os.path.join(original_folder, class_name)
    if not os.path.isdir(output_folder):
        new_output_folder = os.path.join(original_folder, str(filter_instance.note_number) + os.sep + 'output')
        if not os.path.isdir(new_output_folder):
            others_new_output_folder = os.path.join(original_folder, str(filter_instance.note_number) + os.sep + 'others' + os.sep + 'output')
            if not os.path.isdir(others_new_output_folder):
                parent_id = 'nn_per_note'
                if parent is not None:
                    parent_id = parent.identifier
                others_new_output_folder_2 = original_folder + os.sep + 'post_processing' + os.sep + parent_id + os.sep + str(filter_instance.note_number) + os.sep + class_name
                if not os.path.isdir(others_new_output_folder_2):
                    mg.ERROR('No models folder found with one of the following paths:\n\t-> \'' + str(output_folder) + '\'\n\t-> \'' + str(new_output_folder) + '\'\n\t-> \'' + str(others_new_output_folder) + '\'\'\n\t-> \'' + str(others_new_output_folder_2) + '\'')
                else:
                    return others_new_output_folder_2
            else:
                new_output_folder = others_new_output_folder

        output_folder = os.path.join(new_output_folder, filter_instance.get_class_name(filter_instance))
        if not os.path.isdir(output_folder):
            output_folder = mg_utils.get_last_folder_from_folder(main_folder=new_output_folder)
            output_folder = os.path.join(output_folder, class_name)
            if not os.path.isdir(output_folder):
                output_folder = os.path.dirname(output_folder)
                parent_id = 'nn_per_note'
                if parent is not None:
                    parent_id = parent.identifier
                if 'fix_' in parent_id and 'per_note' in parent_id:
                    class_name = parent_id + '_' + class_name + '_' + str(filter_instance.note_number)

                output_folder = os.path.join(output_folder, 'post_processing' + os.sep + str(parent_id) + os.sep + str(filter_instance.note_number) + os.sep + class_name)
                if not os.path.isdir(output_folder):
                    original_class_name = filter_instance.get_class_name(filter_instance)
                    if 'fix_onsets' in original_class_name:
                        class_name = parent_id + '_fix_onset_' + str(filter_instance.note_number)
                        output_folder = os.path.dirname(output_folder)
                        output_folder = os.path.join(output_folder, class_name)
    return output_folder


def __get_best_metric_folder(models_folder, best_model):
    best_folder = os.path.join(models_folder, 'best')
    if not os.path.isdir(best_folder):
        mg.ERROR('The following directory doesn\'t exist \'' + str(best_folder) + '\'')

    best_metric_folder = os.path.join(best_folder, best_model)
    if os.path.isdir(best_metric_folder):
        return best_metric_folder
    else:
        filenames = os.listdir(best_folder)
        for f in filenames:
            if f.lower() == best_model:
                return os.path.join(best_folder, f)

        mg.ERROR('The following directory doesn\'t exist \'' + str(best_metric_folder) + '\'')  # If it reaches here is because it didn\'t find any directory with that name or similar


def __get_model_filepath_from_models_by_iteration(models, model_iteration):
    last_iteration_model = None
    for m in models:
        if type(m) is list:  # Sub folder models
            if last_iteration_model:
                return last_iteration_model
            else:
                new_model = __get_model_filepath_from_models_by_iteration(models=m,
                                                                          model_iteration=model_iteration)
                if new_model is not None:
                    return new_model
        else:  # Main folder models
            if model_iteration is None:  # Old model usage
                return m
            elif model_iteration == 'last':  # Get the last iteration model. It favor the current folder model
                last_iteration_model = m
            else:
                return m

    if model_iteration == 'last':
        return last_iteration_model
    else:
        return None


def __get_existent_models_from_folder(filter_instance, folder, model_iteration=None, is_best_model=False):
    if not os.path.isdir(folder):
        mg.ERROR('The following models directory doesn\'t exist \'' + str(folder) + '\'')

    model_name = filter_instance.get_model_filename(model_iteration=model_iteration,
                                                    with_extension=not is_best_model)
    available_models = __get_available_tensorflow_models_from_folder(models_folder=folder,
                                                                     model_name=model_name,
                                                                     recursive=True)
    num_of_models = len(available_models)
    if num_of_models == 0:
        all_available_models = __get_available_tensorflow_models_from_folder(models_folder=folder,
                                                                             model_name=None,
                                                                             recursive=True)
        mg.ERROR('No models denominated by \'%r\' found in folder \'%r\'.\n\tAvailable models: %r' % (model_name, folder, all_available_models))
    return available_models


def __get_available_tensorflow_models_from_folder(models_folder, model_name=None, recursive=False):
    available_models = []
    for f in os.listdir(models_folder):
        if mg.TENSORFLOW_MODEL_EXTENSION in f:  # Check if is a TF model
            filepath = os.path.join(models_folder, f)
            filepath = filepath.split(mg.TENSORFLOW_MODEL_EXTENSION)[0]  # For restoring in TF we don\'t need the fullpath
            filepath += mg.TENSORFLOW_MODEL_EXTENSION
            if model_name is None:
                available_models.append(filepath)
            else:
                match = re.match(model_name, f, re.M | re.I)  # Match model name
                if match and filepath not in available_models:
                    available_models.append(filepath)
        elif recursive:
            full_filepath = os.path.join(models_folder, f)
            if os.path.isdir(full_filepath):
                dir_models = __get_available_tensorflow_models_from_folder(models_folder=full_filepath,
                                                                           model_name=model_name,
                                                                           recursive=recursive)
                if len(dir_models) != 0:
                    available_models.append(dir_models)

    return available_models
