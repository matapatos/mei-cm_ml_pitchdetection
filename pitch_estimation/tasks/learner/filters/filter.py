'''
This class is the parent class of each filter. Each filter is specific for only one note (ex: FilterDo1 -> responsable to identify only Do1 in musics).
'''

import tensorflow as tf
import numpy as np
import time
import os
from functools import reduce


import debug_tools
import main_globals as mg
import utils as mg_utils
import pitch_estimation.tasks.learner.filters.models.models as models
import pitch_estimation.tasks.learner.filters.configs as filter_configs
import pitch_estimation.tasks.learner.filters.utils as filter_utils
from pitch_estimation.auxiliary.data.results.note_results import NoteResults
from pitch_estimation.auxiliary.metrics.controllers import SingleNoteMetrics
from pitch_estimation.auxiliary.training.early_stopping import EarlyStopping


class Filter(SingleNoteMetrics):

    def __init__(self,
                 note_number,
                 summary_output_folder,
                 is_training,
                 models_folder=None,
                 model_iteration=None,
                 best_model=None,
                 log_configs_filepath=None,
                 best_model_folderpath=None):

        early_stopping = EarlyStopping(num_iterations_after_best=30, min_percentage=0.1, metrics='fmeasure', is_training=is_training)
        self.graph = tf.Graph()
        super(Filter, self).__init__(note_number=note_number,
                                     summary_output_folder=summary_output_folder,
                                     best_model_folderpath=best_model_folderpath,
                                     graph=self.graph,
                                     early_stopping=early_stopping)

        self.summary_output_folder = summary_output_folder
        self.note_index = mg_utils.get_note_index_from_number(note_number=self.note_number)
        self.is_training = None
        self.is_model_restored = False
        # Initialize NeuralNetwork
        self.__build_NN(log_configs_filepath=log_configs_filepath)
        # Restore NN if necessary
        self.__restore_model_if_needed(models_folder=models_folder,
                                       model_iteration=model_iteration,
                                       best_model=best_model,
                                       is_training=is_training)

    def define_NN_parameters_and_build_model(self):
        '''
        This function contains all the NeuralNetworks configurations, such as number of neurons, learning rate, weights, biases used, etc.
        '''

        self.learning_rate = 1e-6
        # dropout_prob = tf.train.exponential_decay(0.8,
        #                                           global_step=self.metrics.global_step,
        #                                           decay_steps=20,
        #                                           decay_rate=0.2)
        # min_dropout_prob = tf.constant(0.2, dtype=tf.float32)
        # dropout_prob = tf.cond(tf.less(dropout_exponential_decay, min_dropout_prob), lambda: min_dropout_prob, lambda: dropout_exponential_decay)
        self.model = models.FFTEficientMF_v2(num_input_neurons=mg.NUMBER_OF_SAMPLES_PER_FRAME,
                                             num_output_neurons=self.num_classes,
                                             dropout_prob=0.15,
                                             noise_prob=0.7,
                                             noise_std=0.05,
                                             dropconnect_prob=0,
                                             random_spike_prob=0,
                                             spike_strength=0,
                                             opposite_sign_prob=0,
                                             use_batch_normalization=False,
                                             use_alpha_dropout=False)

    def define_cost_function(self):
        '''
        Function is used to define the cost function used on the NN

        Usage requirements: Make sure to call the buildNNModel function previously.
        '''

        raise NotImplementedError

    def define_others_TF_operations(self):
        '''
        Usage requirements: Make sure to call the defineTFMetrics function previously.
        '''

        self.prediction_op = self.model.predictions

    def define_optimizer(self):
        '''
        Function used to define the Optimizer to be used in the NN.
        Note that an optimizer is the type of algorithm that would do the training operation (tf.train.GradientDescent, tf.train.AdamOptimizer, etc)

        Usage requirements: Make sure to call the defineOthersTFoperations function previously.
        '''

        with tf.name_scope('Training'):
            #  import pitch_estimation.auxiliary.others.learned_optimizer.trainable_adam as learned_optimizer
            #  self.optimizer = learned_optimizer.TrainableAdam(learning_rate=self.learning_rate)
            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):  # Necessary when using Batch Normalization
                gradients, variables = zip(*self.optimizer.compute_gradients(self.loss_op))
                gradients, _ = tf.clip_by_global_norm(gradients, 5.0)
                self.train_op = self.optimizer.apply_gradients(zip(gradients, variables))

    def init_TF_variables(self):
        '''
        Function used to initilize the TensorFlow variables

        Usage requirements: Make sure to call the defineOptimizer function previously.
        '''
        init = tf.global_variables_initializer()
        self.session.run(init)

    def define_tensorboard_and_model_writers(self):
        '''
        Usage requirements: Make sure to call the initTFvariables function previously.
        '''

        self.saver = tf.train.Saver(max_to_keep=mg.MAX_CHECKPOINTS_TO_KEEP)  # For saving model
        self.best_models_saver = tf.train.Saver(max_to_keep=4)  # For saving best models metrics

    def train(self, input_data, output_data, local_meta, is_last_music, debug=False):

        if debug:
            mg.DEBUG("--- %s ---" % (local_meta.filename))
            start_time = time.time()

        if self.is_model_restored:  # Only for prediction
            original_is_training_flag = self.is_training
            result = self.predict(input_data=input_data,
                                  output_data=output_data,
                                  local_meta=local_meta,
                                  is_last_music=is_last_music,
                                  update_metrics=False,
                                  debug=False)
            self.is_training = original_is_training_flag
        else:  # Training phase
            # Optimization technique to improve NN
            shuffled_input, shuffled_output, shuffled_indexes = self.__shuffle_music_data(input_data=input_data,
                                                                                          output_data=output_data)
            self.is_training = True
            shuffled_output = self.__get_output_data_transformed(shuffled_output)
            batches_y = shuffled_output.reshape((len(shuffled_output), 1))
            if mg.BATCH_SIZE == 1:
                predictions, losses = self.__sgd(batches_x=shuffled_input, batches_y=batches_y)
            else:
                predictions, losses = self.__mini_batch(batches_x=shuffled_input, batches_y=batches_y)

            result = self.__get_note_results(labels=shuffled_output,
                                             predictions=predictions,
                                             losses=losses,
                                             local_meta=local_meta,
                                             shuffled_indexes=shuffled_indexes)

        self.metrics.update_metrics(run_mode=mg.RunMode.TRAINING,
                                    note_result=result,
                                    is_last_music=is_last_music,
                                    save_best_model=False)

        if debug:
            debug_tools.calculate_and_print_operation_time(start_time=start_time)
            mg.DEBUG("------")

        return result

    def evaluate(self, input_data, output_data, local_meta, is_last_music, debug=False):
        if debug:
            mg.DEBUG("--- %s ---" % (local_meta.filename))
            start_time = time.time()

        original_is_training_flag = self.is_training
        result = self.predict(input_data=input_data,
                              output_data=output_data,
                              local_meta=local_meta,
                              is_last_music=is_last_music,
                              update_metrics=False,
                              debug=False)

        self.metrics.update_metrics(run_mode=mg.RunMode.EVALUATING,
                                    note_result=result,
                                    is_last_music=is_last_music,
                                    save_best_model=True)
        self.is_training = original_is_training_flag

        if debug:
            debug_tools.calculate_and_print_operation_time(start_time=start_time)
            mg.DEBUG("------")

        return result

    def predict(self, input_data, output_data, local_meta, is_last_music, update_metrics=True, debug=False):
        if debug:
            mg.DEBUG("--- %s ---" % (local_meta.filename))
            start_time = time.time()

        self.is_training = False
        predictions = []
        output_data = self.__get_output_data_transformed(output_data)
        losses = []

        batches_y = output_data.reshape(len(output_data), 1)
        operations = [self.prediction_op, self.loss_op]
        predictions, loss = self.__run_session_operations(operations=operations,
                                                          batch_x=input_data,
                                                          batch_y=batches_y)

        result = self.__get_note_results(labels=output_data,
                                         predictions=predictions,
                                         losses=losses,
                                         local_meta=local_meta)

        if update_metrics:
            self.metrics.update_metrics(run_mode=mg.RunMode.PREDICTING,
                                        note_result=result,
                                        is_last_music=is_last_music)
        if debug:
            debug_tools.calculate_and_print_operation_time(start_time=start_time)
            mg.DEBUG("------")

        return result

    def save_model(self, output_folderpath, iteration_num, debug=False):
        output_folderpath = self._mkdirs_full_output_folderpath_for_model(output_folderpath=output_folderpath,
                                                                          sub_folder='checkpoints')
        model_filepath = os.path.join(output_folderpath, self.get_model_filename(model_iteration=iteration_num))
        self._save_model(model_filepath=model_filepath)

    def close(self):
        self.metrics.close()
        self.session.close()

    def reshape_batchs(self, batch_x, batch_y):
        raise NotImplementedError

    def increase_global_step(self):
        self.metrics.increase_global_step()

    def convert_to_1D_array(self, original_array):
        if self.note_number == filter_configs.MULTI_CLASS_NOTE_NUMBER:
            new_shape = reduce(lambda x, y: x * y, original_array.shape)  # Multiply all elements of shape in order to give the final 1D array
            return np.reshape(original_array, newshape=new_shape)

        if original_array.shape == (len(original_array),):
            return original_array

        if original_array.shape == (len(original_array), 1, 1):
            return np.reshape(original_array, newshape=(len(original_array),))

        if original_array.shape == (len(original_array), 1):
            return np.reshape(original_array, newshape=(len(original_array),))

        new_array = []
        for data in original_array:
            new_array.append(data[1])

        return np.array(new_array)

    def run_session_operations_with_feed_dict(self, operations, feed_dict=None):
        return self.session.run(operations, feed_dict=feed_dict)

    def __build_NN(self, log_configs_filepath):
        with self.graph.as_default():
            self.define_NN_parameters_and_build_model()
            self.define_cost_function()
            self.define_others_TF_operations()
            self.define_optimizer()

            self.init_TF_variables()
            self.define_tensorboard_and_model_writers()
            self.__log_training_configurations(log_configs_filepath=log_configs_filepath)

    def __restore_model_if_needed(self, models_folder, model_iteration, best_model, is_training):
        is_to_restore_model = (models_folder is not None)
        if is_to_restore_model:
            self.__restore_model(models_folder=models_folder,
                                 model_iteration=model_iteration,
                                 best_model=best_model)
        return is_to_restore_model

    def __restore_model(self, models_folder, model_iteration, best_model):
        if models_folder is None:
            mg.ERROR("Models folder is NoneType")

        models_folder = filter_utils.get_models_folder(filter_instance=self,
                                                       original_folder=models_folder)
        if best_model is None:
            full_filepath = filter_utils.get_model_filepath_by_iteration(filter_instance=self,
                                                                         models_folder=models_folder,
                                                                         model_iteration=model_iteration)
        else:
            full_filepath = filter_utils.get_model_filepath_by_best_metric(filter_instance=self,
                                                                           models_folder=models_folder,
                                                                           best_model=best_model)

        mg.INFO('\t%s %d --> \'%s\'' % (SingleNoteMetrics.get_class_name(self), self.note_number, full_filepath))
        with self.graph.as_default():
            # self.saver = tf.train.import_meta_graph(full_filepath + ".meta")
            self.saver.restore(self.session, full_filepath)

        self.is_model_restored = True

    def __unshuffle_metrics(self, labels, predictions, shuffled_indexes):
        new_labels = np.zeros(len(labels))
        new_predictions = np.zeros(len(predictions))

        for label, pred, index in zip(labels, predictions, shuffled_indexes):
            new_labels[index] = label
            new_predictions[index] = pred

        return new_labels, new_predictions

    def __shuffle_music_data(self, input_data, output_data):
        max_size = len(input_data)
        array_of_indexes = np.arange(max_size)
        np.random.shuffle(array_of_indexes)

        shuffled_input_data = []
        shuffled_output_data = []
        for index in array_of_indexes:
            shuffled_input_data.append(input_data[index])
            shuffled_output_data.append(output_data[index])

        return shuffled_input_data, shuffled_output_data, array_of_indexes

    def __run_session_operations(self, operations, batch_x=None, batch_y=None):
        feed_dict = {self.model.X: batch_x, self.model.Y: batch_y, self.model.is_training: self.is_training}
        if batch_x is None or batch_y is None:
            feed_dict = None

        return self.run_session_operations_with_feed_dict(operations=operations, feed_dict=feed_dict)

    def __get_output_data_transformed(self, output_data):
        if self.note_number == filter_configs.MULTI_CLASS_NOTE_NUMBER:
            return output_data
        elif self.note_number == filter_configs.SILENCE_NOTE_NUMBER:
            return self.__transform_output_data_to_silence_output_data(output_data=output_data)
        else:
            return self.__transform_output_data(output_data=output_data)

    def __transform_output_data(self, output_data):
        real = []
        has_note = type(self).HAS_NOTE
        no_note = type(self).NO_NOTE
        for data in output_data:
            value = int(round(data[self.note_index]))
            if value == 1:
                real.append(has_note)
            else:
                real.append(no_note)

        return np.array(real)

    def __transform_output_data_to_silence_output_data(self, output_data):
        real = []
        has_silence = type(self).NO_NOTE
        no_silence = type(self).HAS_NOTE
        for data in output_data:
            total = sum(data)
            if total == 0:
                real.append(has_silence)
            else:
                real.append(no_silence)

        return np.array(real)

    def __inverse(self, a):
        new_a = []
        for val in a:
            if val == 0:
                new_a.append(1)
            else:
                new_a.append(0)
        return new_a

    def __log_training_configurations(self, log_configs_filepath):
        if log_configs_filepath is not None:
            configs = '\n\n----- ' + str(type(self).__name__) + ' ' + str(self.note_number) + ' -----\n'
            configs += 'note number: ' + str(self.note_number) + '\n'
            configs += 'optimizer: ' + str(type(self.optimizer).__name__) + '\n'
            configs += 'lr: ' + str(self.learning_rate) + '\n'
            configs += 'sequencial musics: ' + str(mg.SEQUENCIAL_MUSICS) + '\n'
            configs += 'model: ' + str(self.model) + '\n'
            configs += 'ds folder: "' + str(mg.DATASET_FOLDER) + '"\n'
            with open(log_configs_filepath, 'a') as f:
                f.writelines(configs)

    def __get_note_results(self, labels, predictions, losses, local_meta, shuffled_indexes=None):
        labels = np.array(labels)
        predictions = np.array(predictions)
        if predictions.shape == labels.shape and len(predictions.shape) > 1:
            labels = labels.reshape(predictions.shape[0], predictions.shape[1])

        labels = self.convert_to_1D_array(labels)
        predictions = self.convert_to_1D_array(predictions)

        if self.is_training:
            # Necessary for the Onset and OnsetOffset metrics
            labels_metrics, predictions_metrics = self.__unshuffle_metrics(labels=labels,
                                                                           predictions=predictions,
                                                                           shuffled_indexes=shuffled_indexes)

        # Invert metrics to be 1 -> is silence; 0 -> has note
        if self.note_number == filter_configs.SILENCE_NOTE_NUMBER:
            labels = self.__inverse(labels)
            predictions = self.__inverse(predictions)

        return NoteResults(creator_identifier='classifier_' + str(self.note_number) + '_' + SingleNoteMetrics.get_class_name(self),
                           filename=local_meta.filename,
                           note_number=self.note_number,
                           filter_type=SingleNoteMetrics.get_class_name(self),
                           labels=labels,
                           predictions=predictions,
                           losses=losses)

    def __sgd(self, batches_x, batches_y):
        operations = [self.train_op, self.prediction_op, self.loss_op]
        predictions = []
        losses = []
        for i in range(len(batches_y)):
            # Get batches
            batch_x = batches_x[i]
            batch_y = batches_y[i]

            batch_x = np.array(batch_x)
            batch_x = batch_x.reshape((1, len(batch_x)))
            batch_y = np.array(batch_y)
            batch_y = batch_y.reshape((1, len(batch_y)))

            _, pred, loss = self.__run_session_operations(operations=operations,
                                                          batch_x=batch_x,
                                                          batch_y=batch_y)
            predictions.append(pred[0][0])
            losses.append(loss)

        return predictions, losses

    def __mini_batch(self, batches_x, batches_y):
        operations = [self.train_op, self.prediction_op, self.loss_op]
        predictions = []
        losses = []
        for i in range(0, len(batches_y), mg.BATCH_SIZE):
            maximum_index = min(i + mg.BATCH_SIZE, len(batches_y))
            # Get batches
            batch_x = batches_x[i:maximum_index]
            batch_y = batches_y[i:maximum_index]

            _, preds, loss = self.__run_session_operations(operations=operations,
                                                           batch_x=batch_x,
                                                           batch_y=batch_y)
            predictions.extend(preds)
            losses.append(loss)

        return predictions, losses
