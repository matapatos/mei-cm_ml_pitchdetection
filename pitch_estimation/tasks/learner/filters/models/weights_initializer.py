import tensorflow as tf


class WeightsInitializer():
    '''
    WeightsInitializer use the following link details:
    http://docs.w3cub.com/tensorflow~python/tf/contrib/layers/variance_scaling_initializer/
    '''
    def initialize(self, act_func, use_alpha_dropout_initialization, shape, seed, name, dtype=tf.float32):
        if use_alpha_dropout_initialization:
            return self.__get_xavier_initialization_variable(shape=shape, seed=seed, name=name, dtype=dtype)

        elif act_func in [tf.nn.relu, tf.nn.relu6, tf.nn.elu, tf.nn.leaky_relu, tf.nn.swish, tf.nn.selu]:
            return self.__get_relu_initialization_variable(shape=shape, seed=seed, name=name, dtype=dtype)

        elif act_func is tf.nn.softmax:
            return self.__get_softmax_initialization_variable(shape=shape, seed=seed, name=name, dtype=dtype)

        else:
            # init = tf.random_uniform_initializer(minval=0, maxval=0.8, seed=seed, dtype=dtype)
            # return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=init)
            initializer = tf.truncated_normal_initializer(stddev=0.1, seed=seed, dtype=dtype)
            return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer)

    def initialize_bias(self, act_func, use_alpha_dropout_initialization, shape, seed, name, dtype=tf.float32):
        # Ones initializer
        # initializer = tf.ones_initializer(dtype=tf.float32)
        # return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer)
        return self.initialize(act_func=act_func,
                               use_alpha_dropout_initialization=use_alpha_dropout_initialization,
                               shape=shape,
                               seed=seed,
                               name=name,
                               dtype=dtype)

    def __get_relu_initialization_variable(self, shape, seed, name, dtype=tf.float32):
        '''
        This initialization is based on the following article: https://arxiv.org/pdf/1502.01852v1.pdf
        '''
        initializer = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_IN', uniform=False, seed=seed, dtype=dtype)
        return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer)

    def __get_softmax_initialization_variable(self, shape, seed, name, dtype=tf.float32):
        '''
        This initialization is based on the following article: http://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf
        '''
        initializer = tf.contrib.layers.variance_scaling_initializer(factor=1.0, mode='FAN_AVG', uniform=True, seed=seed, dtype=tf.float32)
        return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer)

    def __get_xavier_initialization_variable(self, shape, seed, name, dtype=tf.float32):
        '''
        This initialization is based on the following article: http://www.jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf
        '''
        initializer = tf.contrib.layers.xavier_initializer(uniform=True,
                                                           seed=seed,
                                                           dtype=dtype)
        return tf.get_variable(name=name, shape=shape, dtype=dtype, initializer=initializer)
