'''
This file contains all the existent NN model's architectures that the network can use

    TIPS:
    If the neurons starts to die, change Relu to Leaky Relu or Max Out
    See the following link for more info: https://youtu.be/-7scQpJT7uo?t=7m33s
'''

import tensorflow as tf
import collections


import pitch_estimation.tasks.learner.filters.configs as filter_configs
from pitch_estimation.tasks.learner.filters.models.weights_initializer import WeightsInitializer
import main_globals as mg


Layer = collections.namedtuple('Layer', ['num_neurons'])


class Model():
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 hidden_layers_act_func,
                 output_act_func=None,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):
        '''
        If the output_act_func is None it will choose between sigmoid or softmax, depending on the number of output neurons
        '''
        if num_input_neurons <= 0 or num_output_neurons <= 0:
            mg.ERROR("Number of input neurons and number or output neurons in a Neural Network must have a value bigger or equal to 1")

        if output_act_func is None:
            output_act_func = tf.nn.softmax
            if num_output_neurons == 1:
                output_act_func = tf.nn.sigmoid

        if use_alpha_dropout:
            if not isinstance(dropout_prob, tf.Tensor) and (not dropout_prob or dropout_prob <= 0):
                use_alpha_dropout = False
            else:
                if hidden_layers_act_func is not tf.nn.selu:
                    mg.WARN('Using tf.nn.selu as activation function for the hidden layers, instead of tf.nn.' + str(hidden_layers_act_func.__name__))
                    hidden_layers_act_func = tf.nn.selu  # Force usage of selu activation function when using alpha dropout

        self.num_input_neurons = num_input_neurons
        self.num_output_neurons = num_output_neurons
        self.weigths_initializer = WeightsInitializer()
        self.use_batch_normalization = use_batch_normalization
        self.dropout_prob = dropout_prob
        self.use_alpha_dropout = use_alpha_dropout
        self.noise_prob = noise_prob
        self.noise_std = noise_std
        self.dropconnect_prob = dropconnect_prob
        self.random_spike_prob = random_spike_prob
        self.spike_strength = spike_strength
        self.opposite_sign_prob = opposite_sign_prob
        self.hidden_layers_act_func = hidden_layers_act_func
        self.output_act_func = output_act_func
        self.hidden_layers = []

        self.X = None
        self.Y = None
        self.is_training = None
        self.weights = None
        self.biases = None
        self.scale = None
        self.beta = None
        self.logits = None
        self.predictions = None
        self.epsilon = tf.constant(value=0.001)

        self.__define_placeholders()
        self.define_hidden_layers()
        self.__define_weights_and_biases()
        self.build_model()

    def define_hidden_layers(self):
        raise NotImplementedError

    def __define_weights_and_biases(self):
        self.define_hidden_layers()
        if not self.hidden_layers:
            mg.ERROR('No hidden layers defined on model \'' + type(self).__name__ + '\'')
        else:
            self.__define_weigths()
            self.__define_bias()

    def __define_weigths(self):
        self.weights = {}
        initial_name = 'w_'
        prev_num_neurons = self.num_input_neurons
        for i, l in enumerate(self.hidden_layers):
            i += 1
            hidden_layer_name = initial_name + str(i)
            self.weights[hidden_layer_name] = self.weigths_initializer.initialize(act_func=self.hidden_layers_act_func,
                                                                                  use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                                                  shape=[prev_num_neurons, l.num_neurons],
                                                                                  seed=filter_configs.RANDOM_SEEDS_WEIGHTS[hidden_layer_name],
                                                                                  name=hidden_layer_name)
            prev_num_neurons = l.num_neurons

        output_layer_name = initial_name + 'out'
        self.weights[output_layer_name] = self.weigths_initializer.initialize(act_func=self.output_act_func,
                                                                              use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                                              shape=[prev_num_neurons, self.num_output_neurons],
                                                                              seed=filter_configs.RANDOM_SEEDS_WEIGHTS[output_layer_name],
                                                                              name=output_layer_name)

    def __define_bias(self):
        self.biases = {}
        initial_name = 'b_'
        for i, l in enumerate(self.hidden_layers):
            i += 1
            hidden_layer_name = initial_name + str(i)
            self.biases[hidden_layer_name] = self.weigths_initializer.initialize(act_func=self.hidden_layers_act_func,
                                                                                 use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                                                 shape=[l.num_neurons],
                                                                                 seed=filter_configs.RANDOM_SEEDS_BIASES[hidden_layer_name],
                                                                                 name=hidden_layer_name)

        output_layer_name = initial_name + 'out'
        self.biases[output_layer_name] = self.weigths_initializer.initialize(act_func=self.output_act_func,
                                                                             use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                                             shape=[self.num_output_neurons],
                                                                             seed=filter_configs.RANDOM_SEEDS_BIASES[output_layer_name],
                                                                             name=output_layer_name)

    def __define_placeholders(self):
        '''
        Define the input variables
            self.X -> Will contain the input data to be trained or predicted
            self.Y -> Will contain the evaluation data, like 0 or 1 (doesn't contain the note or yes it contains)
        '''
        self.X = tf.placeholder("float", [None, self.num_input_neurons], name='X')
        # tf.summary.histogram('X', self.X)
        # Left it empty by purpose
        self.Y = tf.placeholder("float", [None, self.num_output_neurons], name='Y')
        self.is_training = tf.placeholder(tf.bool, [], name="is_training")

    def build_model(self):
        if self.use_batch_normalization is True:
            self.__build_normalized_model()
        else:
            self.__build_non_normalized_model()

    def __build_normalized_model(self):
        # Define hidden layers
        num_layers = len(self.weights)
        layer = self.X
        for i in range(1, num_layers):
            with tf.name_scope('hidden_layer_' + str(i)):
                w = self.weights['w_' + str(i)]
                _, layer = self.__get_normalized_layer(input_data=layer,
                                                       w=w,
                                                       epsilon=self.epsilon,
                                                       b=None,
                                                       index=i,
                                                       activation_function=self.hidden_layers_act_func,
                                                       dropout_prob=self.dropout_prob,
                                                       dropout_var_name="drop_" + str(i),
                                                       use_alpha_dropout=self.use_alpha_dropout,
                                                       noise_prob=self.noise_prob,
                                                       noise_std=self.noise_std,
                                                       noise_var_name="noise_" + str(i),
                                                       dropconnect_prob=self.dropconnect_prob,
                                                       random_spike_prob=self.random_spike_prob,
                                                       spike_strength=self.spike_strength,
                                                       opposite_sign_prob=self.opposite_sign_prob)

        # Define output layer
        with tf.name_scope('output_layer'):
            w = self.weights['w_out']
            b = self.biases['b_out']
            self.logits, self.predictions = self.__get_normalized_layer(input_data=layer,
                                                                        w=w,
                                                                        b=b,
                                                                        epsilon=self.epsilon,
                                                                        index=i + 1,
                                                                        activation_function=self.output_act_func)  # Last layer must not have dropout

    # Based from: https://r2rt.com/implementing-batch-normalization-in-tensorflow.html
    def __get_normalized_layer(self,
                               input_data,
                               w,
                               b,
                               epsilon,
                               index,
                               activation_function,
                               dropout_prob=None,
                               dropout_var_name=None,
                               use_alpha_dropout=False,
                               noise_prob=None,
                               noise_std=None,
                               noise_var_name=None,
                               dropconnect_prob=None,
                               random_spike_prob=None,
                               spike_strength=None,
                               opposite_sign_prob=None):
        w = self.__get_dropconnect_layer(w, dropconnect_prob)
        # Calculate batch mean and variance
        layer = tf.matmul(input_data, w)
        tf.summary.histogram("weigths", w)

        if b is None:
            layer = tf.layers.batch_normalization(inputs=layer,
                                                  training=self.is_training)
        else:
            layer = tf.add(layer, b)

        if activation_function is not None:
            layer_with_act = activation_function(layer)
            tf.summary.histogram("activations", layer)

        layer, layer_with_act = self.__get_opposite_sign_layer(layer=layer,
                                                               layer_with_act=layer_with_act,
                                                               activation_function=activation_function,
                                                               prob=opposite_sign_prob)

        layer, layer_with_act = self.__get_random_spike_layer(layer=layer,
                                                              activation_function=activation_function,
                                                              prob=random_spike_prob,
                                                              strength=spike_strength)

        layer, layer_with_act = self.__get_noise_layer(noise_prob=noise_prob,
                                                       std=noise_std,
                                                       layer=layer,
                                                       layer_with_act=layer_with_act,
                                                       activation_function=activation_function,
                                                       name=noise_var_name)

        layer, layer_with_act = self.__get_dropout_layer(layer=layer,
                                                         layer_with_act=layer_with_act,
                                                         dropout_prob=dropout_prob,
                                                         activation_function=activation_function,
                                                         name=dropout_var_name,
                                                         use_alpha_dropout=use_alpha_dropout)

        return layer, layer_with_act

    def __build_non_normalized_model(self):
        # Define hidden layers
        num_layers = min(len(self.weights), len(self.biases))
        layer = self.X
        for i in range(1, num_layers):
            with tf.name_scope('hidden_layer_' + str(i)):
                w = self.weights['w_' + str(i)]
                tf.summary.histogram(name='w_' + str(i), values=w)
                b = self.biases['b_' + str(i)]
                tf.summary.histogram(name='b_' + str(i), values=b)
                _, layer = self._get_non_normalized_layer(input_data=layer,
                                                          w=w,
                                                          b=b,
                                                          activation_function=self.hidden_layers_act_func,
                                                          dropout_prob=self.dropout_prob,
                                                          dropout_var_name="drop_" + str(i),
                                                          use_alpha_dropout=self.use_alpha_dropout,
                                                          noise_prob=self.noise_prob,
                                                          noise_std=self.noise_std,
                                                          noise_var_name="noise_" + str(i),
                                                          dropconnect_prob=self.dropconnect_prob,
                                                          random_spike_prob=self.random_spike_prob,
                                                          spike_strength=self.spike_strength,
                                                          opposite_sign_prob=self.opposite_sign_prob)

        # Define output layer
        with tf.name_scope('output_layer'):
            w = self.weights['w_out']
            b = self.biases['b_out']
            tf.summary.histogram(name='w_out', values=w)
            tf.summary.histogram(name='b_out', values=b)
            self.logits, self.predictions = self._get_non_normalized_layer(input_data=layer,
                                                                           w=w,
                                                                           b=b,
                                                                           activation_function=self.output_act_func)  # Last layer must not have dropout or added noise

    def _get_non_normalized_layer(self,
                                  input_data,
                                  w,
                                  b,
                                  activation_function,
                                  dropout_prob=None,
                                  dropout_var_name=None,
                                  use_alpha_dropout=False,
                                  noise_prob=None,
                                  noise_std=None,
                                  noise_var_name=None,
                                  dropconnect_prob=None,
                                  random_spike_prob=None,
                                  spike_strength=None,
                                  opposite_sign_prob=None):

        w = self.__get_dropconnect_layer(w, dropconnect_prob)
        layer = tf.add(tf.matmul(input_data, w), b)
        # Tensorboard summaries
        tf.summary.histogram("weigths", w)
        tf.summary.histogram("biases", b)

        if activation_function is not None:
            layer_with_act = activation_function(layer)
            tf.summary.histogram("activations", layer)

        layer, layer_with_act = self.__get_opposite_sign_layer(layer=layer,
                                                               layer_with_act=layer_with_act,
                                                               activation_function=activation_function,
                                                               prob=opposite_sign_prob)

        layer, layer_with_act = self.__get_random_spike_layer(layer=layer,
                                                              layer_with_act=layer_with_act,
                                                              activation_function=activation_function,
                                                              prob=random_spike_prob,
                                                              strength=spike_strength)

        layer, layer_with_act = self.__get_noise_layer(noise_prob=noise_prob,
                                                       std=noise_std,
                                                       layer=layer,
                                                       layer_with_act=layer_with_act,
                                                       activation_function=activation_function,
                                                       name=noise_var_name)

        layer, layer_with_act = self.__get_dropout_layer(layer=layer,
                                                         layer_with_act=layer_with_act,
                                                         dropout_prob=dropout_prob,
                                                         activation_function=activation_function,
                                                         name=dropout_var_name,
                                                         use_alpha_dropout=use_alpha_dropout)

        return layer, layer_with_act

    def __get_noise_layer(self, noise_prob, std, layer, layer_with_act, activation_function, name=None):
        if (not isinstance(noise_prob, tf.Tensor) and (not noise_prob or noise_prob <= 0)) or (not isinstance(std, tf.Tensor) and (not std or std <= 0)):
            return layer, layer_with_act

        if activation_function is None:
            layer = self.__gaussian_noise_layer(input_layer=layer,
                                                std=std,
                                                noise_prob=noise_prob,
                                                name=name)
        else:
            layer_with_act = self.__gaussian_noise_layer(input_layer=layer_with_act,
                                                         std=std,
                                                         noise_prob=noise_prob,
                                                         name=name)
        return layer, layer_with_act

    def __gaussian_noise_layer(self, input_layer, std, noise_prob, prob_seed=filter_configs.NOISE_PROB_SEED, noise_seed=filter_configs.NOISE_SEED, name=None):

        def add_noise():
            # Noise prob condition
            random_uniform = tf.random_uniform(shape=[],
                                               minval=0,
                                               maxval=1,
                                               seed=prob_seed,
                                               name="rand_noise_cond")
            # 1 if it's to add noise or 0 otherwise
            noise_binary_cond = tf.floor(random_uniform + noise_prob, name="rand_noise_binary")
            # Actual noise itself
            noise = tf.random_normal(shape=tf.shape(input_layer), mean=0.0, stddev=std, dtype=tf.float32, name=name, seed=noise_seed)
            return tf.add(input_layer, tf.multiply(noise, noise_binary_cond))

        def no_noise():
            return input_layer

        return tf.cond(self.is_training, lambda: add_noise(), lambda: no_noise())

    def __get_dropout_layer(self, dropout_prob, layer, layer_with_act, activation_function, name, use_alpha_dropout=False):
        if not isinstance(dropout_prob, tf.Tensor) and (not dropout_prob or dropout_prob <= 0):
            return layer, layer_with_act

        if use_alpha_dropout:
            return self.__get_alpha_dropout_layer(dropout_prob=dropout_prob,
                                                  layer=layer,
                                                  layer_with_act=layer_with_act,
                                                  activation_function=activation_function,
                                                  name=name)
        else:
            return self.__get_normal_dropout_layer(dropout_prob=dropout_prob,
                                                   layer=layer,
                                                   layer_with_act=layer_with_act,
                                                   activation_function=activation_function,
                                                   name=name)

    def __get_normal_dropout_layer(self, dropout_prob, layer, layer_with_act, activation_function, name):
        if activation_function is None:
            layer = tf.layers.dropout(inputs=layer,
                                      rate=dropout_prob,
                                      training=self.is_training,
                                      seed=filter_configs.DROPOUT_SEED,
                                      name=name)
        else:
            layer_with_act = tf.layers.dropout(inputs=layer_with_act,
                                               rate=dropout_prob,
                                               training=self.is_training,
                                               seed=filter_configs.DROPOUT_SEED,
                                               name=name)
        return layer, layer_with_act

    def __get_alpha_dropout_layer(self, dropout_prob, layer, layer_with_act, activation_function, name):
        def alpha_dropout(inputs, rate, name):
            return tf.contrib.nn.alpha_dropout(inputs,
                                               1 - rate,
                                               noise_shape=None,
                                               seed=filter_configs.DROPOUT_SEED,
                                               name=name)

        def no_dropout(inputs):
            return inputs

        if activation_function is None:
            layer = tf.cond(self.is_training,
                            lambda: alpha_dropout(inputs=layer, rate=dropout_prob, name=name),
                            lambda: no_dropout(inputs=layer))
        else:
            layer_with_act = tf.cond(self.is_training,
                                     lambda: alpha_dropout(inputs=layer_with_act, rate=dropout_prob, name=name),
                                     lambda: no_dropout(inputs=layer_with_act))

        return layer, layer_with_act

    def __get_dropconnect_layer(self, W, prob):
        if not isinstance(prob, tf.Tensor) and (not prob or prob <= 0):
            return W

        def dropconnect(W, p):
            keep_prob = 1 - p
            return tf.nn.dropout(W, keep_prob=keep_prob, seed=filter_configs.DROPCONNECT_SEED) * keep_prob

        def no_dropconnect(W):
            return W

        return tf.cond(self.is_training, lambda: dropconnect(W=W, p=prob), lambda: no_dropconnect(W=W))

    def __get_random_spike_layer(self, layer, layer_with_act, activation_function, prob, strength):
        if (not isinstance(prob, tf.Tensor) and (not prob or prob <= 0)) or (not isinstance(strength, tf.Tensor) and (not strength or ((isinstance(strength, int) or isinstance(strength, float)) and strength <= 0) or (isinstance(strength, str) and not strength == 'random'))):
            return layer, layer_with_act

        if activation_function is None:
            layer = self.__random_spike_layer(layer=layer,
                                              prob=prob,
                                              strength=strength)
        else:
            layer_with_act = self.__random_spike_layer(layer=layer_with_act,
                                                       prob=prob,
                                                       strength=strength)

        return layer, layer_with_act

    def __random_spike_layer(self, layer, prob, strength):

        def random_spike(layer, prob, strength):
            if strength == 'random':
                layer = self.__dynamic_strength_random_spike(layer=layer, prob=prob)
            else:
                layer = self.__static_random_spike(layer=layer, prob=prob, strength=strength)

            return layer

        def no_spike(layer):
            return layer

        return tf.cond(self.is_training,
                       lambda: random_spike(layer=layer, prob=prob, strength=strength),
                       lambda: no_spike(layer=layer))

    def __static_random_spike(self, layer, prob, strength, seed=filter_configs.RANDOM_SPIKE_SEED):
        # Spike prob condition
        random_uniform = tf.random_uniform(shape=tf.shape(layer),
                                           minval=0,
                                           maxval=1,
                                           seed=seed,
                                           name="rand_spike_cond")
        spike_prob = tf.random_uniform(shape=tf.shape(layer),
                                       minval=prob,
                                       maxval=prob,
                                       name="rand_spike_prob")
        # For each value in the array, if it is 1 the spike will occour otherwise if it is 0 no spike would occour.
        spike_binary_cond = tf.floor(tf.add(random_uniform, spike_prob), name="rand_spike_binary")
        spike_strength = tf.multiply(spike_binary_cond, strength)
        spike_result = tf.add(layer, tf.multiply(layer, spike_strength))
        return spike_result

    def __dynamic_strength_random_spike(self, layer, prob, seed=filter_configs.RANDOM_SPIKE_SEED):
        # Spike prob condition
        random_uniform = tf.random_uniform(shape=tf.shape(layer),
                                           minval=0,
                                           maxval=1,
                                           seed=seed,
                                           name="rand_spike_cond")
        spike_prob = tf.random_uniform(shape=tf.shape(layer),
                                       minval=prob,
                                       maxval=prob,
                                       name="rand_spike_prob")
        # For each value in the array, if it is 1 the spike will occour otherwise if it is 0 no spike would occour.
        spike_binary_cond = tf.floor(tf.add(random_uniform, spike_prob), name="rand_spike_binary")
        strength = tf.random_uniform(shape=tf.shape(layer),
                                     minval=0,
                                     maxval=1,
                                     seed=filter_configs.DYNAMIC_RANDOM_SPIKE_SEED,
                                     name="rand_strength_val")
        spike_strength = tf.multiply(spike_binary_cond, strength)
        spike_result = tf.add(layer, tf.multiply(layer, spike_strength))
        return spike_result

    def __get_opposite_sign_layer(self, layer, layer_with_act, activation_function, prob):
        if not isinstance(prob, tf.Tensor) and (not prob or prob <= 0):
            return layer, layer_with_act

        if activation_function is None:
            layer = self.__opposite_sign_layer(layer=layer,
                                               prob=prob)
        else:
            layer_with_act = self.__opposite_sign_layer(layer=layer_with_act,
                                                        prob=prob)

        return layer, layer_with_act

    def __opposite_sign_layer(self, layer, prob, seed=filter_configs.OPPOSITE_SIGN_SEED):

        def opposite_sign(layer, prob):
            # Opposite sign prob condition
            random_uniform = tf.random_uniform(shape=tf.shape(layer),
                                               minval=0,
                                               maxval=1,
                                               seed=seed,
                                               name="opposite_sign_cond")
            op_sign_prob = tf.random_uniform(shape=tf.shape(layer),
                                             minval=prob,
                                             maxval=prob,
                                             name="opposite_sign_prob")
            # For each value in the array, if it is 1 the spike will occour otherwise if it is 0 no spike would occour.
            spike_binary_cond = tf.floor(tf.add(random_uniform, op_sign_prob), name="op_sign_binary")
            spike_strength = tf.multiply(spike_binary_cond, -2)  # Multiply by -2 in order when to add it subtracts the current value minus another time
            spike_result = tf.add(layer, tf.multiply(layer, spike_strength))
            return spike_result

            return layer

        def no_opposite_sign(layer):
            return layer

        return tf.cond(self.is_training,
                       lambda: opposite_sign(layer=layer, prob=prob),
                       lambda: no_opposite_sign(layer=layer))

    def __str__(self):
        str_model = type(self).__name__ + '\n'
        str_model += '\t\tinput layer -> ' + str(self.num_input_neurons) + '\n'
        str_model += '\t\thidden layers:\n'
        for i, l in enumerate(self.hidden_layers):
            str_model += '\t\t\t' + str(i + 1) + ' -> ' + str(l.num_neurons) + '\n'
        str_model += '\t\toutput layer -> ' + str(self.num_output_neurons)
        return str_model


class Ndiv2plus1Model(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(Ndiv2plus1Model, self).__init__(num_input_neurons=num_input_neurons,
                                              num_output_neurons=num_output_neurons,
                                              hidden_layers_act_func=tf.nn.leaky_relu,
                                              output_act_func=None,
                                              dropout_prob=dropout_prob,
                                              noise_prob=noise_prob,
                                              noise_std=noise_std,
                                              dropconnect_prob=dropconnect_prob,
                                              random_spike_prob=random_spike_prob,
                                              spike_strength=spike_strength,
                                              opposite_sign_prob=opposite_sign_prob,
                                              use_batch_normalization=use_batch_normalization,
                                              use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=2049),
            Layer(num_neurons=1025),
            Layer(num_neurons=513),
            Layer(num_neurons=257),
            Layer(num_neurons=129),
            Layer(num_neurons=65),
            Layer(num_neurons=33)
        ]


class DiamondModel(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(DiamondModel, self).__init__(num_input_neurons=num_input_neurons,
                                           num_output_neurons=num_output_neurons,
                                           hidden_layers_act_func=tf.nn.leaky_relu,
                                           output_act_func=None,
                                           dropout_prob=dropout_prob,
                                           noise_prob=noise_prob,
                                           noise_std=noise_std,
                                           dropconnect_prob=dropconnect_prob,
                                           random_spike_prob=random_spike_prob,
                                           spike_strength=spike_strength,
                                           use_batch_normalization=use_batch_normalization,
                                           use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=5120),
            Layer(num_neurons=6144),
            Layer(num_neurons=7168),
            Layer(num_neurons=8192),
            Layer(num_neurons=6144),
            Layer(num_neurons=4096),
            Layer(num_neurons=2048),
            Layer(num_neurons=512),
            Layer(num_neurons=128),
            Layer(num_neurons=32),
            Layer(num_neurons=8)
        ]


class ShrinkedDiamondModel(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(ShrinkedDiamondModel, self).__init__(num_input_neurons=num_input_neurons,
                                                   num_output_neurons=num_output_neurons,
                                                   hidden_layers_act_func=tf.nn.leaky_relu,
                                                   output_act_func=None,
                                                   dropout_prob=dropout_prob,
                                                   noise_prob=noise_prob,
                                                   noise_std=noise_std,
                                                   dropconnect_prob=dropconnect_prob,
                                                   random_spike_prob=random_spike_prob,
                                                   spike_strength=spike_strength,
                                                   opposite_sign_prob=opposite_sign_prob,
                                                   use_batch_normalization=use_batch_normalization,
                                                   use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=1024),
            Layer(num_neurons=1536),
            Layer(num_neurons=2048),
            Layer(num_neurons=2560),
            Layer(num_neurons=3072),
            Layer(num_neurons=3584),
            Layer(num_neurons=3072),
            Layer(num_neurons=2560),
            Layer(num_neurons=2048),
            Layer(num_neurons=1536),
            Layer(num_neurons=1024),
            Layer(num_neurons=512),
            Layer(num_neurons=128),
            Layer(num_neurons=32)
        ]


class FFTNdiv2plus1Model(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(FFTNdiv2plus1Model, self).__init__(num_input_neurons=num_input_neurons,
                                                 num_output_neurons=num_output_neurons,
                                                 hidden_layers_act_func=tf.nn.leaky_relu,
                                                 output_act_func=None,
                                                 dropout_prob=dropout_prob,
                                                 noise_prob=noise_prob,
                                                 noise_std=noise_std,
                                                 dropconnect_prob=dropconnect_prob,
                                                 random_spike_prob=random_spike_prob,
                                                 spike_strength=spike_strength,
                                                 opposite_sign_prob=opposite_sign_prob,
                                                 use_batch_normalization=use_batch_normalization,
                                                 use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=1025),
            Layer(num_neurons=513),
            Layer(num_neurons=257),
            Layer(num_neurons=129),
            Layer(num_neurons=65),
            Layer(num_neurons=33)
        ]


class FFTDiamondModel(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(FFTDiamondModel, self).__init__(num_input_neurons=num_input_neurons,
                                              num_output_neurons=num_output_neurons,
                                              hidden_layers_act_func=tf.nn.leaky_relu,
                                              output_act_func=None,
                                              dropout_prob=dropout_prob,
                                              noise_prob=noise_prob,
                                              noise_std=noise_std,
                                              dropconnect_prob=dropconnect_prob,
                                              random_spike_prob=random_spike_prob,
                                              spike_strength=spike_strength,
                                              opposite_sign_prob=opposite_sign_prob,
                                              use_batch_normalization=use_batch_normalization,
                                              use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=2560),
            Layer(num_neurons=3072),
            Layer(num_neurons=3584),
            Layer(num_neurons=3072),
            Layer(num_neurons=2560),
            Layer(num_neurons=2048),
            Layer(num_neurons=512),
            Layer(num_neurons=128),
            Layer(num_neurons=32),
            Layer(num_neurons=8)
        ]


class ConvModel(Model):

    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(ConvModel, self).__init__(num_input_neurons=num_input_neurons,
                                        num_output_neurons=num_output_neurons,
                                        hidden_layers_act_func=tf.nn.leaky_relu,
                                        output_act_func=None,
                                        dropout_prob=dropout_prob,
                                        noise_prob=noise_prob,
                                        noise_std=noise_std,
                                        dropconnect_prob=dropconnect_prob,
                                        random_spike_prob=random_spike_prob,
                                        spike_strength=spike_strength,
                                        opposite_sign_prob=opposite_sign_prob,
                                        use_batch_normalization=use_batch_normalization,
                                        use_alpha_dropout=use_alpha_dropout)

    def define_model(self):
        self.__define_hidden_layers_number_output_neurons()
        self.__define_weights()
        self.__define_bias()

    def build_model(self):
        """Model function for CNN."""
        mg.ERROR("You are using the 'ConvModel', note that this model is deprecated.")
        # Input Layer
        num_cols = int(mg.NUMBER_OF_SAMPLES_PER_FRAME / 64)
        input_layer = tf.reshape(self.X, [-1, 64, num_cols, 1])
        # input_layer = self.X

        # Convolutional Layer #1
        kernel_col_size = 4
        conv1 = tf.layers.conv2d(
            inputs=input_layer,
            filters=32,
            kernel_size=[1, kernel_col_size],
            padding="same",
            activation=self.hidden_layers_act_func)

        # Pooling Layer #1
        pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[1, 2], strides=2)

        # Convolutional Layer #2 and Pooling Layer #2
        conv2 = tf.layers.conv2d(
            inputs=pool1,
            filters=64,
            kernel_size=[1, kernel_col_size],
            padding="same",
            activation=self.hidden_layers_act_func)
        pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[1, 2], strides=2)

        # Dense Layer
        total_size = int(mg.NUMBER_OF_SAMPLES_PER_FRAME * kernel_col_size)
        pool2_flat = tf.reshape(pool2, [1, total_size])
        dense = tf.layers.dense(inputs=pool2_flat, units=1024, activation=self.hidden_layers_act_func)
        # dropout = tf.layers.dropout(inputs=dense, rate=0.4, training=self.is_training)

        # Logits Layer
        self.logits = tf.layers.dense(inputs=dense, units=self.num_output_neurons)
        # self.logits = tf.reshape(self.logits, shape=[1, None])
        self.predictions = self.output_act_func(self.logits_without_act_func)

        # _, layer = self._getNonNormalizedLayer(input_data=pool2,
        #                                         w=self.weights['w_1'],
        #                                         b=self.biases['b_1'],
        #                                         activation_function=self.hidden_layers_act_func,
        #                                         dropout_prob=self.dropout_prob)

        # self.logits, self.predictions = self._getNonNormalizedLayer(input_data=layer,
        #                                                             w=self.weights['w_out'],
        #                                                             b=self.biases['b_out'],
        #                                                             activation_function=self.output_act_func,
        #                                                             dropout_prob=None)  # Last layer must not have dropout

    def __define_hidden_layers_number_output_neurons(self):
        self.n_hidden_1 = 1024
        self.n_hidden_2 = 512

    def __define_weights(self):
        # Define the weights variable, initilized by default the fist time with a specific seed.
        # We use a seed to be able to replicate the results
        # self.weights = {
        #     'w_1': self.weigths_initializer.initialize(act_func=self.hidden_layers_act_func,
                                                        #  use_alpha_dropout_initialization=self.use_alpha_dropout,
        #                                                shape=[self.n_hidden_1, self.n_hidden_2],
        #                                                seed=filter_configs.RANDOM_SEEDS_WEIGHTS['w_1'],
        #                                                name='w_1'),

        #     'w_out': self.weigths_initializer.initialize(act_func=self.output_act_func,
                                                        #    use_alpha_dropout_initialization=self.use_alpha_dropout,
        #                                                  shape=[self.n_hidden_2, self.output_act_func],
        #                                                  seed=filter_configs.RANDOM_SEEDS_WEIGHTS['w_out'],
        #                                                  name='w_out')
        # }

        pass

    # This function is called only when the data is not to be normalized
    def __define_bias(self):
        # Define the biases weigths variables, also initilized by default with a specific seed.
        # We use a seed to be able to replicate the results
        self.biases = {
            'b_1': self.weigths_initializer.initialize_bias(act_func=self.hidden_layers_act_func,
                                                            use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                            shape=[self.n_hidden_1],
                                                            seed=filter_configs.RANDOM_SEEDS_BIASES['b_1'],
                                                            name='b_1'),

            'b_out': self.weigths_initializer.initialize_bias(act_func=self.output_act_func,
                                                              use_alpha_dropout_initialization=self.use_alpha_dropout,
                                                              shape=[self.num_output_neurons],
                                                              seed=filter_configs.RANDOM_SEEDS_BIASES['b_out'],
                                                              name='b_out')
        }


class FFTEficientMF(Model):
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(FFTEficientMF, self).__init__(num_input_neurons=num_input_neurons,
                                            num_output_neurons=num_output_neurons,
                                            hidden_layers_act_func=tf.nn.leaky_relu,
                                            output_act_func=None,
                                            dropout_prob=dropout_prob,
                                            noise_prob=noise_prob,
                                            noise_std=noise_std,
                                            dropconnect_prob=dropconnect_prob,
                                            random_spike_prob=random_spike_prob,
                                            spike_strength=spike_strength,
                                            opposite_sign_prob=opposite_sign_prob,
                                            use_batch_normalization=use_batch_normalization,
                                            use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=128),
            Layer(num_neurons=64),
            Layer(num_neurons=32),
            Layer(num_neurons=8)
        ]


class FFTEficientMF_v2(Model):
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(FFTEficientMF_v2, self).__init__(num_input_neurons=num_input_neurons,
                                               num_output_neurons=num_output_neurons,
                                               hidden_layers_act_func=tf.nn.leaky_relu,
                                               output_act_func=None,
                                               dropout_prob=dropout_prob,
                                               noise_prob=noise_prob,
                                               noise_std=noise_std,
                                               dropconnect_prob=dropconnect_prob,
                                               random_spike_prob=random_spike_prob,
                                               spike_strength=spike_strength,
                                               opposite_sign_prob=opposite_sign_prob,
                                               use_batch_normalization=use_batch_normalization,
                                               use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=256),
            Layer(num_neurons=128),
            Layer(num_neurons=64),
            Layer(num_neurons=32),
            Layer(num_neurons=8)
        ]


class EndToEndDNN(Model):
    '''This model is based from:
    An End-to-End Neural Network for Polyphonic Piano Music Transcription
    https://arxiv.org/pdf/1508.01774.pdf
    '''
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(EndToEndDNN, self).__init__(num_input_neurons=num_input_neurons,
                                          num_output_neurons=num_output_neurons,
                                          hidden_layers_act_func=tf.nn.leaky_relu,
                                          output_act_func=None,
                                          dropout_prob=dropout_prob,
                                          noise_prob=noise_prob,
                                          noise_std=noise_std,
                                          dropconnect_prob=dropconnect_prob,
                                          random_spike_prob=random_spike_prob,
                                          spike_strength=spike_strength,
                                          opposite_sign_prob=opposite_sign_prob,
                                          use_batch_normalization=use_batch_normalization,
                                          use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=125),
            Layer(num_neurons=125),
            Layer(num_neurons=125)
        ]


class SimpleFramewiseDNN(Model):
    '''This model is based from:
    ON THE POTENTIAL OF SIMPLE FRAMEWISE APPROACHES TO PIANO TRANSCRIPTION
    https://wp.nyu.edu/ismir2016/wp-content/uploads/sites/2294/2016/07/179_Paper.pdf
    '''
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(SimpleFramewiseDNN, self).__init__(num_input_neurons=num_input_neurons,
                                                 num_output_neurons=num_output_neurons,
                                                 hidden_layers_act_func=tf.nn.leaky_relu,
                                                 output_act_func=None,
                                                 dropout_prob=dropout_prob,
                                                 noise_prob=noise_prob,
                                                 noise_std=noise_std,
                                                 dropconnect_prob=dropconnect_prob,
                                                 random_spike_prob=random_spike_prob,
                                                 spike_strength=spike_strength,
                                                 opposite_sign_prob=opposite_sign_prob,
                                                 use_batch_normalization=use_batch_normalization,
                                                 use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=512),
            Layer(num_neurons=512),
            Layer(num_neurons=512)
        ]


class PPSingleNN(Model):
    def __init__(self,
                 num_input_neurons,
                 num_output_neurons,
                 dropout_prob=None,
                 noise_prob=None,
                 noise_std=0.2,
                 dropconnect_prob=None,
                 random_spike_prob=None,
                 spike_strength=None,
                 opposite_sign_prob=None,
                 use_batch_normalization=False,
                 use_alpha_dropout=False):

        super(PPSingleNN, self).__init__(num_input_neurons=num_input_neurons,
                                         num_output_neurons=num_output_neurons,
                                         hidden_layers_act_func=tf.nn.leaky_relu,
                                         output_act_func=None,
                                         dropout_prob=dropout_prob,
                                         noise_prob=noise_prob,
                                         noise_std=noise_std,
                                         dropconnect_prob=dropconnect_prob,
                                         random_spike_prob=random_spike_prob,
                                         spike_strength=spike_strength,
                                         opposite_sign_prob=opposite_sign_prob,
                                         use_batch_normalization=use_batch_normalization,
                                         use_alpha_dropout=use_alpha_dropout)

    def define_hidden_layers(self):
        self.hidden_layers = [
            Layer(num_neurons=5),
            Layer(num_neurons=5),
            Layer(num_neurons=5)
        ]
