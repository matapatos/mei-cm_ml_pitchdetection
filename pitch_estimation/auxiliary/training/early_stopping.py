import copy


from pitch_estimation.auxiliary.metrics.types.metrics import BEST_MODEL_METRICS
import utils as mg_utils


class EarlyStopping():

    def __init__(self, num_iterations_after_best, min_percentage=0.1, metrics='all', is_training=True):
        '''
        \'num_iterations_after_best\' must be an integer (inclusive).
        \'min_percentage\' must be a number between 0 exclusive and 1.
        \'metrics\' can be just a single metric, eg. fmeasure or fmeasure_onset. Or it is possible to pass also an array of metrics. Check the variable pitch_estimation.auxiliary.metrics.types.metrics.BEST_MODEL_METRICS for possible metrics.

        WARN: Please note that early stopping only works if the option of saving the best model in the classifier or the post-processing unit is enabled!
        '''
        self.num_iterations_after_best = num_iterations_after_best
        self.min_percentage = min_percentage
        self.metrics = self.__get_metrics(metrics)
        self.is_training = is_training
        self.__metrics_values = {}
        for m in self.metrics:
            self.__metrics_values[m] = {'best_value': 0.0, 'global_step': 0}
        self.__global_step = 0

    def update_best_model(self, metric_name, value):
        if not self.is_training:
            return False

        if not isinstance(metric_name, str):
            raise TypeError('\'metric_name\' expected str but %s given.' % (type(metric_name)))

        metric_name = metric_name.lower()
        if metric_name and metric_name in self.metrics:
            if self.__metrics_values[metric_name]['best_value'] <= value:  # Making sure that value is equal or smaller than previous best one
                self.__metrics_values[metric_name]['best_value'] = value
                self.__metrics_values[metric_name]['global_step'] = self.__global_step

    def increment_global_step(self):
        if not self.is_training:
            return False

        self.__global_step += 1
        if self.__can_stop():
            reason = 'Early stopping...'
            if hasattr(self, 'identifier'):
                reason = 'Early stopping from %s...' % (self.identifier)
            raise EarlyStoppingException(reason)

    def __can_stop(self):
        '''
        Returns True if all metrics given to take into consideration early stopping technique have all reached their best model. False otherwise
        '''
        possible_to_stop = False
        for _, m_val in self.__metrics_values.items():
            max_step = m_val['global_step'] + self.num_iterations_after_best
            if max_step < self.__global_step:
                if m_val['best_value'] >= self.min_percentage:
                    possible_to_stop = True
                    print(m_val)
                    continue

            possible_to_stop = False
            break

        return possible_to_stop

    def __get_metrics(self, given_metrics):
        if not given_metrics:
            raise TypeError('Expected str or list but %s given.' % (type(given_metrics)))

        if isinstance(given_metrics, str):
            given_metrics = [given_metrics]

        metrics = []
        for m in given_metrics:
            if not isinstance(m, str):
                raise ValueError('Expected one of the following metrics %s but %s given.' % (mg_utils.array_to_string(BEST_MODEL_METRICS), m))

            m = m.lower()
            if m == 'all':
                metrics = copy.deepcopy(BEST_MODEL_METRICS)
                break

            if m not in BEST_MODEL_METRICS:
                raise ValueError('Expected one of the following metrics %s but %s given.' % (mg_utils.array_to_string(BEST_MODEL_METRICS), m))

            metrics.append(m)

        return metrics


class EarlyStoppingException(Exception):
    def __init__(self, *args, **kwargs):
        super(EarlyStoppingException, self).__init__(*args, **kwargs)
