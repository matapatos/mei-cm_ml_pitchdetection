import tensorflow as tf
import collections
import os.path as path
import numpy as np


from pitch_estimation.auxiliary.metrics.configs import ITERATION_DATA_TYPE,\
                                                       GLOBAL_DATA_TYPE,\
                                                       MIN_PERCENTAGE_TO_BE_CONSIDERED_VALID_OFFSET_NOTE,\
                                                       MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE,\
                                                       RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC
import main_globals as mg
from pitch_estimation.auxiliary.metrics.types.wrapper import MetricsWraper
from pitch_estimation.auxiliary.metrics.utils import ConfusionMatrix
from pitch_estimation.auxiliary.metrics.sub.container import Container
import pitch_estimation.tasks.learner.filters.configs as filter_configs
from pitch_estimation.auxiliary.training.early_stopping import EarlyStoppingException
import pitch_estimation.auxiliary.metrics.utils as utils


class Controller():

    def __init__(self, container):
        self.container = container
        self.best_results = collections.defaultdict(float)

    def _define_all_metrics(self):
        graph = self.container.graph
        if graph is None:
            graph = tf.get_default_graph()
        with graph.as_default():

            self.__define_global_step()
            self.__define_placeholders()
            self.__define_TF_metrics()

    def init_summary_writters(self, summary_output_folder):
        self.training_global_summary_writer = self.__get_summary_writer(sub_directories=['train'],
                                                                        summary_output_folder=summary_output_folder,
                                                                        data_type='global')
        self.training_iteration_summary_writer = self.__get_summary_writer(sub_directories=['train'],
                                                                           summary_output_folder=summary_output_folder,
                                                                           data_type='iteration')

        self.testing_global_summary_writer = self.__get_summary_writer(sub_directories=['test'],
                                                                       summary_output_folder=summary_output_folder,
                                                                       data_type='global')
        self.testing_iteration_summary_writer = self.__get_summary_writer(sub_directories=['test'],
                                                                          summary_output_folder=summary_output_folder,
                                                                          data_type='iteration')

    def update_metrics(self, run_mode, note_result, is_last_music, save_best_model=False, update_summaries=True):
        if run_mode == mg.RunMode.TRAINING:
            self.__update_training_metrics(note_result=note_result,
                                           is_last_music=is_last_music,
                                           update_summaries=update_summaries)
        elif run_mode == mg.RunMode.EVALUATING:
            self.__update_evaluation_metrics(note_result=note_result,
                                             is_last_music=is_last_music,
                                             save_best_model=save_best_model,
                                             update_summaries=update_summaries)
        else:
            self.__update_prediction_metrics(note_result=note_result,
                                             is_last_music=is_last_music,
                                             update_summaries=update_summaries)

    def reset_all_metrics(self):
        self.__reset_global_metrics()
        self.__reset_iteration_training_metrics_variables()
        self.__reset_iteration_testing_metrics_variables()

    def increase_global_step(self):
        self.container.session.run(self.increment_global_step_op)
        if self.container.early_stopping:
            self.container.early_stopping.increment_global_step()

    def add_to_training_summary(self, summary):
        self.__add_summary(summary_writer=self.training_iteration_summary_writer, summary=summary)

    @property
    def global_step_val(self):
        return self.__run_session_operations_with_feed_dict(operations=self.global_step)

    def close(self):
        if self.training_global_summary_writer:
            self.training_global_summary_writer.close()

        if self.training_iteration_summary_writer:
            self.training_iteration_summary_writer.close()

        if self.testing_global_summary_writer:
            self.testing_global_summary_writer.close()

        if self.testing_iteration_summary_writer:
            self.testing_iteration_summary_writer.close()

    def __update_training_metrics(self, note_result, is_last_music, update_summaries=True):
        labels = np.rint(note_result.labels)
        predictions = np.rint(note_result.predictions)
        is_multi_class = (note_result.note_number == filter_configs.MULTI_CLASS_NOTE_NUMBER)
        # Update global metrics
        # self.tensorboard_training_global_metrics_queue.updateLoss(losses=note_result.losses)
        operations = self.tensorboard_training_global_metrics_queue.operations
        self.__run_session_metrics_operations(operations=operations, labels=labels, predictions=predictions, is_multi_class=is_multi_class)

        # Update iteration metrics
        # self.tensorboard_training_iteration_metrics_queue.updateLoss(losses=losses)
        operations = self.tensorboard_training_iteration_metrics_queue.operations
        self.__run_session_metrics_operations(operations=operations, labels=labels, predictions=predictions, is_multi_class=is_multi_class)
        if is_last_music:
            if update_summaries:
                # Add to graph global training metrics
                summaries_operations = self.tensorboard_training_global_metrics_queue.summaries
                self.__calculate_metrics_with_operations(operations=summaries_operations, summary_writer=self.training_global_summary_writer)

                # Add to graph iteration training metrics
                summaries_operations = self.tensorboard_training_iteration_metrics_queue.summaries
                self.__calculate_metrics_with_operations(operations=summaries_operations, summary_writer=self.training_iteration_summary_writer)

            self.__reset_iteration_training_metrics_variables()

    def __update_evaluation_metrics(self, note_result, is_last_music, save_best_model=False, update_summaries=True):
        labels = np.rint(note_result.labels)
        predictions = np.rint(note_result.predictions)
        is_multi_class = (note_result.note_number == filter_configs.MULTI_CLASS_NOTE_NUMBER)
        # Update global metrics
        # self.tensorboard_testing_global_metrics_queue.updateLoss(losses=note_result.losses)
        operations = self.tensorboard_testing_global_metrics_queue.operations
        self.__run_session_metrics_operations(operations=operations, labels=labels, predictions=predictions, is_multi_class=is_multi_class)

        # Update local metrics
        # self.tensorboard_testing_iteration_metrics_queue.updateLoss(losses=losses)
        operations = self.tensorboard_testing_iteration_metrics_queue.operations
        self.__run_session_metrics_operations(operations=operations, labels=labels, predictions=predictions, is_multi_class=is_multi_class)

        if is_last_music:
            if update_summaries:
                # Add to graph global testing metrics
                summaries_operations = self.tensorboard_testing_global_metrics_queue.summaries
                self.__calculate_metrics_with_operations(operations=summaries_operations, summary_writer=self.testing_global_summary_writer)

                # Add to graph iteration testing metrics
                summaries_operations = self.tensorboard_testing_iteration_metrics_queue.summaries
                self.__calculate_metrics_with_operations(operations=summaries_operations, summary_writer=self.testing_iteration_summary_writer)

            if save_best_model:
                metrics_values = self.tensorboard_testing_iteration_metrics_queue.metrics_vals
                self.__save_best_model_of_metrics_values(metrics_values=metrics_values)

            self.__reset_iteration_testing_metrics_variables()

    def __update_prediction_metrics(self, note_result, is_last_music, update_summaries=True):
        labels = np.rint(note_result.labels)
        predictions = np.rint(note_result.predictions)

        self.__update_evaluation_metrics(note_result=note_result, is_last_music=is_last_music)
        operations = [self.confusion_matrix]
        ops_result = self.__run_session_metrics_operations(operations=operations, labels=labels, predictions=predictions)
        self.__print_confusion_matrix_from_result(result=ops_result)

    def __reset_global_metrics(self):
        operations = [self.tensorboard_training_global_metrics_queue.reset_metrics_variables_op,
                      self.tensorboard_testing_global_metrics_queue.reset_metrics_variables_op]
        self.container.session.run(operations)

    def __define_global_step(self):
        self.global_step = tf.Variable(0, name='global_step', trainable=False, dtype=tf.int32)
        self.increment_global_step_op = tf.assign_add(ref=self.global_step, value=1)

    def __define_placeholders(self):
        # Define metrics placeholders
        self.labels_frame_based = tf.placeholder(tf.int32, [None], name='LabelsFrameBased')
        self.predictions_frame_based = tf.placeholder(tf.int32, [None], name='PredictionsFrameBased')

        self.labels_onset = tf.placeholder(tf.int32, [None], name='LabelsOnset')
        self.predictions_onset = tf.placeholder(tf.int32, [None], name='PredictionsOnset')

        self.labels_onset_offset = tf.placeholder(tf.int32, [None], name='LabelsOnsetOffset')
        self.predictions_onset_offset = tf.placeholder(tf.int32, [None], name='PredictionsOnsetOffset')

    def __define_TF_metrics(self):
        self.__define_TF_training_metrics(plot_scope='Train')
        self.__define_TF_testing_metrics(plot_scope='Test')
        self.__define_TF_prediction_metrics()

    def __define_TF_training_metrics(self, plot_scope='Train'):
        # Define global metrics
        self.tensorboard_training_global_metrics_queue = MetricsWraper(session=self.container.session,
                                                                       labels_placeholder_frame_based=self.labels_frame_based,
                                                                       predictions_placeholder_frame_based=self.predictions_frame_based,
                                                                       labels_placeholder_onset=self.labels_onset,
                                                                       predictions_placeholder_onset=self.predictions_onset,
                                                                       labels_placeholder_onset_offset=self.labels_onset_offset,
                                                                       predictions_placeholder_onset_offset=self.predictions_onset_offset,
                                                                       plot_scope=plot_scope,
                                                                       data_type_scope=GLOBAL_DATA_TYPE,
                                                                       is_to_stream_loss=False)

        # Define local/per music metrics
        self.tensorboard_training_iteration_metrics_queue = MetricsWraper(session=self.container.session,
                                                                          labels_placeholder_frame_based=self.labels_frame_based,
                                                                          predictions_placeholder_frame_based=self.predictions_frame_based,
                                                                          labels_placeholder_onset=self.labels_onset,
                                                                          predictions_placeholder_onset=self.predictions_onset,
                                                                          labels_placeholder_onset_offset=self.labels_onset_offset,
                                                                          predictions_placeholder_onset_offset=self.predictions_onset_offset,
                                                                          plot_scope=plot_scope,
                                                                          data_type_scope=ITERATION_DATA_TYPE,
                                                                          is_to_stream_loss=False)

    def __define_TF_testing_metrics(self, plot_scope='Test'):
        # Define global metrics
        self.tensorboard_testing_global_metrics_queue = MetricsWraper(session=self.container.session,
                                                                      labels_placeholder_frame_based=self.labels_frame_based,
                                                                      predictions_placeholder_frame_based=self.predictions_frame_based,
                                                                      labels_placeholder_onset=self.labels_onset,
                                                                      predictions_placeholder_onset=self.predictions_onset,
                                                                      labels_placeholder_onset_offset=self.labels_onset_offset,
                                                                      predictions_placeholder_onset_offset=self.predictions_onset_offset,
                                                                      plot_scope=plot_scope,
                                                                      data_type_scope=GLOBAL_DATA_TYPE,
                                                                      is_to_stream_loss=False)

        # Define local/per music metrics
        self.tensorboard_testing_iteration_metrics_queue = MetricsWraper(session=self.container.session,
                                                                         labels_placeholder_frame_based=self.labels_frame_based,
                                                                         predictions_placeholder_frame_based=self.predictions_frame_based,
                                                                         labels_placeholder_onset=self.labels_onset,
                                                                         predictions_placeholder_onset=self.predictions_onset,
                                                                         labels_placeholder_onset_offset=self.labels_onset_offset,
                                                                         predictions_placeholder_onset_offset=self.predictions_onset_offset,
                                                                         plot_scope=plot_scope,
                                                                         data_type_scope=ITERATION_DATA_TYPE,
                                                                         is_to_stream_loss=False)

    def __define_TF_prediction_metrics(self):
        self.confusion_matrix = tf.confusion_matrix(labels=self.labels_frame_based,
                                                    predictions=self.predictions_frame_based,
                                                    num_classes=2)

    def __calculate_metrics_with_operations(self, operations, summary_writer):
        all_summaries = self.__run_session_operations_with_feed_dict(operations=operations)
        for summary in all_summaries:
            self.__add_summary(summary_writer=summary_writer, summary=summary)

    def __reset_iteration_training_metrics_variables(self):
        operations = [self.tensorboard_training_iteration_metrics_queue.reset_metrics_variables_op]
        self.__run_session_operations_with_feed_dict(operations=operations)

    def __reset_iteration_testing_metrics_variables(self):
        operations = [self.tensorboard_testing_iteration_metrics_queue.reset_metrics_variables_op]
        self.__run_session_operations_with_feed_dict(operations=operations)

    def __add_summary(self, summary_writer, summary):
        global_step = tf.train.global_step(sess=self.container.session, global_step_tensor=self.global_step)
        summary_writer.add_summary(summary, global_step=global_step)

    def __run_session_metrics_operations(self, operations, labels, predictions, remove_true_negatives=True, is_multi_class=False):
        labels_onset, predictions_onset, labels_onset_offset, predictions_onset_offset = self.__get_note_based_labels_and_predictions(labels=labels,
                                                                                                                                      predictions=predictions,
                                                                                                                                      is_multi_class=is_multi_class)

        if remove_true_negatives:
            labels, predictions = self.__remove_true_negatives(labels, predictions)

        feed_dict = {
            self.labels_frame_based: labels,
            self.predictions_frame_based: predictions,

            self.labels_onset: labels_onset,
            self.predictions_onset: predictions_onset,

            self.labels_onset_offset: labels_onset_offset,
            self.predictions_onset_offset: predictions_onset_offset
        }
        if labels is None or predictions is None:
            feed_dict = None

        return self.__run_session_operations_with_feed_dict(operations=operations, feed_dict=feed_dict)

    def __run_session_stream_metrics_operations(self, operations):
        return self.__run_session_operations_with_feed_dict(operations=operations, feed_dict=None)

    def __run_session_operations_with_feed_dict(self, operations, feed_dict=None):
        return self.container.session.run(operations, feed_dict=feed_dict)

    def __get_summary_filename(self):
        return "summary_" + str(self.container.note_number)

    def __get_tensorboard_output_filepath(self, sub_directories, main_directory, data_type):
        output_filepath = main_directory
        for d in sub_directories:
            output_filepath = path.join(output_filepath, d)

        output_filepath = path.join(output_filepath, Container.get_class_name(self.container))
        output_filepath = path.join(output_filepath, self.__get_summary_filename())
        return path.join(output_filepath, data_type)

    def __get_summary_writer(self, sub_directories, summary_output_folder, data_type):
        output_filepath = self.__get_tensorboard_output_filepath(sub_directories=sub_directories,
                                                                 main_directory=summary_output_folder,
                                                                 data_type=data_type)
        return tf.summary.FileWriter(output_filepath, graph=self.container.graph)

    def __print_confusion_matrix_from_result(self, result):
        result = result[0]
        tn = result[0][0]
        fn = result[1][0]
        fp = result[0][1]
        tp = result[1][1]

        mg.DEBUG(" |{tp}, {fp}|\n\t|{fn}, {tn}|".format(**locals()))

    def __get_note_based_labels_and_predictions(self, labels, predictions, is_multi_class):
        if is_multi_class:
            new_shape = (int(len(labels) / mg.NUMBER_OF_NOTES), mg.NUMBER_OF_NOTES)
            labels = np.reshape(labels, newshape=new_shape)
            predictions = np.reshape(labels, newshape=new_shape)
            labels_onset = []
            predictions_onset = []
            labels_onset_offset = []
            predictions_onset_offset = []
            for l, p in zip(labels, predictions):
                l_onset, pred_onset, l_onset_offset, pred_onset_offset = self.__get_note_based_labels_and_predictions_from_1D_array(labels=l,
                                                                                                                                    predictions=p)
                labels_onset += l_onset
                predictions_onset += pred_onset
                labels_onset_offset += l_onset_offset
                predictions_onset_offset += pred_onset_offset

            return labels_onset, predictions_onset, labels_onset_offset, predictions_onset_offset
        else:
            return self.__get_note_based_labels_and_predictions_from_1D_array(labels=labels, predictions=predictions)

    def __get_note_based_labels_and_predictions_from_1D_array(self, labels, predictions):
        labels_notes_group = utils.get_notes_group(group=labels)
        predictions_onset_notes_group = utils.get_notes_group(group=predictions)
        predictions_onset_offset_notes_group = list(predictions_onset_notes_group)

        confusion_matrix_onset = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
        confusion_matrix_onset_offset = ConfusionMatrix(tp=0, fp=0, fn=0, tn=0)
        for l_g in labels_notes_group:
            notes = self.__get_valid_onset_notes_on_group_by_index(group=predictions_onset_notes_group, index=l_g.start_index)
            if len(notes) == 0:
                confusion_matrix_onset.fn += 1
                confusion_matrix_onset_offset.fn += 1
            else:
                confusion_matrix_onset.tp += 1
                offset_note = self.__get_valid_offset_note_from_notes(label_note=l_g, predicted_notes=notes)
                if offset_note is None:
                    confusion_matrix_onset_offset.fn += 1
                    predictions_onset_notes_group.remove(notes[0])
                else:
                    confusion_matrix_onset_offset.tp += 1
                    predictions_onset_offset_notes_group.remove(offset_note)
                    predictions_onset_notes_group.remove(offset_note)

        confusion_matrix_onset.fp = len(predictions_onset_notes_group)
        confusion_matrix_onset_offset.fp = len(predictions_onset_offset_notes_group)

        labels_onset, predictions_onset = self.__get_labels_and_predictions_from_confusion_matrix(cf=confusion_matrix_onset)
        labels_onset_offset, predictions_onset_offset = self.__get_labels_and_predictions_from_confusion_matrix(cf=confusion_matrix_onset_offset)

        return labels_onset, predictions_onset, labels_onset_offset, predictions_onset_offset

    def __get_labels_and_predictions_from_confusion_matrix(self, cf):
        # True Positives
        labels = [1] * cf.tp
        predictions = [1] * cf.tp

        # False Positives
        labels += [0] * cf.fp
        predictions += [1] * cf.fp

        # False Negatives
        labels += [1] * cf.fn
        predictions += [0] * cf.fn

        # True Negatives
        labels += [0] * cf.tn
        predictions += [0] * cf.tn

        return labels, predictions

    def __get_valid_onset_notes_on_group_by_index(self, group, index):
        '''
        This function is based from:
        http://music-ir.org/mirex/wiki/2007:Multiple_Fundamental_Frequency_Estimation_%26_Tracking_Results#Overall_Summary_Results_Task_II
        '''
        min_index = index - RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC
        max_index = index + RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC

        notes = []
        for g in group:
            if min_index <= g.start_index and max_index >= g.start_index:
                notes.append(g)

        return notes

    def __get_valid_offset_note_from_notes(self, label_note, predicted_notes):
        for p in predicted_notes:
            if self.__is_valid_offset(label_note=label_note, predicted_note=p):
                return p

        return None

    def __is_valid_offset(self, label_note, predicted_note):
        '''
        This function returns True if the predicted_note has the necessary guidelines
        to be considered a valid offset comparing with the label_note otherwise it
        returns False.

        Note that this already assumes that the predicted_note has a valid Onset,
        corresponding to the label_note. For that the predicted_note passed must be
        some returned by the function __getValidOnsetNotesOnGroupByIndex(...)

        The guidelines necessary to be considered with a valid offset were based and
        adapted from:
        http://music-ir.org/mirex/wiki/2007:Multiple_Fundamental_Frequency_Estimation_%26_Tracking_Results#Overall_Summary_Results_Task_II

        The adapted guidelines are marked in the code bellow by '# ADAPTED-GUIDELINE'
        '''
        real_size = label_note.end_index - label_note.start_index + 1
        predicted_size = predicted_note.end_index - label_note.start_index + 1

        if real_size < MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE:  # ADAPTED-GUIDELINE This condition was added by me. In the MIREX rules they don't include because they consider a note being played more than 50ms
            distance = abs(real_size - predicted_size)
            if distance < RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC:
                return True
        else:
            num_frames = real_size * 0.2
            real_num_frames = max(num_frames, MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE)
            if predicted_size >= real_num_frames:
                if predicted_note.end_index <= label_note.end_index + RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC:  # ADAPTED-GUIDELINE Added this condition to limit the size of the offset, in a way that it will be not larger than the real one
                    return True

        return False

    def __save_best_model_of_metrics_values(self, metrics_values):
        operations = list(metrics_values.values())
        results = self.__run_session_operations_with_feed_dict(operations=operations)

        if len(self.best_results) == 0:
            for i, (name, _) in enumerate(metrics_values.items()):
                value = results[i]
                self.best_results[name] = value
                self.container.save_best_model(metric_name=name,
                                               value=value)
                if self.container.early_stopping:
                    self.container.early_stopping.update_best_model(metric_name=name, value=value)
        else:
            for i, (name, value) in enumerate(self.best_results.items()):
                new_value = results[i]
                if value < new_value:
                    self.best_results[name] = new_value
                    self.container.save_best_model(metric_name=name,
                                                   value=new_value)
                    if self.container.early_stopping:
                        self.container.early_stopping.update_best_model(metric_name=name, value=new_value)

    def __remove_true_negatives(self, labels, predictions):
        if len(labels) != len(predictions):
            mg.ERROR('Labels and predictions size does\'t match.')

        new_labels = []
        new_predictions = []
        for i in range(len(labels)):
            label = labels[i]
            pred = predictions[i]

            if label == 0 and pred == 0:
                continue
            else:
                new_labels.append(label)
                new_predictions.append(pred)

        return new_labels, new_predictions
