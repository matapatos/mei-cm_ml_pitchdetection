
import abc
import os
import tensorflow as tf


import utils
import main_globals as mg


class Container():

    def __init__(self, note_number, best_model_folderpath=None, graph=None, early_stopping=False):
        self.note_number = note_number  # What is the expected note number to be discovered by this NeuralNetwork
        self.best_model_folderpath = best_model_folderpath
        self.graph = graph
        self.session = tf.Session(graph=self.graph)
        self.best_models_saver = None
        self.early_stopping = early_stopping

    def save_best_model(self, metric_name, value):
        metric_name = metric_name.lower()
        output_folderpath = self._mkdirs_full_output_folderpath_for_model(output_folderpath=self.best_model_folderpath,
                                                                          sub_folder='best' + os.sep + metric_name)
        filename = self.get_model_filename(with_extension=True)
        model_filepath = os.path.join(output_folderpath, filename)
        self.__save_best_model(model_filepath=model_filepath,
                               metric_value=value)

    def _mkdirs_full_output_folderpath_for_model(self, output_folderpath, sub_folder=None):
        name = Container.get_class_name(self)
        if hasattr(self, 'identifier'):
            if not ('nn_per_note' in self.identifier and 'single_nn' in self.identifier):
                name = self.identifier

        output_folderpath = os.path.join(output_folderpath, name)
        if sub_folder is not None:
            output_folderpath = os.path.join(output_folderpath, sub_folder)
        if not os.path.exists(output_folderpath):
            os.makedirs(output_folderpath)
        return output_folderpath

    def get_model_filename(self, model_iteration=None, with_extension=True):
        extension = mg.TENSORFLOW_MODEL_EXTENSION
        if not with_extension:
            extension = ""

        if model_iteration is not None:
            if model_iteration == 'last':
                extension = "_n\d{1,}" + extension
            else:
                extension = "_n" + str(model_iteration) + extension

        return "filter_" + str(self.note_number) + extension

    def _save_model(self, model_filepath, saver=None):
        if saver is None:
            saver = self.saver
        saver.save(self.session, model_filepath)

    def __save_best_model(self, model_filepath, metric_value):
        self._save_model(model_filepath=model_filepath,
                         saver=self.best_models_saver)
        with open(model_filepath + '.configs', 'w') as f:
            f.writelines('Value: ' + str(metric_value))

    @abc.abstractstaticmethod
    def get_class_name(filter_class):
        try:
            name = filter_class.__name__
        except AttributeError:
            name = type(filter_class).__name__

        name = name.replace('Filter', '')
        name = name.replace('Learner', '')
        name = utils.convert_to_underscore_and_lowercase(name=name)
        return name.lower()
