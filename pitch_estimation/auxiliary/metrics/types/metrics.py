import tensorflow as tf


BEST_MODEL_METRICS = ['accuracy', 'fmeasure', 'fmeasure_onset', 'fmeasure_onsetoffset']


class Metric():

    def __init__(self, plot_scope, data_type_scope, labels_placeholder=None, predictions_placeholder=None, name=None):
        self.labels_placeholder = labels_placeholder
        self.predictions_placeholder = predictions_placeholder
        self.plot_scope = plot_scope
        self.data_type_scope = data_type_scope

        self.metric_val = None
        self.update_op = None
        self.summary = None
        self.name = name

        if self.name is None:
            self.name = type(self).__name__

    def define_val_and_update_op(self, tf_metric_function):
        var_scope = self.plot_scope + self.data_type_scope
        self.metric_val, self.update_op = tf_metric_function(self.labels_placeholder, self.predictions_placeholder, name=var_scope)

    def define_scalar_summary(self):
        self.summary = tf.summary.scalar(self.name + '_' + self.data_type_scope, self.metric_val, family=self.plot_scope)


class Accuracy(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(Accuracy, self).__init__(plot_scope=plot_scope,
                                       data_type_scope=data_type_scope,
                                       labels_placeholder=labels_placeholder,
                                       predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.accuracy)
        self.define_scalar_summary()


class MeanAbsoluteError(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(MeanAbsoluteError, self).__init__(plot_scope=plot_scope,
                                                data_type_scope=data_type_scope,
                                                labels_placeholder=labels_placeholder,
                                                predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.mean_absolute_error)
        self.define_scalar_summary()


class MeanRelativeError(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(MeanRelativeError, self).__init__(plot_scope=plot_scope,
                                                data_type_scope=data_type_scope,
                                                labels_placeholder=labels_placeholder,
                                                predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.mean_relative_error)
        self.define_scalar_summary()


class MeanSquaredError(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(MeanSquaredError, self).__init__(plot_scope=plot_scope,
                                               data_type_scope=data_type_scope,
                                               labels_placeholder=labels_placeholder,
                                               predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.mean_squared_error)
        self.define_scalar_summary()


class RootMeanSquaredError(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(RootMeanSquaredError, self).__init__(plot_scope=plot_scope,
                                                   data_type_scope=data_type_scope,
                                                   labels_placeholder=labels_placeholder,
                                                   predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.root_mean_squared_error)
        self.define_scalar_summary()


class Recall(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope, name=None):
        super(Recall, self).__init__(plot_scope=plot_scope,
                                     data_type_scope=data_type_scope,
                                     labels_placeholder=labels_placeholder,
                                     predictions_placeholder=predictions_placeholder,
                                     name=name)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.recall)
        self.define_scalar_summary()


class Precision(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope, name=None):
        super(Precision, self).__init__(plot_scope=plot_scope,
                                        data_type_scope=data_type_scope,
                                        labels_placeholder=labels_placeholder,
                                        predictions_placeholder=predictions_placeholder,
                                        name=name)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.precision)
        self.define_scalar_summary()


class AUC(Metric):

    def __init__(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        super(AUC, self).__init__(plot_scope=plot_scope,
                                  data_type_scope=data_type_scope,
                                  labels_placeholder=labels_placeholder,
                                  predictions_placeholder=predictions_placeholder)

        self.define_val_and_update_op(tf_metric_function=tf.metrics.auc)
        self.define_scalar_summary()


class FMeasure(Metric):

    def __init__(self, recall_metric, precision_metric, plot_scope, data_type_scope, name=None):
        super(FMeasure, self).__init__(plot_scope=plot_scope,
                                       data_type_scope=data_type_scope,
                                       labels_placeholder=None,
                                       predictions_placeholder=None,
                                       name=name)

        self.recall_metric = recall_metric
        self.precision_metric = precision_metric
        self.define_val_and_update_op()
        self.define_scalar_summary()

    def define_val_and_update_op(self):
        # (2 * Recall * Precision ) / (Recall + Precision)
        zero = tf.constant(value=0.0)
        two = tf.constant(value=2.0)
        mul_recall_precision_op = tf.multiply(self.recall_metric.metric_val, self.precision_metric.metric_val)
        mul_recall_precision_op = tf.multiply(two, mul_recall_precision_op)
        add_recall_precision_op = tf.add(self.recall_metric.metric_val, self.precision_metric.metric_val)

        def zero_operation():
            return zero

        def fmeasure_operation():
            return tf.divide(mul_recall_precision_op, add_recall_precision_op)

        self.update_op = tf.cond(tf.equal(add_recall_precision_op, zero),
                                 zero_operation,
                                 fmeasure_operation)
        self.metric_val = self.update_op
