
import tensorflow as tf


import pitch_estimation.auxiliary.metrics.types.metrics as Metrics
import pitch_estimation.auxiliary.metrics.types.stream as StreamMetrics


class MetricsWraper():

    def __init__(self,
                 session,
                 labels_placeholder_frame_based,
                 predictions_placeholder_frame_based,
                 labels_placeholder_onset,
                 predictions_placeholder_onset,
                 labels_placeholder_onset_offset,
                 predictions_placeholder_onset_offset,
                 plot_scope,
                 data_type_scope,
                 is_to_stream_loss=True):
        self.__metrics_frame_based = self.__define_frame_based_metrics(labels_placeholder=labels_placeholder_frame_based,
                                                                       predictions_placeholder=predictions_placeholder_frame_based,
                                                                       plot_scope=plot_scope,
                                                                       data_type_scope=data_type_scope)
        self.__metrics_onset = self.__define_note_onset_metrics(labels_placeholder=labels_placeholder_onset,
                                                                predictions_placeholder=predictions_placeholder_onset,
                                                                plot_scope=plot_scope,
                                                                data_type_scope=data_type_scope)
        self.__metrics_onset_offset = self.__define_note_onset_offset_metrics(labels_placeholder=labels_placeholder_onset_offset,
                                                                              predictions_placeholder=predictions_placeholder_onset_offset,
                                                                              plot_scope=plot_scope,
                                                                              data_type_scope=data_type_scope)
        self.metrics = self.__metrics_frame_based
        self.metrics += self.__metrics_onset
        self.metrics += self.__metrics_onset_offset

        self.__metrics_vals = None

        self.__operations_frame_based = self.__get_frame_based_operations()
        self.__operations_onset = self.__get_onset_operations()
        self.__operations_onset_offset = self.__get_onset_offset_operations()
        self.operations = self.__operations_frame_based
        self.operations += self.__operations_onset
        self.operations += self.__operations_onset_offset

        self.__summaries_frame_based = self.__get_frame_based_summaries()
        self.__summaries_onset = self.__get_onset_summaries()
        self.__summaries_onset_offset = self.__get_onset_offset_summaries()
        self.summaries = self.__summaries_frame_based
        self.summaries += self.__summaries_onset
        self.summaries += self.__summaries_onset_offset

        self.is_to_stream_loss = is_to_stream_loss
        if self.is_to_stream_loss:
            self.stream_loss = StreamMetrics.StreamCrossEntropy(session=session,
                                                                plot_scope=plot_scope,
                                                                data_type_scope=data_type_scope)
            self.summaries.append(self.stream_loss.summary)

        self.reset_metrics_variables_op = self.__define_reset_variables_op(plot_scope=plot_scope,
                                                                           data_type_scope=data_type_scope)

    @property
    def metrics_vals(self):
        if self.__metrics_vals is not None:
            return self.__metrics_vals

        vals = {}
        for m in self.metrics:
            if m.name.lower() in Metrics.BEST_MODEL_METRICS:  # Restrict metrics in which the model will be saved according to.
                vals[m.name] = m.metric_val
        return vals

    def update_loss(self, losses):
        if self.is_to_stream_loss:
            self.stream_loss.update(data=losses)

    @property
    def stream_loss_op(self):
        if not self.is_to_stream_loss:
            return None
        else:
            return self.stream_loss.update_op

    def __define_frame_based_metrics(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):

        accuracy = Metrics.Accuracy(labels_placeholder=labels_placeholder,
                                    predictions_placeholder=predictions_placeholder,
                                    plot_scope=plot_scope,
                                    data_type_scope=data_type_scope)
        precision = Metrics.Precision(labels_placeholder=labels_placeholder,
                                      predictions_placeholder=predictions_placeholder,
                                      plot_scope=plot_scope,
                                      data_type_scope=data_type_scope)
        recall = Metrics.Recall(labels_placeholder=labels_placeholder,
                                predictions_placeholder=predictions_placeholder,
                                plot_scope=plot_scope,
                                data_type_scope=data_type_scope)
        fmeasure = Metrics.FMeasure(recall_metric=recall,
                                    precision_metric=precision,
                                    plot_scope=plot_scope,
                                    data_type_scope=data_type_scope)

        return [
            accuracy,
            precision,
            recall,
            fmeasure
        ]

    def __define_note_onset_metrics(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        precision_onset = Metrics.Precision(labels_placeholder=labels_placeholder,
                                            predictions_placeholder=predictions_placeholder,
                                            plot_scope=plot_scope,
                                            data_type_scope=data_type_scope,
                                            name='Precision_Onset')
        recall_onset = Metrics.Recall(labels_placeholder=labels_placeholder,
                                      predictions_placeholder=predictions_placeholder,
                                      plot_scope=plot_scope,
                                      data_type_scope=data_type_scope,
                                      name='Recall_Onset')
        fmeasure_onset = Metrics.FMeasure(recall_metric=recall_onset,
                                          precision_metric=precision_onset,
                                          plot_scope=plot_scope,
                                          data_type_scope=data_type_scope,
                                          name='FMeasure_Onset')

        return [
            precision_onset,
            recall_onset,
            fmeasure_onset
        ]

    def __define_note_onset_offset_metrics(self, labels_placeholder, predictions_placeholder, plot_scope, data_type_scope):
        precision_onset_offset = Metrics.Precision(labels_placeholder=labels_placeholder,
                                                   predictions_placeholder=predictions_placeholder,
                                                   plot_scope=plot_scope,
                                                   data_type_scope=data_type_scope,
                                                   name='Precision_OnsetOffset')
        recall_onset_offset = Metrics.Recall(labels_placeholder=labels_placeholder,
                                             predictions_placeholder=predictions_placeholder,
                                             plot_scope=plot_scope,
                                             data_type_scope=data_type_scope,
                                             name='Recall_OnsetOffset')
        fmeasure_onset_offset = Metrics.FMeasure(recall_metric=recall_onset_offset,
                                                 precision_metric=precision_onset_offset,
                                                 plot_scope=plot_scope,
                                                 data_type_scope=data_type_scope,
                                                 name='FMeasure_OnsetOffset')

        return [
            precision_onset_offset,
            recall_onset_offset,
            fmeasure_onset_offset
        ]

    def __define_reset_variables_op(self, plot_scope, data_type_scope):
        var_scope = plot_scope + data_type_scope
        # Define reset global metrics variables operations
        running_vars = tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope=var_scope)
        reset_metrics_variables_op = tf.variables_initializer(var_list=running_vars)

        if self.is_to_stream_loss:
            reset_metrics_variables_op = [reset_metrics_variables_op, self.stream_loss.reset_operations]

        return reset_metrics_variables_op

    def __get_frame_based_operations(self):
        return self.__get_operations(metrics=self.__metrics_frame_based)

    def __get_onset_operations(self):
        return self.__get_operations(metrics=self.__metrics_onset)

    def __get_onset_offset_operations(self):
        return self.__get_operations(metrics=self.__metrics_onset_offset)

    def __get_operations(self, metrics):
        operations = []
        for m in metrics:
            operations.append(m.update_op)

        return operations

    def __get_frame_based_summaries(self):
        return self.__get_summaries(metrics=self.__metrics_frame_based)

    def __get_onset_summaries(self):
        return self.__get_summaries(metrics=self.__metrics_onset)

    def __get_onset_offset_summaries(self):
        return self.__get_summaries(metrics=self.__metrics_onset_offset)

    def __get_summaries(self, metrics):
        summaries = []
        for m in metrics:
            summaries.append(m.summary)
        return summaries
