import tensorflow as tf
import numpy as np


from pitch_estimation.auxiliary.metrics.types.metrics import Metric


class StreamMetric(Metric):

    def __init__(self, session, plot_scope, data_type_scope):
        super(StreamMetric, self).__init__(plot_scope=plot_scope, data_type_scope=data_type_scope, labels_placeholder=None, predictions_placeholder=None)
        self.session = session
        self.metric_val = None
        self.update_op = None
        self.assign_op = None
        self.summary = None
        self.reset_operations = None

        self.zero = tf.constant(0.0, dtype=tf.float32)
        var_scope = self.plot_scope + self.data_type_scope
        self.counter = tf.Variable(0.0, name=var_scope + "_" + type(self).__name__ + "_counter", dtype=tf.float32)

        self.define_val_and_update_op()
        self.define_scalar_summary()
        self.define_reset_ops()

    def update(self, data):
        data_len = len(data)
        data_mean = np.mean(data)

        self.update_op = tf.cond(tf.equal(self.counter, self.zero),
                                 lambda: self.metric_val.assign(data_mean),
                                 lambda: self.metric_val.assign(tf.divide(tf.add(tf.multiply(self.metric_val, self.counter), data_mean * data_len), tf.add(self.counter, data_len))))  # ((self.metric_val * self.counter) + (data_mean * data_len)) / data_len + self.counter

        # TODO discover a OnEndOperationsHook execute last one for updating counter
        raise NotImplementedError('runSessionOperationsWithFeedDict doesn\'t exist anymore.')
        self.filter.runSessionOperationsWithFeedDict(operations=self.update_op)  # Update mean
        self.filter.runSessionOperationsWithFeedDict(operations=self.counter.assign_add(data_len))  # Update counter

    def define_val_and_update_op(self):
        var_scope = self.plot_scope + self.data_type_scope
        self.metric_val = tf.Variable(0.0, name=var_scope + "_" + type(self).__name__ + "_metric", dtype=tf.float32)
        self.update_op = None

    def define_reset_ops(self):
        self.reset_operations = [self.counter.assign(self.zero), self.metric_val.assign(self.zero)]


class StreamCrossEntropy(StreamMetric):

    def __init__(self, session, plot_scope, data_type_scope):
        super(StreamCrossEntropy, self).__init__(session=session, plot_scope=plot_scope, data_type_scope=data_type_scope)
