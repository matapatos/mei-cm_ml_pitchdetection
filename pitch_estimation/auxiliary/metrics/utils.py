import collections


import main_globals as mg


class ConfusionMatrix():
    def __init__(self, tp, fp, fn, tn):
        self.tp = tp
        self.fp = fp
        self.fn = fn
        self.tn = tn

    def update(self, label, pred):
        label = round(label)
        pred = round(pred)
        if label == 1 and pred == 1:
            self.tp += 1
            return True
        elif label == 0 and pred == 0:
            self.tn += 1
            return True
        elif label == 1 and pred == 0:
            self.fn += 1
            return False
        elif label == 0 and pred == 1:
            self.fp += 1
            return False
        else:
            mg.ERROR('Invalid value in label or pred variable. Label %s\'\' and pred \'%s\'' % (label, pred))

    def __str__(self):
        return "|{0}, {1}|\n|{2}, {3}|".format(self.tp, self.fp, self.fn, self.tn)

    def __add__(self, other):
        if not isinstance(other, ConfusionMatrix):
            raise NotImplementedError('Addition of is only supported between ConfusionMatrix types but \'%s\' given' % (type(other)))

        return ConfusionMatrix(tp=self.tp + other.tp,
                               fp=self.fp + other.fp,
                               fn=self.fn + other.fn,
                               tn=self.tn + other.tn)


def get_notes_group(group):
    notes_group = []
    has_next_note, note_indexes = __get_next_note_indexes(group)
    while has_next_note:
        notes_group.append(note_indexes)
        has_next_note, note_indexes = __get_next_note_indexes(group, note_indexes)

    return notes_group


def __get_next_note_indexes(labels, note=None):
    start_index = 0
    if note is not None:
        start_index = note.end_index + 1

    if start_index >= len(labels):
        return False, None

    Note = collections.namedtuple('Note', ['start_index', 'end_index'])
    next_note = None
    for i in range(start_index, len(labels)):
        value = labels[i]
        if value > 0.5:
            if next_note is None:
                next_note = Note(start_index=i, end_index=None)
        else:
            if next_note is not None:
                next_note = Note(start_index=next_note.start_index, end_index=i - 1)
                break

    if next_note is None:
        return False, None

    if next_note.end_index is None:
        next_note = Note(start_index=next_note.start_index, end_index=len(labels) - 1)

    return True, next_note
