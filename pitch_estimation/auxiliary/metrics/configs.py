
# Metrics constants
GLOBAL_DATA_TYPE = 'GL'
ITERATION_DATA_TYPE = 'IT'

RATIO_AROUND_PIVOT_OF_NR_FRAMES_FOR_NOTE_BASED_METRIC = 0  # Number of frames that a note can still count as a valid onset metric from the actual starting point
MIN_PERCENTAGE_TO_BE_CONSIDERED_VALID_OFFSET_NOTE = 20
MIN_FRAMES_TO_BE_CONSIDERED_VALID_OFFSET_NOTE = 1
