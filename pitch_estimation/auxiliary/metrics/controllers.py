
import tensorflow as tf
import os


from pitch_estimation.auxiliary.metrics.sub.container import Container
from pitch_estimation.auxiliary.metrics.sub.controller import Controller


class Metrics(Container):
    def __init__(self, note_number, summary_output_folder, best_model_folderpath=None, graph=None, early_stopping=False):
        if graph is None:
            graph = tf.Graph()

        super(Metrics, self).__init__(note_number=note_number,
                                      best_model_folderpath=best_model_folderpath,
                                      graph=graph,
                                      early_stopping=early_stopping)

        self.summary_output_folder = summary_output_folder
        self.metrics = Controller(container=self)
        self.metrics._define_all_metrics()

        if summary_output_folder:
            self.__init_graph_vars()

    def __init_graph_vars(self):
        with self.graph.as_default():
            # Initialize Graph variables
            init = tf.global_variables_initializer()
            self.session.run(init)
            self.metrics.reset_all_metrics()
            self.metrics.init_summary_writters(summary_output_folder=self.summary_output_folder)


class GlobalMetrics(Metrics):
    def __init__(self, summary_output_folder, best_model_folderpath=None, graph=None, early_stopping=False):
        summary_output_folder = os.path.join(summary_output_folder, 'global')
        super(GlobalMetrics, self).__init__(note_number='global',
                                            summary_output_folder=summary_output_folder,
                                            best_model_folderpath=best_model_folderpath,
                                            graph=graph,
                                            early_stopping=early_stopping)


class SingleNoteMetrics(Metrics):
    def __init__(self, note_number, summary_output_folder, best_model_folderpath=None, graph=None, early_stopping=False):
        summary_output_folder = os.path.join(summary_output_folder, 'per_note')
        super(SingleNoteMetrics, self).__init__(note_number=note_number,
                                                summary_output_folder=summary_output_folder,
                                                best_model_folderpath=best_model_folderpath,
                                                graph=graph,
                                                early_stopping=early_stopping)
