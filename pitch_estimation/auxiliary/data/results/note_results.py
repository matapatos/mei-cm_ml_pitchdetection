
import numpy as np
import copy


import main_globals as mg


class NoteResults():
    def __init__(self, creator_identifier, filename, note_number, filter_type, labels, predictions, losses, previous_note_results=None):
        self.creator_identifier = creator_identifier
        self.filename = filename
        self.note_number = note_number
        self.filter_type = filter_type
        self.labels = np.array(labels)
        self.predictions = np.array(predictions)
        self.losses = np.array(losses)
        self.previous_note_results = previous_note_results

    def new_instance(self, creator_identifier):
        if not creator_identifier:
            mg.ERROR('No \'creator_identifier\' given %s' % (creator_identifier))
        self.__self_append_to_list_of_previous_note_results()
        self.creator_identifier = creator_identifier
        return self

    def get_first_note_results_from_creator_identifier(self, creator_identifier):
        if self.creator_identifier == creator_identifier:
            return self
        elif self.previous_note_results:
            return self.previous_note_results.get_first_note_results_from_creator_identifier(creator_identifier=creator_identifier)
        else:
            return None

    def __self_append_to_list_of_previous_note_results(self):
        clone = copy.deepcopy(self)
        if not self.previous_note_results:
            self.previous_note_results = []
        self.previous_note_results = clone
