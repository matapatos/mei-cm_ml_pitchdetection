'''
This file contains the types of sounds that can exists in a dataset
'''


from enum import Enum


class MusicType(Enum):
    SINGLE_NOTE = 'SINGLE_NOTE'
    CHORD_RAND = 'CHORD_RAND'
    CHORD_UCHO = 'CHORD_UCHO'
    MUSIC = 'MUSIC'
