'''
This file contains the model of the data used in the Global Metadata
'''


import abc


import main_globals as mg
import utils as mg_utils
import pitch_estimation.auxiliary.data.metadata.configs_global_metadata as configs


class GlobalMetadata:

    PIANO_TYPES_FORMAT = '_piano_type'

    def __init__(self, metadata, is_training_data):
        self.is_training = is_training_data
        self.piano_types = self.__get_piano_types_from_meta(metadata=metadata)
        self.nr_of_sounds = mg_utils.get_int(metadata[configs.H_NR_SOUNDS])
        self.nr_sounds_as_musics = mg_utils.get_int(metadata[configs.H_NR_OF_MUSICS])
        self.nr_sounds_as_chord_random = mg_utils.get_int(metadata[configs.H_NR_OF_RAND_CHORDS])
        self.nr_sounds_as_chord_ucho = mg_utils.get_int(metadata[configs.H_NR_OF_UCHO_CHORS])
        self.nr_sounds_as_siglenote = mg_utils.get_int(metadata[configs.H_NR_SINGLE_NOTES_SOUNDS])
        self.nr_frames = mg_utils.get_int(metadata[configs.H_NR_FRAMES])
        self.nr_of_notes = mg_utils.get_int(metadata[configs.H_NR_NOTES])
        self.nr_frames_with_note = mg_utils.get_int(metadata[configs.H_NR_FRAMES_WITH_NOTE])
        self.nr_frames_without_note = mg_utils.get_int(metadata[configs.H_NR_FRAMES_WITHOUT_NOTE])
        self.nr_notes_per_frame = list(metadata[configs.H_NR_NOTES_PER_FRAME])
        self.nr_of_notes_by_noteid = list(metadata[configs.H_NR_NOTES_BY_NOTEID])
        self.nr_of_notes_per_music = list(metadata[configs.H_NR_NOTES_PER_MUSIC])
        self.frequency_samples = mg_utils.get_int(metadata.get(configs.H_FREQUENCY_SAMPLES, mg.DEFAULT_FREQUENCY_SAMPLES))
        self.frame_samples = mg_utils.get_int(metadata.get(configs.H_FRAME_SAMPLES, mg.DEFAULT_FRAME_SAMPLES))
        self.data_type = metadata.get(configs.H_DATA_TYPE, mg.DEFAULT_DATA_TYPE)
        self.overlap_ratio = mg_utils.get_int(metadata.get(configs.H_OVERLAP_RATIO, mg.DEFAULT_OVERLAP_RATIO))

    @abc.abstractstaticmethod
    def get_joined_DS_informations(training_global_meta, testing_global_meta):
        if type(training_global_meta) is not GlobalMetadata:
            mg.ERROR('Invalid type for training global metadata.')

        if type(testing_global_meta) is not GlobalMetadata:
            mg.ERROR('Invalid type for testing global metadata.')

        info = '----------- Dataset information -----------'
        info += '\nNumber of frames: ' + str(training_global_meta.nr_frames) + " | " + str(testing_global_meta.nr_frames)
        info += '\nNumber of sounds: ' + str(training_global_meta.nr_of_sounds) + " | " + str(testing_global_meta.nr_of_sounds)
        info += '\n\t-> Number of single notes sounds: ' + str(training_global_meta.nr_sounds_as_siglenote) + " | " + str(testing_global_meta.nr_sounds_as_siglenote)
        info += '\n\t-> Number of chords random sounds: ' + str(training_global_meta.nr_sounds_as_chord_random) + " | " + str(testing_global_meta.nr_sounds_as_chord_random)
        info += '\n\t-> Number of common chords sounds: ' + str(training_global_meta.nr_sounds_as_chord_ucho) + " | " + str(testing_global_meta.nr_sounds_as_chord_ucho)
        info += '\n\t-> Number of musics: ' + str(training_global_meta.nr_sounds_as_musics) + " | " + str(testing_global_meta.nr_sounds_as_musics)
        info += '\n--------------------------------------------------'
        return info

    def __get_piano_types_from_meta(self, metadata):
        piano_types = []
        for key, value in metadata.items():
            if self.PIANO_TYPES_FORMAT in key:
                splitted = key.split(self.PIANO_TYPES_FORMAT)
                piano_types.append({splitted[0]: value})

        return piano_types

    def __str__(self):
        type_of_data = 'Testing'
        if self.is_training:
            type_of_data = 'Training'

        info = '----------- ' + type_of_data + ' dataset information -----------'
        info += '\nNumber of frames: ' + str(self.nr_frames)
        info += '\nNumber of sounds: ' + str(self.nr_of_sounds)
        info += '\n\t-> Number of single notes sounds: ' + str(self.nr_sounds_as_siglenote)
        info += '\n\t-> Number of chords random sounds: ' + str(self.nr_sounds_as_chord_random)
        info += '\n\t-> Number of common chords sounds: ' + str(self.nr_sounds_as_chord_ucho)
        info += '\n\t-> Number of musics: ' + str(self.nr_sounds_as_musics)
        info += '\n--------------------------------------------------'
        return info
