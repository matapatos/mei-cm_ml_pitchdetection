H_FILENAME = 'filename'
H_PIANO_TYPE = 'piano_type'
H_MUSIC_TYPE = 'music_type'
H_NR_FRAMES = 'nr_frames'
H_NR_NOTES_BY_FRAMES = 'nr_of_notes_by_frames'
H_NR_FRAMES_WITH_NOTE = 'nr_frames_with_note'
H_NR_FRAMES_WITHOUT_NOTE = 'nr_frames_without_note'
H_NR_NOTES_PER_FRAME = 'nr_notes_per_frame'
H_NR_NOTES_BY_NOTEID = 'nr_of_notes_by_noteid'
H_NR_NON_ZEROS_PER_FRAME = 'nr_of_non_zeros_per_frame'
H_NR_NOTES_PER_MUSIC = 'nr_of_notes_per_music'
