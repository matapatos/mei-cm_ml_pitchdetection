'''
This file contains the model of the Local Metadata
'''

import pitch_estimation.auxiliary.data.metadata.configs_local_metadata as configs


class LocalMetadata:

    def __init__(self, metadata):
        self.filename = metadata[configs.H_FILENAME]
        self.piano_type = metadata[configs.H_PIANO_TYPE]
        self.music_type = metadata[configs.H_MUSIC_TYPE]
        self.nr_frames = int(metadata[configs.H_NR_FRAMES])
        self.nr_of_notes_by_frames = list(metadata[configs.H_NR_NOTES_BY_FRAMES])
        self.nr_frames_with_note = int(metadata[configs.H_NR_FRAMES_WITH_NOTE])
        self.nr_frames_without_note = int(metadata[configs.H_NR_FRAMES_WITHOUT_NOTE])
        self.nr_notes_per_frame = list(metadata[configs.H_NR_NOTES_PER_FRAME])
        self.nr_of_notes_by_noteid = list(metadata[configs.H_NR_NOTES_BY_NOTEID])
        self.nr_of_non_zeros_per_frame = list(metadata[configs.H_NR_NON_ZEROS_PER_FRAME])
        self.nr_of_notes_per_music = list(metadata[configs.H_NR_NOTES_PER_MUSIC])

    def __str__(self):
        info = '----------- Music information -----------'
        info += '\nFilename: ' + self.filename
        info += '\nPiano type: ' + self.piano_type
        info += '\nMusic type: ' + self.music_type
        info += '\nNumber of frames: ' + str(self.nr_frames)
        info += '\n\t-> With at least one note: ' + str(self.nr_frames_with_note)
        info += '\n\t-> Without any note: ' + str(self.nr_frames_without_note)
        info += '\n--------------------------------------------------'
        return info
