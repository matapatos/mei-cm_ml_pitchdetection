import os


import main_globals as mg
import utils
import pitch_estimation.tasks.post_processing.types.all as pp_units


def get_summaries_output_folder(start_time):
    return __get_folder(start_time=start_time, default_folder=mg.PARENT_SUMMARIES_FOLDER)


def get_models_output_folder(start_time):
    return __get_folder(start_time=start_time, default_folder=mg.PARENT_MODELS_FOLDER)


def get_log_music_sequence_filepath(start_time):
    log_folder = __get_folder(start_time=start_time, default_folder=mg.LOG_FOLDER)
    return os.path.join(log_folder, mg.LOG_TRAINING_MUSIC_SEQUENCE)


def get_log_training_configs_filepath(start_time):
    log_folder = __get_folder(start_time=start_time, default_folder=mg.LOG_FOLDER)
    return os.path.join(log_folder, mg.LOG_TRAINING_CONFIGS)


def get_best_models_folderpath(start_time):
    return get_models_output_folder(start_time=start_time)


def add_post_processing_unit_if_does_not_exist(post_processes, names_post_processes, pp_unit_class):
    if not post_processes:
        post_processes = []

    if not names_post_processes:
        names_post_processes = []

    if len(post_processes) != len(names_post_processes):
        mg.ERROR('Length of post_processes is different from the names_post_processes. \n\tpost_processes: %s\n\tnames_post_processes: %s' % (post_processes, names_post_processes))

    has_write_output = False
    for pp in reversed(post_processes):
        if pp == pp_unit_class:
            has_write_output = True
            break
        elif pp in pp_units.units_that_change_output_result:
            has_write_output = False
            break

    if not has_write_output:
        post_processes.append(pp_unit_class)
        names_post_processes.append(pp_unit_class.get_class_name(pp_unit_class))

    return post_processes, names_post_processes


def __get_folder(start_time, default_folder):
    if type(start_time) is not float:
        start_time = start_time.start_time

    output_folder = utils.get_output_folder(start_time=start_time, default_folder=default_folder)
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    return output_folder
