# Project description #

**Note:** All the source code in this repository is derived from the work (dissertation): *Automatic Transcription of Music using Deep Learning Techniques* by Gil, Grilo, Reis and Domingues (2019).


This repository contains the source code of the *classifiers* using the Tensorflow framework, that composes our transcription system which is presented in the work mentioned above.
This system applies the *divide-and-conquer* design paradigm, where each classifier is responsible for transcribing only one musical note. Resulting in a total of 88 classifiers.
Apart from that, this repository also contains the source code of the several tested post-processing steps:

- *Fix notes duration* -> nn_per_note
- *Fix notes duration regarding to onsets* -> nn_per_note_also_classifiers_onsets_classifier
- *Fix onsets* -> fix_onsets
- *Fix offsets* -> fix_offsets (it does not improves the system performance)
- *Discard notes* -> discard_notes (it does not improves the system performance)


To conclude, the dataset used for training/testing these classifiers is the one created by [MEI-CM_ML_IO_Generator](https://bitbucket.org/matapatos/mei-cm_ml_io_generator/src/master/).


# Installation process #


## Ubuntu/Linux ##

For **automatic installation** you can simply run the following script in a terminal:

```
./set_up_linux.sh
```

Otherwise, if you prefer to install it **manually**, you can do the following steps:

1. Install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
2. Create a new conda environment called 'pitchestimation' with python 3.6
3. Activate the pitchestimation conda environment
4. Install [Pip](https://bootstrap.pypa.io/get-pip.py)
5. Install the necessary dependencies from the requirements.txt file
6. Install the following required packages for magenta using the command: sudo apt-get install build-essential libasound2-dev libjack-dev

## Windows/Mac ##

Currently the *magenta* package does not support Windows or Mac. However, in case that you want to run this project in a Windows/Mac OS you can follow the following steps:

1. Clone project
2. Checkout to another branch called *other_OS*
3. Following Ubuntu manual installation from step **1** to **5**

Nevertheless, note that using this brach makes the functionality of converting a predicted transcription into an audible version (e.g. [transcription examples](https://bitbucket.org/matapatos/mei-cm_ml_pitchdetection/src/master/transcription_examples/)) not available.

# Run project #

There are two main scripts in this project:

1. *main.py*, that is used for the classification stage (classifiers), and 
2. *training_pp_units.py*, that is for the post-processing stage.

When any of the scripts are runned, an additional directory is created in the folder *./others/output/[DATETIME]* for saving the best achieved model and another folder in *./others/graphs/[DATETIME]* with the achieved metrics during the training/prediction stage (i.e. in reality are tensorboard graphs).

## main.py ##

#### Training example ####

```
python main.py --training -n [NUMBER OF EPOCHS TO TRAIN] --notes_number [NOTE MIDI NUMBER TO TRAIN] -ds [DATASET PATH]
```

Arguments explanation:

- *-n*, is the number of epochs to train
- *--notes_number* must be **only** one note MIDI number between 21 and 108 (both inclusive)
- *ds* a dataset folder with both training and testing datasets created by [MEI-CM_ML_IO_Generator](https://bitbucket.org/matapatos/mei-cm_ml_io_generator/src/master/)

#### Prediction example ####

```
python main.py --predicting --notes_number [NOTE MIDI NUMBER TO TRAIN] -ds [DATASET PATH] -m [MODELS ROOT FOLDER]
```

Arguments explanation:

- *notes_number* in this case can be one or several MIDI note numbers between 21 and 108 (both inclusive)
- *m* is the root folder containing the resultant models

## training_pp_units.py ##


#### Training example ####

For training a post-processing unit, the process is very similar to the previous one, however it is necessary to specify an *--upload* option and also a type of post-processing using the *--post-processing* option. 
An example can be seen bellow:

```
python training_pp_units.py --training -n [NUMBER OF EPOCHS TO TRAIN] --notes_number [NOTE MIDI NUMBER TO TRAIN] -ds [DATASET PATH] --post_processing step_1|step_2|step_3 --upload [FOLDER WHERE TO UPLOAD TRAINED MODEL]
```

#### Testing example ####

```
python training_pp_units.py --predicting -n [NUMBER OF EPOCHS TO TRAIN] --notes_number [NOTE MIDI NUMBER TO TRAIN] -ds [DATASET PATH] --post_processing step_1|step_2|step_3 --upload [FOLDER WHERE TO UPLOAD RESULTS] -m [MODELS ROOT FOLDER] 
```

#### Dataset for training_pp_units.py ####


When executing the *training_pp_units.py* script a different dataset must be given as input. This dataset consists of the previous outputted predictions from the previous stage. For instance, for the first step of the post-processing stage it is necessary to give as input the outputed predictions from the classification stage.
This dataset is created whenever you run any of the scripts, *main.py* or *training_pp_units.py*. However, before using it, it is necessary to do the following steps:

1. Open generated "predictions" dataset
2. Delete all occurences of *MAPS_MUS_\**


## Debug mode ##

For additional feedback the *-d/-D/--debug* option can be applied in joint with the previous mentioned commands.