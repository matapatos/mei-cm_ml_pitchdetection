'''
This file contains all the globals variables or functions corresponding to the program it self
'''
import os
import sys
from enum import Enum


# FLOYDHUB PARAMETERS
FLOYDHUB_PARENT_MODELS_FOLDER = '/output/output'
FLOYDHUB_PARENT_SUMMARIES_FOLDER = '/output/graphs'
FLOYDHUB_DATASET_FOLDER = '/mei-cm_ml_io_generator_output'
# END PARAMETERS

# LOCAL PC PARAMETERS
SEQUENCIAL_MUSICS = True  # True for being able to repeat the experiment or False for best performant model
PARENT_OTHERS_FOLDER_NAME = 'others'
PARENT_MODELS_FOLDER_NAME = os.path.join(PARENT_OTHERS_FOLDER_NAME, 'output')
PARENT_GRAPHS_FOLDER_NAME = os.path.join(PARENT_OTHERS_FOLDER_NAME, 'graphs')
PARENT_MODELS_FOLDER = os.path.join('.', PARENT_MODELS_FOLDER_NAME)
PARENT_SUMMARIES_FOLDER = os.path.join('.', PARENT_GRAPHS_FOLDER_NAME)
LOG_FOLDER = PARENT_SUMMARIES_FOLDER
LOG_TRAINING_MUSIC_SEQUENCE = 'log_training_music_sequence.txt'
LOG_TRAINING_CONFIGS = 'log_training_configs.txt'
DATASET_FOLDER = '../mei-cm_ml_io_generator/output'
# DATASET_FOLDER = '/media/andre/AB4A-A1D0/Thesis/GeneratedDatasets/Gustavo_DS/FFT_20_80'
# END PARAMETERS
FIRST_NOTE_NUMBER = 21
NUMBER_OF_NOTES = 88
BATCH_SIZE = 1  # If 1 is used, it consist on SGD otherwise a number higher than 1 consists on Mini-batch GD.

# Default parameters
DEFAULT_FREQUENCY_SAMPLES = 44100  # Nr of samples per second
DEFAULT_FRAME_SAMPLES = 4096
DEFAULT_DATA_TYPE = 'FFT'
DEFAULT_OVERLAP_RATIO = 1.0
# NUMBER_OF_SAMPLES_PER_FRAME = 4096
NUMBER_OF_SAMPLES_PER_FRAME = int(DEFAULT_FRAME_SAMPLES / (2 if DEFAULT_DATA_TYPE == 'FFT' else 1))

MAX_CHECKPOINTS_TO_KEEP = 1  # Used for property max_to_keep in https://www.tensorflow.org/api_docs/python/tf/train/Saver
TENSORFLOW_MODEL_EXTENSION = '.ckpt'


def ERROR(message, stop_program=True):
    print("ERROR: " + str(message))
    if stop_program:
        sys.exit(-2)


def WARN(message):
    print("WARN: " + str(message))


def INFO(message):
    print("INFO: " + str(message))


def ENABLE_DEBUG():
    global DEBUG
    DEBUG = _DEBUG


def _DEBUG(message):
    print("DEBUG: " + str(message))


def _EMPTY_DEBUG(message):
    return


DEBUG = _EMPTY_DEBUG


def ENABLE_FLOYD_HUB_MODE():
    global PARENT_MODELS_FOLDER
    PARENT_MODELS_FOLDER = FLOYDHUB_PARENT_MODELS_FOLDER

    global PARENT_SUMMARIES_FOLDER
    PARENT_SUMMARIES_FOLDER = FLOYDHUB_PARENT_SUMMARIES_FOLDER

    global DATASET_FOLDER
    DATASET_FOLDER = FLOYDHUB_DATASET_FOLDER

    WARN("FloydHub mode enabled")


def SET_DATASET_FOLDER(dataset_folder):
    global DATASET_FOLDER
    DATASET_FOLDER = dataset_folder


def GET_TRAINING_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'train.csv')


def GET_TESTING_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'test.csv')


def GET_TRAINING_OUTPUT_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'train_output.csv')


def GET_TESTING_OUTPUT_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'test_output.csv')


def GET_TRAINING_GLOBAL_METADATA_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'train_global_metadata.csv')


def GET_TESTING_GLOBAL_METADATA_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'test_global_metadata.csv')


def GET_TRAINING_LOCAL_METADATA_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'train_local_metadata.csv')


def GET_TESTING_LOCAL_METADATA_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'test_local_metadata.csv')


def GET_ONSETS_OUTPUT_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'onset_output.csv')


def GET_POSSIBLE_NOTES_OUTPUT_FILEPATH():
    return os.path.join(DATASET_FOLDER, 'possible_notes.csv')


def GET_MIREX_CONFUSION_MATRIX_OUTPUT_FILEPATH():
    return os.path.join()


def GET_ALL_NOTES():
    all_notes = []
    for n in range(FIRST_NOTE_NUMBER, FIRST_NOTE_NUMBER + NUMBER_OF_NOTES):
        all_notes.append(n)
    return all_notes


class RunMode(Enum):
    TRAINING = 0
    PREDICTING = 1
    EVALUATING = 2
