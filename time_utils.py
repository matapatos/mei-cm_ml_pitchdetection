import time


TIME_FORMAT = '%a_%b_%d_%Hh%Mm%Ss_%Y'
L1_BIGGER_THAN_L2 = 1
L1_EQUAL_L2 = 0
L1_SMALLER_THAN_L2 = -1


def convert_string_as_datetime(str_):
        return time.strptime(str_, TIME_FORMAT)


def compare_localtimes(localtime_1, localtime_2):
        l1 = time.mktime(localtime_1)
        l2 = time.mktime(localtime_2)

        if l1 > l2:
                return L1_BIGGER_THAN_L2
        elif l2 > l1:
                return L1_SMALLER_THAN_L2
        else:
                return L1_EQUAL_L2
